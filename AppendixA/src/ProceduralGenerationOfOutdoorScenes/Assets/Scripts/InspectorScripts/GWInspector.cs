using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenerateWater))]
public class GWInspector : Editor
{
    private SerializedProperty _controlPoints;
    private SerializedProperty _showControlPoints;

    private SerializedProperty _waterLevel;
    private SerializedProperty _waterMaxYOffset;
    private SerializedProperty _travelDistance;
    private SerializedProperty _maxStepsUntilFail;
    private SerializedProperty _waterWrapping;
    private SerializedProperty _material;
    private SerializedProperty _carveDepth;
    private SerializedProperty _waterStartWidth;
    private SerializedProperty _waterEndWidth;
    private SerializedProperty _riverStartDepth;
    private SerializedProperty _riverEndDepth;
    private SerializedProperty _waterLevelStart;
    private SerializedProperty _waterLevelEnd;
    private SerializedProperty _underWaterLevelDepth;

    private SerializedProperty _targetWeight;
    private SerializedProperty _heightDifferenceWeight;
    private SerializedProperty _gradientWeight;
    private SerializedProperty _sameDirectionWeight;
    
    private bool _controlPointsFoldout = true;
    private bool _waterFoldout = true;
    private bool _isAddingControlPoints = false;
    
    private int _alternationType = 0;
    
    // Set the used properties on enable
    private void OnEnable()
    {
        _controlPoints = serializedObject.FindProperty("controlPointsList");
        _showControlPoints = serializedObject.FindProperty("showControlPoints");
        _waterStartWidth = serializedObject.FindProperty("waterStartWidth");
        _waterEndWidth = serializedObject.FindProperty("waterEndWidth");
        _waterLevel = serializedObject.FindProperty("waterLevel");
        _waterMaxYOffset = serializedObject.FindProperty("waterMaxYOffset");
        _travelDistance = serializedObject.FindProperty("travelDistance");
        _maxStepsUntilFail = serializedObject.FindProperty("maxStepsUntilFail");
        _waterWrapping = serializedObject.FindProperty("waterWrapping");
        _material = serializedObject.FindProperty("material");
        _carveDepth = serializedObject.FindProperty("carveDepth");
        _riverStartDepth = serializedObject.FindProperty("riverStartDepth");
        _riverEndDepth = serializedObject.FindProperty("riverEndDepth");
        _waterLevelStart = serializedObject.FindProperty("waterLevelStart");
        _waterLevelEnd = serializedObject.FindProperty("waterLevelEnd");
        _underWaterLevelDepth = serializedObject.FindProperty("underWaterLevelDepth");
        _targetWeight = serializedObject.FindProperty("targetWeight");
        _heightDifferenceWeight = serializedObject.FindProperty("heightDifferenceWeight");
        _gradientWeight = serializedObject.FindProperty("gradientWeight");
        _sameDirectionWeight = serializedObject.FindProperty("sameDirectionWeight");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GenerateWater generateWater = (GenerateWater) target;
        
        // Water chunks parameters
        EditorGUILayout.LabelField("Water Chunk Mesh Parameters", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_waterWrapping, new GUIContent("Mesh Wrapping:", "Number of points in a line for a water chunk."));
        EditorGUILayout.PropertyField(_material, new GUIContent("Water Material:", "Material that all the chunks will use."));
        EditorGUILayout.PropertyField(_waterLevel, new GUIContent("Water Level:", "The base water level height."));
        
        EditorGUILayout.Space(10);

        // Generate water chunks button
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Generate Water Chunks"))
        {
            generateWater.GenerateWaterChunksButton();
        }
        EditorGUILayout.EndHorizontal();

        // Reset water data button
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Reset Water Data")) // "Resets all river textures."
        {
            generateWater.ResetWaterData();
            SceneView.RepaintAll();
        }
        // Delete water chunks button
        if (GUILayout.Button("Delete Water Chunks")) // "Deletes all water chunks."
        {
            generateWater.DeleteWaterChunksButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);

        // Control Points Foldout
        _controlPointsFoldout = EditorGUILayout.Foldout(_controlPointsFoldout, "River Control points", EditorStyles.foldoutHeader);
        if (_controlPointsFoldout)
        {
            // Set text of adding button
            string buttonText;
            if (_isAddingControlPoints)
            {
                buttonText = "Stop Manually Adding Points";
            }
            else
            {
                buttonText = "Start Manually Adding Points";
            }
        
            // Adding manual point button
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(buttonText))
            {
                _isAddingControlPoints = !_isAddingControlPoints;
            }
            EditorGUILayout.EndHorizontal();
        
            // Control point field
            EditorGUILayout.PropertyField(_controlPoints, new GUIContent("Control Points:", "Control points of the road"));
            EditorGUILayout.PropertyField(_showControlPoints, new GUIContent("Show Control points:", "Toggles showing of the control points."));
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
        
            // Reset points button
            if (GUILayout.Button("Reset points"))
            {
                generateWater.ResetControlPointsButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            
            // Reset all expect 1st and last button
            if (GUILayout.Button("Reset except 1st and last"))
            {
                generateWater.ResetControlPointsWithoutFirstLastButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.Space(20);

            // River agent parameters
            EditorGUILayout.LabelField("River Agent", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_travelDistance, new GUIContent("Travel Distance:", "Distance from one generated control point to another."));
            EditorGUILayout.PropertyField(_maxStepsUntilFail, new GUIContent("Max. Steps of Agent:", "The maximum steps from start to end an agent can take."));
            EditorGUILayout.Space(10);

            // Reward parameters for the river agent
            EditorGUILayout.LabelField("Reward Parameters", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_targetWeight, new GUIContent("Direction To Target:", "How much the direction to the next point is considered."));
            EditorGUILayout.PropertyField(_heightDifferenceWeight, new GUIContent("Height Difference:", "How much difference in height to the candidate point is considered."));
            EditorGUILayout.PropertyField(_gradientWeight, new GUIContent("Gradient Direction:", "How much the direction of the current gradient is considered."));
            EditorGUILayout.PropertyField(_sameDirectionWeight, new GUIContent("Same Direction:", "How much the last direction is considered."));

            EditorGUILayout.Space(10);

            // Connect river button
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Connect River"))
            {
                generateWater.ConnectRiverPointsButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);

        // Water and river mesh alteration
        _waterFoldout = EditorGUILayout.Foldout(_waterFoldout, "Water Mesh Alteration", EditorStyles.foldoutHeader);
        if (_waterFoldout)
        {
            EditorGUILayout.PropertyField(_waterMaxYOffset, new GUIContent("Water Max Y Offset:", "The maximum height difference the water can be offsetted from the terrain. DO NOT change this value after generating, if you do reset the rivers."));

            EditorGUILayout.Space();
            
            string[] labels = {"River Generation", "Terrain Carve"};
            EditorGUILayout.LabelField("Alternation type:" , EditorStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            _alternationType = GUILayout.Toolbar(_alternationType, labels);
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            // River generation parameters
            if (_alternationType == 0)
            {
                EditorGUILayout.PropertyField(_waterStartWidth, new GUIContent("River Start Width:", "Width of a river at the start of the river."));
                EditorGUILayout.PropertyField(_waterEndWidth, new GUIContent("River End Width:", "Width of a river at the end of the river."));
                EditorGUILayout.Space(5);
                EditorGUILayout.PropertyField(_riverStartDepth, new GUIContent("River Start Depth:", "The starting depth of the river to be generated. Depth of the river is interpolated between start and end depth. (Negative number means offset of terrain downwards.)"));
                EditorGUILayout.PropertyField(_riverEndDepth, new GUIContent("River End Depth:", "The ending depth of the river to be generated. Depth of the river is interpolated between start and end depth. (Negative number means offset of terrain downwards.)"));
                EditorGUILayout.Space(5);
                EditorGUILayout.PropertyField(_waterLevelStart, new GUIContent("WaterLevel Start:", "Height of water at the start of the currently generated river."));
                EditorGUILayout.PropertyField(_waterLevelEnd, new GUIContent("WaterLevel End:", "Height of water at the end of the currently generated river."));
                EditorGUILayout.Space(5);
                EditorGUILayout.PropertyField(_underWaterLevelDepth, new GUIContent("Under WaterLevel Depth:", "Height to which the terrain will be carved to under the water level (usually at the end of rivers.)."));

                EditorGUILayout.Space();

                // Generate river button
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Generate River")) // "Lowers the terrain and Highers the water level on water chunks."
                {
                    generateWater.GenerateRiverButton(true, false);
                    SceneView.RepaintAll();
                }
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();                
            }

            // Terrain carve parameters
            if (_alternationType == 1)
            {
                EditorGUILayout.PropertyField(_waterStartWidth, new GUIContent("Carve Start Width:", "Width of a river at the start of the river."));
                EditorGUILayout.PropertyField(_waterEndWidth, new GUIContent("Carve End Width:", "Width of a river at the end of the river."));
                EditorGUILayout.PropertyField(_carveDepth, new GUIContent("Carve Depth:", "Target distance from water level to the bottom of the terrain underwater. (Negative number means offset of terrain downwards.)" ));

                EditorGUILayout.Space();

                // Carve terrain button
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Carve Terrain"))
                {
                    generateWater.GenerateRiverButton(false, false);
                    SceneView.RepaintAll();
                }
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();
            }
        }

        EditorGUILayout.Space();
        
        // Reset the changes made by terrain carve and river generation button
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Reset Mesh Alternation Using Control Points"))
        {
            generateWater.GenerateRiverButton(false, true);
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);

        serializedObject.ApplyModifiedProperties();
    }

    // Used for raycasting from camera to terrain
    private void OnSceneGUI()
    {
        if(!_isAddingControlPoints) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray rayToCast = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            
            if (Physics.Raycast(rayToCast, out hitInfo))
            {
                GenerateWater generateWater = (GenerateWater) target;
                generateWater.AddWaterPoint(hitInfo.point);
                
            }
            Event.current.Use();
        }
    }
}
