using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenerateHydroErosion))]
public class GHInspector : Editor
{
    private SerializedProperty _maxAngle;
    private SerializedProperty _maxStepsUntilFail;
    private SerializedProperty _inertia;
    private SerializedProperty _numberOfDropsToSimulatePerChunk;
    private SerializedProperty _erosionStrength;
    private SerializedProperty _dropType;
    private SerializedProperty _dropletWidth;

    private bool _isAddingControlPoints = true;
    
    // Set the used properties on enable
    private void OnEnable()
    {
        _maxAngle = serializedObject.FindProperty("maxAngle");
        _maxStepsUntilFail = serializedObject.FindProperty("maxStepsUntilFail");
        _inertia = serializedObject.FindProperty("inertia");
        _numberOfDropsToSimulatePerChunk = serializedObject.FindProperty("numberOfDropsToSimulatePerChunk");
        _erosionStrength = serializedObject.FindProperty("erosionStrength");
        _dropType = serializedObject.FindProperty("dropType");
        _dropletWidth = serializedObject.FindProperty("dropletWidth");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GenerateHydroErosion generateHydraulicErosion = (GenerateHydroErosion) target;

        EditorGUILayout.LabelField("Test the droplet paths" , EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Red control point means subtracting from terrain and blue control point means adding to the terrain.", MessageType.Info);

        // Set text of button
        string buttonText;
        if (_isAddingControlPoints)
        {
            buttonText = "Stop Manually Adding Points";
        }
        else
        {
            buttonText = "Start Manually Adding Points";
        }
        
        // Add points manually button
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button(buttonText))
        {
            _isAddingControlPoints = !_isAddingControlPoints;
        }

        // Reset points button
        if (GUILayout.Button("Reset points"))
        {
            generateHydraulicErosion.ResetControlPointsButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space(20);

        // Drop type
        string[] test = {"Grid", "Random"};
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Drop type:" , EditorStyles.boldLabel);
        _dropType.intValue = GUILayout.Toolbar(_dropType.intValue, test);
        EditorGUILayout.EndHorizontal();

        // Droplet parameters
        EditorGUILayout.LabelField("Droplet Parameters" , EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_maxAngle, new GUIContent("Maximum Angle:", "When the droplets next direction differs by this angle the simulation of this drop is stopped."));
        EditorGUILayout.PropertyField(_maxStepsUntilFail, new GUIContent("Droplet lifetime:", "Maximum number of steps a droplet takes."));
        EditorGUILayout.PropertyField(_inertia, new GUIContent("Inertia of droplets:", "How much momentum will they keep from last position."));
        EditorGUILayout.PropertyField(_numberOfDropsToSimulatePerChunk, new GUIContent("Droplets per chunk row:", "How many drops will be cast on the terrain per chunk per row in a grid (works same as wrapping)."));
        EditorGUILayout.PropertyField(_dropletWidth, new GUIContent("Droplets Width of Effect:", "How far from its path will the droplet affect the terrain."));
        
        EditorGUILayout.Space();
        
        // Erosion interactive parameters
        EditorGUILayout.LabelField("Interactive parameters:" , EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_erosionStrength, new GUIContent("Erosion Strength:", "Strength of the erosion map."));

        EditorGUILayout.Space(10);

        // Generate erosion button
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Generate Erosion"))
        {
            generateHydraulicErosion.GenerateErosionButton();
            SceneView.RepaintAll();
        }
        // Reset erosion button
        if (GUILayout.Button("Reset Erosion"))
        {
            generateHydraulicErosion.ResetErosionButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        
        serializedObject.ApplyModifiedProperties();
    }

    // Used for raycasting from camera to terrain
    private void OnSceneGUI()
    {
        if(!_isAddingControlPoints) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray rayToCast = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            
            if (Physics.Raycast(rayToCast, out hitInfo))
            {
                GenerateHydroErosion generateHydraulicErosion = (GenerateHydroErosion) target;
                generateHydraulicErosion.DropADroplet(hitInfo.point);
                
            }
            Event.current.Use();
        }

    }
}
