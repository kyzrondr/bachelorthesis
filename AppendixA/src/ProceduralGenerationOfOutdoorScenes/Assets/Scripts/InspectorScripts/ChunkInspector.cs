using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

[CustomEditor(typeof(Chunk)), CanEditMultipleObjects]
public class ChunkInspector : Editor
{
    private SerializedProperty _enableChange;
    private SerializedProperty _heightOffset;
    private SerializedProperty _heightRange;
    
    // Set the used properties on enable
    private void OnEnable()
    {
        _enableChange = serializedObject.FindProperty("enableTerrainChange");
        _heightOffset = serializedObject.FindProperty("heightOffset");
        _heightRange = serializedObject.FindProperty("heightRange");
    }

    // Generates chunks from selected in a given direction
    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();
        
        // Toggle to enable height changing
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Change Height Tool", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_enableChange, new GUIContent("Enable ", "Enables the influence of height change in this chunk."));
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();

        // Show height effector values when enabled
        if (_enableChange.boolValue)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(_heightOffset, new GUIContent("Height Offset:", "Offset in y-axis of the chunks points."));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(_heightRange, new GUIContent("Range:", "Range of the height change."));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        
        EditorGUILayout.Space(20);
        
        // Multi chunk generation
        Object[] objectChunks = targets;
        if (GUILayout.Button("Generate Chunk Up (z+)"))
        {
            Object[] newSelection = new Object[Selection.objects.Length];
            int i = 0;
            foreach (Object objectChunk in objectChunks)
            {
                Chunk chunk = (Chunk) objectChunk;
                Chunk newChunk = chunk.GenerateTopChunk();
                if (newChunk == null) continue;
                newSelection[i] = newChunk.gameObject;
                i++;
            }

            Selection.objects = newSelection;
        }
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Generate Chunk left (x-)"))
        {
            Object[] newSelection = new Object[Selection.objects.Length];
            int i = 0;
            foreach (Object objectChunk in objectChunks)
            {
                Chunk chunk = (Chunk) objectChunk;
                Chunk newChunk = chunk.GenerateLeftChunk();
                if (newChunk == null) continue;
                newSelection[i] = newChunk.gameObject;
                i++;
            }
            
            Selection.objects = newSelection;
        }
        if (GUILayout.Button("Generate Chunk Right (x+)"))
        {
            Object[] newSelection = new Object[Selection.objects.Length];
            int i = 0;
            foreach (Object objectChunk in objectChunks)
            {
                Chunk chunk = (Chunk) objectChunk;
                Chunk newChunk = chunk.GenerateRightChunk();
                if (newChunk == null) continue;
                newSelection[i] = newChunk.gameObject;
                i++;
            }
            
            Selection.objects = newSelection;
        }
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Generate Chunk Down (z-)"))
        {
            Object[] newSelection = new Object[Selection.objects.Length];
            int i = 0;
            foreach (Object objectChunk in objectChunks)
            {
                Chunk chunk = (Chunk) objectChunk;
                Chunk newChunk = chunk.GenerateBottomChunk();
                if (newChunk == null) continue;
                newSelection[i] = newChunk.gameObject;
                i++;
            }
            
            Selection.objects = newSelection;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
