using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenerateTerrain))]
public class GTInspector : Editor
{
    private SerializedProperty _seed;
    private SerializedProperty _parent;
    private SerializedProperty _amplitude;
    private SerializedProperty _wrapping;
    private SerializedProperty _numberOfChunks;
    private SerializedProperty _material;
    private SerializedProperty _chunkManager;
    private SerializedProperty _numberOfOctaves;
    private SerializedProperty _perlinScale;

    // Set the used properties on enable
    private void OnEnable()
    {
        _seed = serializedObject.FindProperty("seed");
        _parent = serializedObject.FindProperty("parent");
        _amplitude = serializedObject.FindProperty("amplitude");
        _wrapping = serializedObject.FindProperty("wrapping");
        _numberOfChunks = serializedObject.FindProperty("numberOfChunks");
        _material = serializedObject.FindProperty("material");
        _chunkManager = serializedObject.FindProperty("chunkManager");
        _numberOfOctaves = serializedObject.FindProperty("numberOfOctaves");
        _perlinScale = serializedObject.FindProperty("perlinScale");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();
        
        GenerateTerrain generateTerrain = (GenerateTerrain)target;
        
        // Parameters
        EditorGUILayout.PropertyField(_seed, new GUIContent("Seed:", "Seed used to generate the random terrain."));
        EditorGUILayout.PropertyField(_parent, new GUIContent("Parent:", "To what object the generated chunks will be parented."));
        EditorGUILayout.PropertyField(_perlinScale, new GUIContent("Perlin scale:", "How will the perlin noise be scaled. Smaller means smoother.."));
        EditorGUILayout.PropertyField(_amplitude, new GUIContent("Amplitude:", "The highest possible point of terrain."));
        EditorGUILayout.PropertyField(_numberOfOctaves,
            new GUIContent("No. octaves of perlin n.:", "How much will the terrain be recursively bumpy."));
        
        // Wrapping buttons
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField( new GUIContent("Wrapping:", "How many verticies will be in a line."));
        if (GUILayout.Button("-2"))
        {
            generateTerrain.SetNewWrappingButton(_wrapping.intValue - 2);
        }        
        var labelStyle = new GUIStyle(GUI.skin.label) {alignment = TextAnchor.MiddleCenter};
        GUILayout.Label(_wrapping.intValue.ToString(), labelStyle);
        if (GUILayout.Button("+2"))
        {
            generateTerrain.SetNewWrappingButton(_wrapping.intValue + 2);
        }
        EditorGUILayout.EndHorizontal();

        // Parameters
        EditorGUILayout.PropertyField(_numberOfChunks, new GUIContent("Width and Height:", "How many chunks will be generated in all directions."));
        EditorGUILayout.PropertyField(_material, new GUIContent("Material:", "Which material will the terrain use (will be tiled based on wrapping)."));
        EditorGUILayout.PropertyField(_chunkManager, new GUIContent("Chunk Manager:", "After generating with button below this object will be the origin of the terrain."));

        // Generate terrain button
        EditorGUILayout.Space(20);
        if (GUILayout.Button("Generate Terrain"))
        {
            generateTerrain.GenerateButton();
        }

        serializedObject.ApplyModifiedProperties();
    }
}

// Read only attribute
public class ReadOnlyInspectorAttribute : PropertyAttribute {}

[CustomPropertyDrawer(typeof(ReadOnlyInspectorAttribute))]
public class ReadOnlyInspectorDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label);
        GUI.enabled = true;
    }
}

