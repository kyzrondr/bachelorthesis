using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class WaterChunk : MonoBehaviour
{
    private ChunkManager _chunkManager;
    private GenerateWater _generateWater;
    private Chunk _parentChunk;

    // Water Chunk parameters
    private int _wrappingOfPoints;
    private float _waterLevel;

    // Variables of chunk
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;
    private MeshCollider _meshCollider;
    private Vector2Int _indexes;
    private MaterialPropertyBlock _materialPropertyBlock;
    
    // Water variables
    private Texture2D _waterTexture;
    private float _waterMaxYOffset;
    private float _waterYOffset;

    // Constants
    private const float WATER_CHUNK_SIZE = 30;
    private const string WATER_FOLDER = "WaterTextures/";
    private const int WATER_RESOLUTION = 128;
    
    // Helps to not do mesh updates in OnValidate()
    private bool _updateNextFrame;

    // ----------------------------------Mono-behaviour-----------------------------------------------------------------
    // On Inspector change
    private void OnValidate()
    {
        // Checks the references to important elements of the scene
        if (_chunkManager == null)
        {
            _chunkManager = transform.parent.parent.GetComponent<ChunkManager>();
        }
        if (_generateWater == null)
        {
            _generateWater = transform.parent.parent.parent.GetComponentInChildren<GenerateWater>();
        }
        if (_parentChunk == null)
        {
            _parentChunk = transform.parent.GetComponent<Chunk>();
        }
    }

    private void Update()
    {
        // Updates the height of the water chunk if an update was posted
        if (_updateNextFrame)
        {
            _updateNextFrame = false;
            UpdateHeight();
        }
    }

    //----------------------------------------------Water-Chunk-Creation------------------------------------------------
    // Initializes all the essential information of the water chunk
    public void Initialize(ChunkManager chunkManager, GenerateWater generateWater, Vector2Int indexes,
            float waterLevel, int wrapping, Material material)
    {
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();
        _meshFilter = gameObject.AddComponent<MeshFilter>();
        _meshCollider = gameObject.AddComponent<MeshCollider>();

        _chunkManager = chunkManager;
        _generateWater = generateWater;
        _parentChunk = transform.parent.GetComponent<Chunk>();
        _indexes = indexes;
        _wrappingOfPoints = wrapping;
        _waterLevel = waterLevel;
        
        _meshRenderer.material = material;
        _materialPropertyBlock = new MaterialPropertyBlock();
        
        // water texture
        _waterTexture = GetEmptyWaterTexture();
        SetWaterTexture(_waterTexture);
        
        transform.position = new Vector3(indexes.x, 0, indexes.y) * WATER_CHUNK_SIZE;
        CalculateMesh();
    }

    // Gets the indexes of the water chunk from it's name
    private void RecalculateIndexes()
    {
        string indexesString = name.Substring(11, name.Length-1-11);
        string[] twoIndexes = indexesString.Split(',');
        int x, y;
        int.TryParse(twoIndexes[0], out x);
        int.TryParse(twoIndexes[1], out y);
                
        _indexes = new Vector2Int(x,y);
    }

    // Creates the water chunks mesh
    private void CalculateMesh()
    {
        CheckReferences();
        
        // Calculate the offset (distance) between points
        float pointPositionOffset;
        if (_wrappingOfPoints % 2 == 0) // Special case for even numbers shouldn't be possible
        {
            pointPositionOffset = WATER_CHUNK_SIZE;
        }
        else
        {
            pointPositionOffset = WATER_CHUNK_SIZE / (_wrappingOfPoints - 1);
        }
        
        int numberOfVertices = _wrappingOfPoints * _wrappingOfPoints;
        int numberOfTriangles = (numberOfVertices - (_wrappingOfPoints + _wrappingOfPoints - 1)) * 2;

        // Arrays for storing the mesh
        Vector3[] vertices = new Vector3[numberOfVertices];
        Vector2[] uvCoords = new Vector2[numberOfVertices];
        int[] triangles = new int[numberOfTriangles * 3];
        int triangleIdx = 0;

        // Store neighbouring water chunks for edge connecting and normal correction
        WaterChunk[] neighbours = new WaterChunk[8];
        neighbours[0] = _generateWater.GetWaterChunk(_indexes.x - 1, _indexes.y + 1);       // Top left
        neighbours[1] = _generateWater.GetWaterChunk(_indexes.x, _indexes.y + 1);             // Top
        neighbours[2] = _generateWater.GetWaterChunk(_indexes.x + 1, _indexes.y + 1);       // Top right
        neighbours[3] = _generateWater.GetWaterChunk(_indexes.x - 1, _indexes.y);             // Middle left
        neighbours[4] = _generateWater.GetWaterChunk(_indexes.x + 1, _indexes.y);             // Middle right
        neighbours[5] = _generateWater.GetWaterChunk(_indexes.x - 1, _indexes.y - 1);       // Bottom left
        neighbours[6] = _generateWater.GetWaterChunk(_indexes.x, _indexes.y - 1);             // Bottom
        neighbours[7] = _generateWater.GetWaterChunk(_indexes.x + 1, _indexes.y - 1);       // Bottom right
        
        // Calculate integer division by 2 of wrapping points, used in calculation the offset 
        int wrapDiv2 = _wrappingOfPoints / 2;

        // Create vertices, faces and uv coords
        for (int vertexIdx = 0; vertexIdx < numberOfVertices; vertexIdx++) 
        {
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset = (vertexIdx % _wrappingOfPoints) - wrapDiv2;
            int yGridOffset = (vertexIdx / _wrappingOfPoints) - wrapDiv2;

            // Vertex x and z
            float x = xGridOffset * pointPositionOffset;
            float z = yGridOffset * pointPositionOffset;

            // Texture height calculation
            float height = GetHeightOfPoint(new Vector3(x, 0, z));

            height = ConnectEdges(vertexIdx, x, z, height, ref neighbours);
            
            Vector3 vertexPosition = new Vector3(x, height, z);
            vertices[vertexIdx] = vertexPosition;

            // UV coords
            uvCoords[vertexIdx] = new Vector2((x + WATER_CHUNK_SIZE/2) / WATER_CHUNK_SIZE, (z + WATER_CHUNK_SIZE/2) / WATER_CHUNK_SIZE);
            
            // Faces (only when not last row and not last column)
            if ((vertexIdx + 1) % _wrappingOfPoints != 0 && vertexIdx / _wrappingOfPoints != _wrappingOfPoints - 1)
            {
                // First face of quad 
                triangles[triangleIdx++] = vertexIdx;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints + 1;
                
                // Second face of quad
                triangles[triangleIdx++] = vertexIdx;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints + 1;
                triangles[triangleIdx++] = vertexIdx + 1;
            }
        }
        
        _meshFilter.sharedMesh = new Mesh {vertices = vertices, triangles = triangles, uv = uvCoords, name = this.name};
        RecalculateNormals();
        _meshFilter.sharedMesh.RecalculateBounds();
        _meshCollider.sharedMesh = _meshFilter.sharedMesh;
    }
    
    // Gets the height of a point in neighbour, used to connect edges of water chunks function
    private float ConnectEdges(int vertexIdx, float x, float z, float height, ref WaterChunk[] neighbours)
    {
        int numberOfVertices = _wrappingOfPoints * _wrappingOfPoints;
        
        // Top Left corner
        if (vertexIdx == 0)
        {
            if (neighbours[5] != null)
            {
                height = Mathf.Max(neighbours[5].GetHeightOfPoint(new Vector3(-x, 0, -z)), height);
            }
            if (neighbours[3] != null)
            {
                height = Mathf.Max(neighbours[3].GetHeightOfPoint(new Vector3(-x, 0, z)), height);
            }
            if (neighbours[6] != null)
            {
                height = Mathf.Max(neighbours[6].GetHeightOfPoint(new Vector3(x, 0, -z)), height);
            }
        }
        
        // Top Right corner
        else if (vertexIdx == _wrappingOfPoints - 1)
        {
            if (neighbours[7] != null)
            {
                height = Mathf.Max(neighbours[7].GetHeightOfPoint(new Vector3(-x, 0, -z)), height);
            }
            if (neighbours[4] != null)
            {
                height = Mathf.Max(neighbours[4].GetHeightOfPoint(new Vector3(-x, 0, z)), height);
            }
            if (neighbours[6] != null)
            {
                height = Mathf.Max(neighbours[6].GetHeightOfPoint(new Vector3(x, 0, -z)), height);
            }
        }
        
        // Bottom left corner
        else if (vertexIdx == (numberOfVertices - 1) - (_wrappingOfPoints - 1))
        {
            if (neighbours[0] != null)
            {
                height = Mathf.Max(neighbours[0].GetHeightOfPoint(new Vector3(-x, 0, -z)), height);
            }
            if (neighbours[3] != null)
            {
                height = Mathf.Max(neighbours[3].GetHeightOfPoint(new Vector3(-x, 0, z)), height);
            }
            if (neighbours[1] != null)
            {
                height = Mathf.Max(neighbours[1].GetHeightOfPoint(new Vector3(x, 0, -z)), height);
            }
        }
        
        // Bottom Right corner
        else if (vertexIdx == numberOfVertices - 1)
        {
            // Bottom right
            if (neighbours[2] != null)
            {
                height = Mathf.Max(neighbours[2].GetHeightOfPoint(new Vector3(-x, 0, -z)), height);
            }
            if (neighbours[4] != null)
            {
                height = Mathf.Max(neighbours[4].GetHeightOfPoint(new Vector3(-x, 0, z)), height);
            }
            if (neighbours[1] != null)
            {
                height = Mathf.Max(neighbours[1].GetHeightOfPoint(new Vector3(x, 0, -z)), height);
            }
        }
        
        // Bottom
        else if (vertexIdx < _wrappingOfPoints)
        {
            if (neighbours[6] != null)
            {
                height = Mathf.Max(neighbours[6].GetHeightOfPoint(new Vector3(x, 0, -z)), height);
            }
        }
        
        // Top
        else if (vertexIdx >= numberOfVertices - _wrappingOfPoints)
        {
            if (neighbours[1] != null)
            {
                height = Mathf.Max(neighbours[1].GetHeightOfPoint(new Vector3(x, 0, -z)), height);
            }
        }
        
        // Right
        else if (vertexIdx % _wrappingOfPoints == _wrappingOfPoints - 1)
        {
            if (neighbours[4] != null)
            {
                height = Mathf.Max(neighbours[4].GetHeightOfPoint(new Vector3(-x, 0, z)), height);
            }
        }
        
        // Right
        else if (vertexIdx % _wrappingOfPoints == 0)
        {
            if (neighbours[3] != null)
            {
                height = Mathf.Max(neighbours[3].GetHeightOfPoint(new Vector3(-x, 0, z)), height);
            }
        }

        return height;
    }

    // Recalculates the normal vectors of the mesh using
    private void RecalculateNormals()
    {
        Vector3[] vertices = _meshFilter.sharedMesh.vertices;
        Vector3[] vertexNormals = new Vector3[_wrappingOfPoints * _wrappingOfPoints];
        int triangleCount = (_wrappingOfPoints - 1) * (_wrappingOfPoints - 1) * 2;
        
        // Calculation is:   points in one row * faces * four sides + corner faces
        int numberOfBorderTriangles = (_wrappingOfPoints - 1) * 2 * 4 + 8;
        int borderWrapping = _wrappingOfPoints + 2;
        int borderWrapDiv2 = borderWrapping / 2;
            
        // Calculate the offset (distance) between points
        float pointPositionOffset;
        if (borderWrapping % 2 == 0) // Special case for even numbers shouldn't be possible
        {
            pointPositionOffset = WATER_CHUNK_SIZE + (WATER_CHUNK_SIZE/_wrappingOfPoints) * 2;
        }
        else
        {
            pointPositionOffset = (WATER_CHUNK_SIZE + (WATER_CHUNK_SIZE/(_wrappingOfPoints-1)) * 2)  / (borderWrapping - 1) ;
        }

        // Loop through faces including an outer layer (to calculate normals without seams)
        int triangleCountWithBorders = triangleCount + numberOfBorderTriangles;
        for (int triangleIdx = 0; triangleIdx < triangleCountWithBorders; triangleIdx++)
        {
            int triangleRowIdx = triangleIdx / ((borderWrapping-1) * 2);
            
            int vertexIdx1;
            int vertexIdx2;
            int vertexIdx3;
            
            // Odd Triangles
            if (triangleIdx % 2 == 0)
            {
                vertexIdx1 = triangleIdx / 2 + triangleRowIdx;
                vertexIdx2 = vertexIdx1 + borderWrapping;
                vertexIdx3 = vertexIdx2 + 1;
            }
            // Even Triangles
            else
            {
                vertexIdx1 = (triangleIdx - 1) / 2 + triangleRowIdx;
                vertexIdx2 = vertexIdx1 + 1;
                vertexIdx3 = vertexIdx2 + borderWrapping;
            }

            // ----------------------------------------------- Vertex 1 -----------------------------------------------
            
            // Coordinates in the grid from [0,0] to [wrapping+2, wrapping+2]
            int xGridOffset1 = (vertexIdx1 % borderWrapping) - borderWrapDiv2;
            int yGridOffset1 = (vertexIdx1 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of vertices
            float x1 = xGridOffset1 * pointPositionOffset;
            float z1 = yGridOffset1 * pointPositionOffset;
            

            // If vertex1 isn't a border point
            int vertIdxModulo = vertexIdx1 % borderWrapping;
            int vertexRowIdx = vertexIdx1 / borderWrapping;

            bool vertex1Inside = vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1;

            Vector3 vertex1 = vertex1Inside ?
                vertices[vertexIdx1 - borderWrapping - (vertexRowIdx * 2 - 1)]
                : new Vector3(x1, GetHeightOfPoint(new Vector3(x1,0,z1)), z1);
            
            
            // ----------------------------------------------- Vertex 2 -----------------------------------------------
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset2 = (vertexIdx2 % borderWrapping) - borderWrapDiv2;
            int yGridOffset2 = (vertexIdx2 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of vertices
            float x2 = xGridOffset2 * pointPositionOffset;
            float z2 = yGridOffset2 * pointPositionOffset;
            
            // If vertex2 isn't a border point
            vertIdxModulo = vertexIdx2 % borderWrapping;
            vertexRowIdx = vertexIdx2 / borderWrapping;

            bool vertex2Inside = vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1;

            Vector3 vertex2 = vertex2Inside ?
                vertices[vertexIdx2 - borderWrapping - (vertexRowIdx * 2 - 1)]
                : new Vector3(x2, GetHeightOfPoint(new Vector3(x2,0,z2)), z2);
            
            
            // ----------------------------------------------- Vertex 3 -----------------------------------------------
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset3 = (vertexIdx3 % borderWrapping) - borderWrapDiv2;
            int yGridOffset3 = (vertexIdx3 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of verticies
            float x3 = xGridOffset3 * pointPositionOffset;
            float z3 = yGridOffset3 * pointPositionOffset;
            
            // If vertex3 isn't a border point
            vertIdxModulo = vertexIdx3 % borderWrapping;
            vertexRowIdx = vertexIdx3 / borderWrapping;

            bool vertex3Inside = vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1;

            Vector3 vertex3 = vertex3Inside ?
                vertices[vertexIdx3 - borderWrapping - (vertexRowIdx * 2 - 1)]
                : new Vector3(x3, GetHeightOfPoint(new Vector3(x3,0,z3)), z3);

            
            // Get normal vector of vertices
            Vector3 normalVector = GetUpNormalFromPoints(vertex1, vertex2, vertex3);

            
            // If vertex1 isn't a border point
            vertexRowIdx = vertexIdx1 / borderWrapping;
            
            if (vertex1Inside)
            {
                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx1 - borderWrapping - (vertexRowIdx * 2 - 1);
                
                vertexNormals[oldIdx] += normalVector;
            }
            
            // If vertex2 isn't a border point
            vertexRowIdx = vertexIdx2 / borderWrapping;
            if (vertex2Inside)
            {
                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx2 - borderWrapping - (vertexRowIdx * 2 - 1);

                vertexNormals[oldIdx] += normalVector;
            }
            
            // If vertex3 isn't a border point
            vertexRowIdx = vertexIdx3 / borderWrapping;
            if (vertex3Inside)
            {
                int oldIdx = vertexIdx3 - borderWrapping - (vertexRowIdx * 2 - 1);

                vertexNormals[oldIdx] += normalVector;
            }
        }

        // Normalize all normal vectors
        for (int i = 0; i < vertices.Length; i++)
        {
            vertexNormals[i] = vertexNormals[i].normalized;
        }

        _meshFilter.sharedMesh.normals = vertexNormals;
    }

    // Calculates the normal vector of a triangle made up from 3 vertices, considers only up facing normal vectors
    private Vector3 GetUpNormalFromPoints(Vector3 point1, Vector3 point2, Vector3 point3)
    {
        Vector3 fromOneToTwo = point2 - point1;
        Vector3 fromOneToThree = point3 - point1;

        Vector3 normalVector = Vector3.Cross(fromOneToTwo, fromOneToThree).normalized;
        return normalVector.y >= 0 ? normalVector : -normalVector;
    }

    //-----------------------------------------------------Chunk-Editing------------------------------------------------
    // Updates the chunk and recalculates height
    public void UpdateParameters(ChunkManager chunkManager, GenerateWater generateWater, float waterLevel, float waterYMaxLevel, Material material)
    {
        CheckReferences();
        
        _chunkManager = chunkManager;
        _generateWater = generateWater;
        _waterMaxYOffset = waterYMaxLevel;
        _waterLevel = waterLevel;
        _meshRenderer.material = material;
        
        _updateNextFrame = true;
    }
    
    // Updates height of vertices based on chunks that change height (effectors)
    private void UpdateHeight()
    {
        // Neighbour chunks
        WaterChunk[] neighbours = new WaterChunk[8];
        neighbours[0] = _generateWater.GetWaterChunk(_indexes.x - 1, _indexes.y + 1);       // Top left
        neighbours[1] = _generateWater.GetWaterChunk(_indexes.x, _indexes.y + 1);             // Top
        neighbours[2] = _generateWater.GetWaterChunk(_indexes.x + 1, _indexes.y + 1);       // Top right
        neighbours[3] = _generateWater.GetWaterChunk(_indexes.x - 1, _indexes.y);             // Middle left
        neighbours[4] = _generateWater.GetWaterChunk(_indexes.x + 1, _indexes.y);             // Middle right
        neighbours[5] = _generateWater.GetWaterChunk(_indexes.x - 1, _indexes.y - 1);       // Bottom left
        neighbours[6] = _generateWater.GetWaterChunk(_indexes.x, _indexes.y - 1);             // Bottom
        neighbours[7] = _generateWater.GetWaterChunk(_indexes.x + 1, _indexes.y - 1);       // Bottom right
        
        Vector3[] vertices = _meshFilter.sharedMesh.vertices;

        int numberOfVertices = _wrappingOfPoints * _wrappingOfPoints;
        for (int vertexIdx = 0; vertexIdx < numberOfVertices; vertexIdx++)
        {
            Vector3 point = vertices[vertexIdx];

            // height calculation from texture
            float height = GetHeightOfPoint(point);

            height = ConnectEdges(vertexIdx, point.x, point.z, height, ref neighbours);
            
            point.y = height;
            vertices[vertexIdx] = point;
        }
        
        _meshFilter.sharedMesh.vertices = vertices;
        RecalculateNormals();
        _meshFilter.sharedMesh.RecalculateBounds();
        _meshCollider.sharedMesh = _meshFilter.sharedMesh;
        
    }

    // Changes the number of vertices per line in a chunk and recalculates the chunk
    public void ChangeWrapping(int newWrapping)
    {
        if (newWrapping % 2 != 1)
        {
            newWrapping += 1;
        }
        _wrappingOfPoints = newWrapping;
        CalculateMesh();
    }

    // Calculates height of point from X and Z (uses Perlin noise, Height effector chunks, path changes, road changes and hydraulic erosion changes)
    private float GetHeightOfPoint(Vector3 point)
    {
        Vector2 pointXZ = new Vector2(point.x, point.z);
        
        float height = _waterLevel;
        
        // --------------Texture coordinates calculation
        Vector2 textureCoordsFloat = pointXZ;
        textureCoordsFloat /= (WATER_CHUNK_SIZE / 2);
        textureCoordsFloat += Vector2.one;
        textureCoordsFloat /= 2;
        
        textureCoordsFloat.x = Mathf.Clamp(textureCoordsFloat.x, 0, 1);
        textureCoordsFloat.y = Mathf.Clamp(textureCoordsFloat.y, 0, 1);
        textureCoordsFloat *= WATER_RESOLUTION;
        int texX = Mathf.Clamp(Mathf.RoundToInt(textureCoordsFloat.x), 0, WATER_RESOLUTION-1);
        int texY = Mathf.Clamp(Mathf.RoundToInt(textureCoordsFloat.y), 0, WATER_RESOLUTION-1);
        
        // ----------------Height offset water texture
        if (_waterTexture != null)
        {
            Color pixelWater = _waterTexture.GetPixel(texX, texY);
            float lerp = EaseOutQuint(Mathf.Clamp(pixelWater.a * 3,0,1));
            height += (pixelWater.g) * _waterMaxYOffset * lerp;
        }

        if (height < _waterLevel) height = _waterLevel;
        
        return height;
    }
    
    // Taken from https://easings.net/en#easeInOutCubic
    private float EaseOutQuint(float x){
        return 1 - Mathf.Pow(1 - x, 5);
    }

    // ----------------------------------------Water-Utils--------------------------------------------------------------
    // Sets the water parameters and makes the water chunk update next frame    
    public void SetWaterParameters(float newWaterYOffset, float newWaterMaxYOffset)
    {
        _waterYOffset = newWaterYOffset;
        _waterMaxYOffset = newWaterMaxYOffset;
        _updateNextFrame = true;
    }
    
    // Sets the water texture of the water chunk and makes the water chunk update next frame
    public void SetWaterTexture(Texture2D newWaterTexture)
    {
        _waterTexture = newWaterTexture;
        
        if (newWaterTexture == null)
        {
            _waterTexture = GetEmptyWaterTexture();   
        }  
        
        _waterTexture.filterMode = FilterMode.Bilinear;
        _waterTexture.wrapMode = TextureWrapMode.Clamp;
        _waterTexture.name = _parentChunk.name + "WaterTex";
        _waterTexture.Apply();
        
        if (_meshRenderer == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
        }
        
        if (_materialPropertyBlock == null)
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
        }

        int idxWater = Shader.PropertyToID("_WaterTexture");
        _materialPropertyBlock.SetTexture(idxWater, _waterTexture);
        _meshRenderer.SetPropertyBlock(_materialPropertyBlock);
        
        _updateNextFrame = true;
    }
    

    // -------------------------------------Texture-Loading-Utils--------------------------------------------
    // Loads a water texture
    private Texture2D LoadTexture()
    {
        string textureFile =  _parentChunk.name + "WaterTex";
        int textureResolution = WATER_RESOLUTION;
        
        byte[] bytes;

        try
        {
            string pathWithFolder = Application.dataPath + "/ChunkTextures/" + SceneManager.GetActiveScene().name + "/" + WATER_FOLDER;
            if (!System.IO.Directory.Exists(pathWithFolder)) System.IO.Directory.CreateDirectory(pathWithFolder);
            bytes = System.IO.File.ReadAllBytes(pathWithFolder + textureFile + ".png");
        }
        catch 
        {
            return null;
        }
        
        Texture2D tex = new Texture2D(textureResolution,textureResolution,  TextureFormat.RGBAFloat, true);
        tex.LoadImage(bytes);
        tex.Apply();
        
        return tex;
    }

    // Creates a 1 by 1 placeholder texture to reset water texture
    private Texture2D GetEmptyWaterTexture()
    {
        Texture2D newWaterTex = new Texture2D(1,1, TextureFormat.RGBAFloat, true);

        Color pixel = new Color(0,0,0,0);

        newWaterTex.filterMode = FilterMode.Bilinear;
        newWaterTex.wrapMode = TextureWrapMode.Clamp;
        newWaterTex.SetPixel(0,0, pixel);
        newWaterTex.name = _parentChunk.name + "WaterTex";
        newWaterTex.Apply();

        return newWaterTex;
    }
    
    // -----------------------------------------------Chunk-Utils-------------------------------------------------------
    // When opening the project or entering play mode references are not kept
    private void CheckReferences()
    {
        if (_meshRenderer == null)
        { 
            _meshRenderer = GetComponent<MeshRenderer>();
        }
        
        if (_meshFilter == null)
        {
            _meshFilter = GetComponent<MeshFilter>();
        }

        if (_meshCollider == null)
        {
            _meshCollider = GetComponent<MeshCollider>();
        }
        
        if (_chunkManager == null)
        {
            _chunkManager = GetComponentInParent<ChunkManager>();
            RecalculateIndexes();
        }

        if (_generateWater == null)
        {
            transform.parent.parent.parent.GetComponentInChildren<GenerateWater>();
            _wrappingOfPoints = _generateWater.GetWaterChunkWrapping();
        }

        if (_wrappingOfPoints == 0)
        {
            _wrappingOfPoints = _generateWater.GetWaterChunkWrapping();
        }

        if (_parentChunk == null)
        {
            _parentChunk = transform.parent.GetComponent<Chunk>();
        }
        
        if (_materialPropertyBlock == null)
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
        }

        if (_waterTexture == null)
        {
            Texture2D newWaterTex = LoadTexture();
            SetWaterTexture(newWaterTex);
        }
    }
}
