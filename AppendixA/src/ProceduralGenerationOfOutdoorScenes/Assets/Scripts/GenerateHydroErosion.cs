using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class GenerateHydroErosion : MonoBehaviour
{
    [SerializeField] private int dropType = 0;      // 0 is for random and 1 for grid droplet position
    [SerializeField] private float maxAngle = 75f;
    [SerializeField] private int maxStepsUntilFail = 60;
    [SerializeField] [Range(0, 1)] private float inertia = 0.1f;
    [SerializeField] [Range(1,30)] private int numberOfDropsToSimulatePerChunk = 7;
    [SerializeField] private float dropletWidth = 3f;

    [SerializeField] [Range(0, 40)] private float erosionStrength = 20;

    private readonly List<Droplet> _controlPoints = new List<Droplet>();

    private ChunkManager _chunkManager;
    private bool _isFreshlyOpened = true;
    
    // Constants
    private const float TRAVEL_DISTANCE = 1f;
    private const float ERROR_CHECK_DEPOSIT_DISTANCE = 0.5f;
    private const int EROS_RESOLUTION = 128;

    // Struct to store information about a droplet in time
    struct Droplet
    {
        public readonly Vector3 Pos;
        public readonly Chunk Chunk;
        public Vector2 TEXCoord;
        public readonly float DeltaSediment;

        public Droplet(Vector3 position, Chunk givenChunk, Vector2 tex, float deltaSedimentGiven)
        {
            Pos = position;
            Chunk = givenChunk;
            TEXCoord = tex;
            DeltaSediment = deltaSedimentGiven;
        }
    }
    
    // --------------------------------Mono-behaviour-------------------------------------------------------------------
    private void Start()
    {
        // References are not kept upon entering playmode/project open
        if (_isFreshlyOpened)
        {
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
            _chunkManager.UpdateErosionParameters(erosionStrength);
            _isFreshlyOpened = false;
        }
    }

    // On Inspector change
    private float _lastErosionStrength;
    private void OnValidate()
    {
        if (_chunkManager == null) _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();

        // Update erosion upon strength change
        if (_lastErosionStrength != erosionStrength)
        {
            _chunkManager.UpdateErosionParameters(erosionStrength);
            _lastErosionStrength = erosionStrength;
        }

        // only odd same as chunk wrapping
        if (numberOfDropsToSimulatePerChunk % 2 != 1)
        {
            numberOfDropsToSimulatePerChunk -= 1;
        }
    }

    // --------------------------------Hydro-Erosion-Utils--------------------------------------------------------------
    // Resets all the testing points
    public void ResetControlPointsButton()
    {
        _controlPoints.Clear();
    }

    // Simulate a droplets movement / called by droplet testing button
    public void DropADroplet(Vector3 start)
    {
        Vector3 currentPosition = start;
        Vector3 prevDirection = Vector3.zero;
        Vector3 prevPosition = start;

        float amplitude = _chunkManager.GetAmplitude();
        
        // Constants for erosion
        float maxSpeed = 10;
        float speed = 10;
        float sediment = 0;
        float maxCapacity = amplitude/4; 
        float capacity = amplitude/4;   
        
        // Start going from given point down
        for (int i = 0; i < maxStepsUntilFail; i++)
        {
            // Raycast on the terrain
            RaycastHit hitInfo;
            if (!Physics.Raycast(currentPosition + Vector3.up * 500, Vector3.down, out hitInfo))
            {
                break;
            }

            currentPosition = hitInfo.point;
            
            // Angle calculation
            Vector3 rotationAxis = Vector3.Cross(hitInfo.normal, Vector3.up).normalized;
            Vector3 gradient = (Quaternion.AngleAxis(-90, rotationAxis) * hitInfo.normal).normalized;
            Vector3 currentDirection = gradient * (1 - inertia) + prevDirection * inertia;

            // When the droplet does a very sharp turn stop simulating
            Vector3 currDirXZ = currentDirection;
            currDirXZ.y = 0;
            prevDirection.y = 0;
            if (Vector3.Angle(prevDirection, currDirXZ) > maxAngle)
            {
                break;
            }
            
            // Sediment calculation
            float deltaHeight = prevPosition.y - currentPosition.y;
            float deltaSediment = deltaHeight * (speed/maxSpeed);

            // Start depositing if over capacity
            if (sediment >= capacity)
            {
                deltaSediment -= Mathf.Abs(sediment-capacity) * 0.01f;
            }
            
            // Water does not flow upwards so start depositing
            if (deltaHeight <= ERROR_CHECK_DEPOSIT_DISTANCE)
            {
                deltaSediment -= Mathf.Min(Mathf.Abs(deltaHeight), sediment) * 0.01f;
            }
            
            // Add the delta sediment to the current one
            sediment += deltaSediment;
            
            // Can only have positive sediment
            if (sediment < 0)
            {
                sediment = 0;
            }
            
            // Speed update based on gravity
            speed = Mathf.Sqrt(Mathf.Max(0,speed * speed - deltaHeight * 4f));
            
            // When there is droplet doesn't have enough speed it stops simulating
            if (speed <= 0.1f)
            {
                break;
            }

            // Update capacity (the less speed the droplet has the lower is it's capacity)
            capacity = maxCapacity * (speed / maxSpeed);
            
            // Update direction and previous position
            prevDirection = currentDirection;
            prevPosition = currentPosition;
            currentPosition += currentDirection.normalized * TRAVEL_DISTANCE;

            // Terrain changes inversely of the amount of sediment gained/lost
            deltaSediment = -deltaSediment;

            // Divide by capacity to normalize it for texture drawing
            deltaSediment /= Mathf.Max(capacity, 1f);

            // First point doesn't carry any change of height
            if (i == 0) deltaSediment = 0;
            
            // Smooth out a bit at start
            if (i == 1) deltaSediment /= 2;

            Droplet droplet = new Droplet(hitInfo.point, hitInfo.collider.GetComponent<Chunk>(), hitInfo.textureCoord, deltaSediment);
            if (droplet.Chunk == null) return;
            _controlPoints.Add(droplet);
        }
    }
    
    // --------------------------------------Texture-Utils--------------------------------------------------------------
    // Generates the hydraulic erosion
    public void GenerateErosionButton()
    {
        // Reset testing points
        ResetControlPointsButton();

        transform.parent.GetComponentInChildren<GenerateWater>()?.DisableAllWaterChunks();
        
        // Brush width setup
        int widthOfBrush = (int)((1.0f / _chunkManager.GetChunkSize()) * dropletWidth * EROS_RESOLUTION);
        int widthOfBrushTexSpaceDiv2 = widthOfBrush / 2;
        
        // Size of one pixel in world units
        float pixelInWorld = _chunkManager.GetChunkSize() / EROS_RESOLUTION;

        // Store all chunks with their texture
        Dictionary<Chunk, Texture2D> chunkTextures = new Dictionary<Chunk, Texture2D>();
        ICollection allChunks = _chunkManager.GetAllChunksTotal();
        foreach (Chunk chunk in allChunks)
        {
            if (chunk == null) continue;
            Texture2D texture2D = chunk.GetErosionTexture();
            
            // Create a new texture when resolution doesn't match
            if (texture2D.width != EROS_RESOLUTION)
            {
                texture2D = CreateNewTexture(EROS_RESOLUTION, chunk.name);
            }
            chunkTextures.Add(chunk, texture2D);
        }

        // Cross drawing utils
        Texture2D[] neighborTextures = new Texture2D[9];

        float chunkSize = _chunkManager.GetChunkSize();
        float chunkSizeHalf = _chunkManager.GetChunkSize() / 2;

        // for each chunk
        foreach (Chunk chunk in allChunks)
        {
            Vector3 chunkPosition = chunk.transform.position;
            int doubleDrop = numberOfDropsToSimulatePerChunk * numberOfDropsToSimulatePerChunk;
            
            // Randomly generate number of started points
            for (int i = 0; i < doubleDrop; i++)
            {
                Vector3 position;
                if (dropType == 0)
                {
                    position = chunkPosition + Vector3.forward * (chunkSize * (((i%numberOfDropsToSimulatePerChunk)-numberOfDropsToSimulatePerChunk/2) / (float)numberOfDropsToSimulatePerChunk))
                                                     + Vector3.right * (chunkSize * (((i/numberOfDropsToSimulatePerChunk)-numberOfDropsToSimulatePerChunk/2) / (float)numberOfDropsToSimulatePerChunk));
                }
                else
                {
                    position = chunkPosition + Vector3.forward * (chunkSize * Random.value - chunkSizeHalf)
                                             + Vector3.right * (chunkSize * Random.value - chunkSizeHalf);
                }
                
                // Drop a droplet
                DropADroplet(position);

                if (_controlPoints.Count < 4)
                {
                    ResetControlPointsButton();
                    continue;
                }

                // Build neighboring textures and draw on textures
                BuildNeighborTextures(ref neighborTextures, ref chunkTextures, _controlPoints[0].Chunk);
                for (int o = 0; o < _controlPoints.Count - 1; o++)
                {
                    Droplet droplet1 = _controlPoints[o];
                    Droplet droplet2 = _controlPoints[o + 1];
                    
                    DrawOnDropletPath(ref droplet1, ref droplet2);
                    
                    // Entering a new chunk -> rebuild neighbor textures
                    if (droplet1.Chunk != droplet2.Chunk)
                    {
                        BuildNeighborTextures(ref neighborTextures, ref chunkTextures, droplet2.Chunk);
                    }
                }

                ResetControlPointsButton();
            }

            transform.parent.GetComponentInChildren<GenerateWater>()?.EnableAllWaterChunks();

            // Apply all changed textures
            foreach (var chunkTexturePair in chunkTextures)
            {
                Texture2D tex = chunkTexturePair.Value;
                tex.Apply();
                chunkTexturePair.Key.NotifyChunkToUpdateOnce();
                chunkTexturePair.Key.SetErosionTexture(tex, true);
            }
        }
        
        // Function to draw (inside this func not to pass too many variables)-----------------------------
        void DrawOnDropletPath(ref Droplet start, ref Droplet end)
        {
            // Create a texture field instead of a single texture (on edges of chunk there might be a max of 3 textures to draw in)
            Texture2D[] texturesToDrawIn = new Texture2D[3];
            
            // Add texture resolution to interpolate texcoords
            if (start.Chunk != end.Chunk)
            {
                TransformCoords(ref start, ref end);
            }
            
            Vector3 currPoint = start.Pos;
            float currToEndDistance = Vector3.Distance(start.Pos, end.Pos);
            float startToEndDist = currToEndDistance;
            Vector3 direction = (end.Pos - start.Pos).normalized;

            
            // Draw on texture moving by one pixel at a time 
            while (currToEndDistance >= pixelInWorld)
            {
                // Get world coords to texture coords
                Vector2 texPosFloat = Vector2.Lerp(start.TEXCoord, end.TEXCoord, 1 - currToEndDistance/startToEndDist) * EROS_RESOLUTION;
                Vector2Int texPosInt = new Vector2Int(Mathf.RoundToInt(texPosFloat.x), Mathf.RoundToInt(texPosFloat.y));

                // Set new color as a lerp between new and old color
                float sedimentValue = Mathf.Lerp(start.DeltaSediment, end.DeltaSediment,
                    currToEndDistance / startToEndDist);

                // Deposit in a larger area
                int tempWidthDiv2 = widthOfBrushTexSpaceDiv2;
                if (sedimentValue > 0)
                {
                    tempWidthDiv2 = tempWidthDiv2 * 2;
                    sedimentValue = sedimentValue / 4;
                }
                
                // Draw to texture
                int startX = texPosInt.x - tempWidthDiv2;
                int endX = texPosInt.x + tempWidthDiv2;
                int startY = texPosInt.y - tempWidthDiv2;
                int endY = texPosInt.y + tempWidthDiv2;
                for (int x = startX; x <= endX; x++)
                {
                    for (int y = startY; y <= endY; y++)
                    {
                        // optimization
                        float angle = Vector2.SignedAngle((new Vector2(x, y) - texPosFloat), new Vector2(direction.x, direction.z));
                        bool isOnPath =  (angle < 120 && angle > 60) || (angle < -60 && angle > -120);
                        
                        // Edge case where we need to duplicate the pixels on seam
                        int distX = x;
                        int distY = y;
                        if (distX < 0) distX++;
                        if (distX >= EROS_RESOLUTION) distX--;
                        if (distY < 0) distY++;
                        if (distY >= EROS_RESOLUTION) distY--;
                        float distanceFromPath = Vector2.Distance(texPosInt, new Vector2(distX, distY));
                        
                        // Skip irrelevant pixels
                        if ((currPoint != start.Pos && currToEndDistance >= pixelInWorld * 2 && 1 - (distanceFromPath/ tempWidthDiv2) < 0.5f) && !isOnPath) continue;
                        
                        int tempX = x;
                        int tempY = y;

                        texturesToDrawIn[0] = null;
                        texturesToDrawIn[1] = null;
                        texturesToDrawIn[2] = null;
                        Vector2Int xy1 = new Vector2Int(x, y);
                        Vector2Int xy2 = new Vector2Int(x, y);
                        
                        // Sets the right textures and coordinates for drawing
                        GetRightTextureForCoords(ref tempX, ref tempY, ref neighborTextures, ref texturesToDrawIn, ref xy1, ref xy2);
                        if (texturesToDrawIn[0] == null && texturesToDrawIn[1] == null && texturesToDrawIn[2] == null) continue;

                        // Is 0 when close to path 1 when on edge of path
                        float tDistFromPath = distanceFromPath / tempWidthDiv2;
                        tDistFromPath = 1 - Mathf.Clamp(tDistFromPath, 0, 1);

                        // Create a new color
                        Color lastColor = texturesToDrawIn[0].GetPixel(tempX, tempY);
                        Color newColor = Color.gray + Color.gray * sedimentValue * tDistFromPath * 5;
                        newColor.a = tDistFromPath ;

                        // Do not overdraw if previous color has higher alpha
                        if (lastColor.a > newColor.a)
                        {
                            newColor = lastColor;
                        }

                        // Draw in chunk textures
                        texturesToDrawIn[0].SetPixel(tempX, tempY,  newColor);
                        
                        // Edge cases on chunk edges and corners
                        if (texturesToDrawIn[1] != null) texturesToDrawIn[1].SetPixel(xy1.x, xy1.y,  newColor);
                        if (texturesToDrawIn[2] != null) texturesToDrawIn[2].SetPixel(xy2.x, xy2.y,  newColor);
                    }
                }
                
                // Move towards end point
                currPoint += direction * pixelInWorld;
                currToEndDistance = Vector3.Distance(currPoint, end.Pos);
            }
        }
    }

    // Builds the neighboring textures for the current chunk
    private void BuildNeighborTextures(ref Texture2D[] neighborTextures, ref Dictionary<Chunk, Texture2D> chunkTextures, Chunk chunk)
    {
        Vector2Int indices = chunk.GetIndices();
        Chunk tempChunk = null;
        
        // Top left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y + 1);
        if (tempChunk) neighborTextures[0] = chunkTextures[tempChunk];
        else neighborTextures[0] = null;

        // Top middle
        tempChunk = _chunkManager.GetChunk(indices.x, indices.y + 1);
        if (tempChunk) neighborTextures[1] = chunkTextures[tempChunk];
        else neighborTextures[1] = null;

        // Top right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y + 1);
        if (tempChunk) neighborTextures[2] = chunkTextures[tempChunk];
        else neighborTextures[2] = null;

        // Middle left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y);
        if (tempChunk) neighborTextures[3] = chunkTextures[tempChunk];
        else neighborTextures[3] = null;

        // Middle middle
        neighborTextures[4] = chunkTextures[chunk];

        // Middle right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y);
        if (tempChunk) neighborTextures[5] = chunkTextures[tempChunk];
        else neighborTextures[5] = null;

        // Bottom left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y - 1);
        if (tempChunk) neighborTextures[6] = chunkTextures[tempChunk];
        else neighborTextures[6] = null;

        // Bottom middle
        tempChunk = _chunkManager.GetChunk(indices.x, indices.y - 1);
        if (tempChunk) neighborTextures[7] = chunkTextures[tempChunk];
        else neighborTextures[7] = null;

        // Bottom right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y - 1);
        if (tempChunk) neighborTextures[8] = chunkTextures[tempChunk];
        else neighborTextures[8] = null;
    }

    // Transforms texture coordinates based on start and end chunks 
    private void TransformCoords(ref Droplet start, ref Droplet end)
    {
        Vector2 idxStart = start.Chunk.GetIndices();
        Vector2 idxEnd = end.Chunk.GetIndices();

        Vector2 diff = idxEnd - idxStart;

        end.TEXCoord += diff;
    }
    
    // Gets the right textures and coordinates for a given x and y.
    private void GetRightTextureForCoords(ref int x, ref int y, ref Texture2D[] neighborTextures, 
        ref Texture2D[] texturesToDrawIn, ref Vector2Int xy1, ref Vector2Int xy2)
    {
        // Top left
        if (x < 0 && y >= EROS_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[0];
            x += EROS_RESOLUTION;
            y -= EROS_RESOLUTION;
        }
        // Top middle
        else if (x >= 0 && x < EROS_RESOLUTION && y >= EROS_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[1];
            y -= EROS_RESOLUTION;
        }
        // Top right
        else if (x >= EROS_RESOLUTION && y >= EROS_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[2];
            x -= EROS_RESOLUTION;
            y -= EROS_RESOLUTION;
        }
        // Middle left
        else if (x < 0 && y >= 0 && y < EROS_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[3];
            x += EROS_RESOLUTION;
        }
        // Middle middle
        else if (x >= 0 && x < EROS_RESOLUTION && y >= 0 && y < EROS_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[4];
            
            // At edges we need to cross draw
            // Left side
            if (x == 0)
            {
                texturesToDrawIn[1] = neighborTextures[3];
                xy1.x = EROS_RESOLUTION - 1;
                xy1.y = y;
                
                // Bottom left
                if (y == 0)
                {
                    texturesToDrawIn[2] = neighborTextures[6];
                    xy2.x = EROS_RESOLUTION - 1;
                    xy2.y = EROS_RESOLUTION - 1;
                }
                // Top left
                else if (y == EROS_RESOLUTION - 1)
                {
                    texturesToDrawIn[2] = neighborTextures[0];
                    xy2.x = EROS_RESOLUTION - 1;
                    xy2.y = 0;
                }
            }

            // Right side
            else if (x == EROS_RESOLUTION-1)
            {
                texturesToDrawIn[1] = neighborTextures[5];
                xy1.x = 0;
                xy1.y = y;
                
                // Bottom right
                if (y == 0)
                {
                    texturesToDrawIn[2] = neighborTextures[8];
                    xy2.x = 0;
                    xy2.y = EROS_RESOLUTION - 1;
                }
                // Top right
                else if (y == EROS_RESOLUTION - 1)
                {
                    texturesToDrawIn[2] = neighborTextures[2];
                    xy2.x = 0;
                    xy2.y = 0;
                }
            }
            
            // Top side
            else if (y == EROS_RESOLUTION - 1)
            {
                texturesToDrawIn[1] = neighborTextures[1];
                xy1.x = x;
                xy1.y = 0;
            }
            
            // Bottom side
            else if (y == 0)
            {
                texturesToDrawIn[1] = neighborTextures[7];
                xy1.x = x;
                xy1.y = EROS_RESOLUTION - 1;
            }
        }
        // Middle right
        else if (x >= EROS_RESOLUTION && y >= 0 && y < EROS_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[5];
            x -= EROS_RESOLUTION;
        }
        // Bottom left
        else if (x < 0 && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[6];
            x += EROS_RESOLUTION;
            y += EROS_RESOLUTION;
        }
        // Bottom middle
        else if (x >= 0 && x < EROS_RESOLUTION && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[7];
            y += EROS_RESOLUTION;
        }
        // Bottom right
        else if (x >= EROS_RESOLUTION && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[8];
            x -= EROS_RESOLUTION;
            y += EROS_RESOLUTION;
        }
    }

    // Resets the erosion textures in all chunks
    public void ResetErosionButton()
    {
        _chunkManager.ResetErosionData();
    }
    
    // Creates a new Hydraulic erosion texture
    private Texture2D CreateNewTexture(int resolution, string chunkName)
    {
        Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBAFloat, true);
        int resPow2 = resolution * resolution;
        Color[] pixels = new Color[resPow2];
        
        for (int i = 0; i < resPow2; i++)
        {
            Color pixel = Color.gray;
            pixel.a = 0;
            pixels[i] = pixel;
        }
        
        tex.SetPixels(pixels);

        tex.filterMode = FilterMode.Bilinear;
        tex.wrapMode = TextureWrapMode.Clamp;
        tex.alphaIsTransparency = true;
        tex.name = chunkName + "erosTex";
        
        tex.Apply();

        return tex;
    }

    // Draws gizmos to show the testing hydraulic points
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        foreach (Droplet point in _controlPoints)
        {
            // Color based on sediment
            if (point.DeltaSediment > 0)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(point.Pos, Mathf.Max(point.DeltaSediment*10, 0.1f));
                
            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(point.Pos, Mathf.Max(-point.DeltaSediment*10, 0.1f));

            }
        }
    }
}
