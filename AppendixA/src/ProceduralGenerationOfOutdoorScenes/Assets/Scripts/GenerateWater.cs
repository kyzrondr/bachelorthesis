using System;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class GenerateWater : MonoBehaviour
{
    [SerializeField] private List<Vector3> controlPointsList = new List<Vector3>();
    [SerializeField] private bool showControlPoints = true;
    [SerializeField] private float travelDistance = 2f;
    [SerializeField] private int maxStepsUntilFail = 100;
    [SerializeField] [Range(0.1f, 5f)] private float targetWeight = 2f;
    [SerializeField] [Range(0.1f, 5f)] private float heightDifferenceWeight = 3f;
    [SerializeField] [Range(0.1f, 5f)] private float gradientWeight = 4f;
    [SerializeField] [Range(0.1f, 5f)] private float sameDirectionWeight = 1f;

    [SerializeField] private float waterStartWidth = 4f;
    [SerializeField] private float waterEndWidth = 10f;

    [SerializeField] private float waterMaxYOffset = 100f;
    [SerializeField] private float waterLevel = -0.2f;
    [SerializeField] private float carveDepth = -0.2f;
    [SerializeField] private float riverStartDepth = -1f;
    [SerializeField] private float riverEndDepth = -1f;
    [SerializeField] private float waterLevelStart = 0.5f;
    [SerializeField] private float waterLevelEnd = 0.5f;
    [SerializeField] private float underWaterLevelDepth = -0.5f;

    [SerializeField] private int waterWrapping = 15;
    [SerializeField] public Material material;

    private readonly Hashtable _waterChunksTable = new Hashtable(); // Name => WaterChunk
    private bool _isWaterGenerated = false;
    private bool _checkForWaterChunks = true;
    
    // The inner representation of the controlPointsList, used so that I can easily insert points between other points
    private LinkedList<Vector3> _controlPointsLinkedList = new LinkedList<Vector3>();
    private ChunkManager _chunkManager;
    private bool _isNotFreshlyOpened;
    private bool _postedWrappingUpdate;
    private bool _postedParamUpdate;

    // Constant
    private const int WATER_RESOLUTION = 128;

    // --------------------------------Mono-behaviour-------------------------------------------------------------------
    private void Start()
    {
        // References and private variables are not kept on project reload or entering play mode
        if (!_isNotFreshlyOpened)
        {
            RecalculateWaterChunkWrapping();
            ReAddWaterChunks();
            UpdateParametersOfWaterChunks();
            
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
            _isWaterGenerated = _chunkManager.GetComponentInChildren<Chunk>()?.GetComponent<WaterChunk>() != null;
            _isNotFreshlyOpened = true;
        }
        
        // Re add water chunks
        if (_checkForWaterChunks)
        {
            ReAddWaterChunks();
        }
    }

    private void Update()
    {
        // If wrapping update was posted, recalculate the water chunk wrapping
        if (_postedWrappingUpdate)
        {
            _postedWrappingUpdate = false;

            if (_waterChunksTable.Count == 0)
            {
                ReAddWaterChunks();
                if (_waterChunksTable.Count == 0)
                {
                    return;
                }
            }
            RecalculateWaterChunkWrapping();
        }
        
        // If parameter update was posted, update the water chunks heights
        if (_postedParamUpdate)
        {
            _postedParamUpdate = false;

            if (_waterChunksTable.Count == 0)
            {
                ReAddWaterChunks();
                if (_waterChunksTable.Count == 0)
                {
                    return;
                }
            }
            _chunkManager.UpdateWaterParameters(waterMaxYOffset);
            UpdateParametersOfWaterChunks();
        }
        
    }

    // Editor change
    private int _lastWrapping;
    private float _lastWaterLevel;
    private Material _lastMaterial;
    private void OnValidate()
    {        
        // Update control points
        _controlPointsLinkedList.Clear();
        foreach (Vector3 controlPoint in controlPointsList)
        {
            _controlPointsLinkedList.AddLast(controlPoint);
        }

        // Check references
        if (_chunkManager == null)
        {
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
        }

        // Update material
        if (_lastMaterial != material)
        {
            _lastMaterial = material;
            _postedParamUpdate = true;
        }

        // Update wrapping
        if (_lastWrapping != waterWrapping)
        {
            if (waterWrapping % 2 == 0) waterWrapping += 1;
            if (waterWrapping < 3) waterWrapping = 3;
            _lastWrapping = waterWrapping;
            _postedWrappingUpdate = true;
        }

        // Update water level
        if (_lastWaterLevel != waterLevel)
        {
            _lastWaterLevel = waterLevel;
            _postedParamUpdate = true;
        }
    }
    
    // --------------------------------Water-Chunk-Generation---------------------------------------------------------------------
    // Initializes the component
    public void Initialize(ChunkManager chunkManager)
    {
        _chunkManager = chunkManager;
    }
    
    // Generates a water chunk for each normal terrain chunk present
    public void GenerateWaterChunksButton()
    {
        _waterChunksTable.Clear();
        
        ICollection chunks = _chunkManager.GetAllChunks();

        foreach (Chunk chunk in chunks)
        {
            // Destroy previous water chunks
            WaterChunk prevChunk = chunk.GetComponentInChildren<WaterChunk>();
            if (prevChunk != null) DestroyImmediate(prevChunk.gameObject);
            
            GameObject waterChunk = new GameObject();
            waterChunk.transform.parent = chunk.transform;

            waterChunk.name = "Water" + chunk.name;
            WaterChunk waterChunkComponent = waterChunk.AddComponent<WaterChunk>();
            waterChunkComponent.Initialize(_chunkManager, this, chunk.GetIndices(),  waterLevel, waterWrapping, material);
            
            _waterChunksTable.Add(chunk.name, waterChunkComponent);
        }

        _isWaterGenerated = true;
        
    }

    // Deletes all generated water chunks
    public void DeleteWaterChunksButton()
    {
        _waterChunksTable.Clear();
        
        ICollection chunks = _chunkManager.GetAllChunks();

        foreach (Chunk chunk in chunks)
        {
            // Destroy water chunks
            WaterChunk waterChunk = chunk.GetComponentInChildren<WaterChunk>();
            if (waterChunk != null) DestroyImmediate(waterChunk.gameObject);
        }

        _isWaterGenerated = false;
    }
    
    // Checks the scenes for generated water chunks
    private void ReAddWaterChunks()
    {
        ICollection chunks = _chunkManager.GetAllChunks();
        
        foreach (Chunk chunk in chunks)
        {
            WaterChunk waterChunk = chunk.GetComponentInChildren<WaterChunk>();
            if (waterChunk != null && !_waterChunksTable.ContainsKey(waterChunk.transform.parent.name))
            {
                _waterChunksTable.Add(waterChunk.transform.parent.name, waterChunk);
            }
        }
                        
        // If water chunks were found set the isWaterGenerated
        if (_waterChunksTable.Count == 0)
        {
            _isWaterGenerated = false;
            _checkForWaterChunks = false;
        }
        else
        {
            _isWaterGenerated = true;
        }
    }
    
    // Generates a single water chunk based on the parent chunk
    public void GenerateWaterChunk(Chunk parentChunk)
    {
        // Destroy previous chunks
        WaterChunk prevChunk = parentChunk.GetComponentInChildren<WaterChunk>();
        if (prevChunk != null) DestroyImmediate(prevChunk.gameObject);
            
        GameObject waterChunk = new GameObject();
        waterChunk.transform.parent = parentChunk.transform;
        waterChunk.name = "Water" + parentChunk.name;
        WaterChunk waterChunkComponent = waterChunk.AddComponent<WaterChunk>();
        waterChunkComponent.Initialize(_chunkManager, this, parentChunk.GetIndices(), waterLevel, waterWrapping, material);
            
        _waterChunksTable.Add(parentChunk.name, waterChunkComponent);
    }
    
    // Retrieves the water chunk component of given chunk from hashtable
    public WaterChunk GetWaterChunk(int x, int y)
    {
        string chunkName = ChunkManager.CreateChunkName(x, y);
        if (_waterChunksTable.Contains(chunkName))
        {
            return (WaterChunk) _waterChunksTable[chunkName];
        }

        return null;
    }
    
    // Recalculate water chunk wrapping
    private void RecalculateWaterChunkWrapping()
    {
        int tempWrap = waterWrapping;

        if (waterWrapping % 2 != 1)
        {
            waterWrapping += 1;
            tempWrap += 1;
        }

        if (waterWrapping < 3)
        {
            waterWrapping = 3;
            tempWrap = 3;
        }

        foreach (WaterChunk waterChunk in _waterChunksTable.Values)
        {
            waterChunk.ChangeWrapping(tempWrap);
        }
    }
    
    // Goes through all the water chunks a notifies them to update their height
    private void UpdateParametersOfWaterChunks()
    {
        foreach (WaterChunk waterChunk in _waterChunksTable.Values)
        {
            if (waterChunk == null)
            {
                _waterChunksTable.Remove(waterChunk);
                continue;
            }
            waterChunk.UpdateParameters(_chunkManager, this, waterLevel, waterMaxYOffset, material);
        }
        
        _chunkManager.UpdateWaterParameters(waterMaxYOffset);
       
    }

    // --------------------------------Point-utils----------------------------------------------------------------------
    // Adds a control point to the list
    public void AddWaterPoint(Vector3 point)
    {
        _controlPointsLinkedList.AddLast(point);
        controlPointsList.Add(point);
    }

    // Copies the points from the linked list to the list
    private void CopyFromLinkedListToList()
    {
        controlPointsList.Clear();
        foreach (Vector3 controlPoint in _controlPointsLinkedList)
        {
            controlPointsList.Add(controlPoint);
        }
    }

    // Resets all the control points
    public void ResetControlPointsButton()
    {
        _controlPointsLinkedList.Clear();
        controlPointsList.Clear();
    }
    
    // Resets all the control points except the first and last one
    public void ResetControlPointsWithoutFirstLastButton()
    {
        if (controlPointsList.Count <= 0) return;
        if (_controlPointsLinkedList.Count <= 0) return;
        
        Vector3 first = _controlPointsLinkedList.First.Value;
        Vector3 last = _controlPointsLinkedList.Last.Value;
        _controlPointsLinkedList.Clear();
        controlPointsList.Clear();

        AddWaterPoint(first);
        AddWaterPoint(last);
    }

    //---------------------------River-Agent----------------------------------------------------------------------------
    // Sends an agent through the control points to better approximate the path using the least steepness
    public void ConnectRiverPointsButton()
    {
        // Path needs two points
        if (_controlPointsLinkedList.Count < 2) return;

        Vector3 startPosition = controlPointsList[0];        
        Vector3 currentPosition = startPosition;
        
        // New linked list for result
        LinkedList<Vector3> newControlPoints = new LinkedList<Vector3>();
        newControlPoints.AddLast(currentPosition);
        
        // Go through all the control points
        for (int i = 1 ; i < controlPointsList.Count; i++) 
        {
            Vector3 currentTarget = controlPointsList[i];
            Vector3 lastDirection = Vector3.zero;

            // Find points between two points
            bool foundSolution = false;
            for (int j = 0; j < maxStepsUntilFail; j++)
            {
                Vector3 newPoint = FindNextStep(currentTarget, currentPosition, lastDirection);
                if (newPoint == Vector3.zero) break;

                // Adds the new point to new control points
                newControlPoints.AddLast(newPoint);
                lastDirection = newPoint - currentPosition;
                currentPosition = newPoint;
                
                // Break if too close to control point (control point == next Point)
                if (Vector3.Distance(currentTarget, currentPosition) <= travelDistance + 1)
                {
                    currentPosition = currentTarget;
                    newControlPoints.AddLast(currentTarget);
                    foundSolution = true;
                    break;
                }
            }

            // If no solution was found, return at least an partial solution
            if (!foundSolution)
            {
                _controlPointsLinkedList = newControlPoints;
                CopyFromLinkedListToList();
                Debug.LogError("Couldn't a good river with current configuration!");
                return;
            }
        }

        _controlPointsLinkedList = newControlPoints;
        CopyFromLinkedListToList();
    }

    // Tries to find a best new point to go based on set rules and parameters 
    private Vector3 FindNextStep(Vector3 currentTarget, Vector3 currentPosition, Vector3 lastDirection)
    {
        // 180 degree rotation starting from -90 degrees
        int rotationAngle = 3;
        int tries = 180/rotationAngle + 1;
        
        Vector3 bestPosition = Vector3.zero;
        float bestSteepnessCoef = 0;

        Vector3 candidatePosition = currentPosition;
        Vector3 candidateDirection = Quaternion.AngleAxis(-90 - rotationAngle, Vector3.up) * (currentTarget - currentPosition).normalized;
        candidateDirection.y = 0;
        
        // For each direction
        float candidateSteepnessCoef;
        while (tries >= 0)
        {
            // Try different direction
            candidateDirection = (Quaternion.AngleAxis(rotationAngle, Vector3.up) * candidateDirection).normalized;
            candidateDirection.y = 0;
            tries--;
            
            // Raycast to get candidate real position
            RaycastHit hitInfoCandidate;
            candidatePosition = currentPosition + candidateDirection * travelDistance + Vector3.up * 500;
            if (!Physics.Raycast(candidatePosition, Vector3.down, out hitInfoCandidate))
            {
                continue;
            }
            candidatePosition = hitInfoCandidate.point;
            
            // Raycast for current normal
            RaycastHit hitInfoCurrent;
            if (!Physics.Raycast(currentPosition + Vector3.up * 500, Vector3.down, out hitInfoCurrent))
            {
                continue;
            }
            
            // Angle calculation
            Vector3 rotationAxis = Vector3.Cross(hitInfoCurrent.normal, Vector3.up).normalized;
            Vector3 gradient = (Quaternion.AngleAxis(-90, rotationAxis) * hitInfoCurrent.normal).normalized;
            
            // Abs to successfully test steepness 
            Vector3 temp = candidatePosition - currentPosition;
            temp.y = Mathf.Abs(temp.y);
            
            float heightDifference = currentPosition.y - candidatePosition.y;
            float distanceFromTarget = Vector3.Distance(candidatePosition, currentTarget);
            
            // Reward function
            candidateSteepnessCoef = (180 - Mathf.Abs(Vector3.Angle(candidateDirection,
                                         currentTarget - currentPosition)) * 7 / distanceFromTarget) * targetWeight
                                     + heightDifference * 90 * heightDifferenceWeight
                                     + 180 - Mathf.Abs(Vector3.Angle(candidateDirection, gradient)) * gradientWeight
                                     + 180 - Mathf.Abs(Vector3.Angle(candidateDirection, lastDirection)) * sameDirectionWeight;
            
            // If new best solution was found save it
            if (candidateSteepnessCoef >= bestSteepnessCoef)
            {
                bestPosition = candidatePosition;
                bestSteepnessCoef = candidateSteepnessCoef;
            }
        }

        return bestPosition;
    }
     
    // -------------------------Texture-Generation----------------------------------------------------------------------
    // Generates the water texture for set chunks
    public void GenerateRiverButton(bool drawOnWaterChunks, bool resetting)
    {        
        if (controlPointsList.Count < 2) return;

        if (!DisableAllWaterChunks())
        {
            return;
        }

        float chunkSize = _chunkManager.GetChunkSize();
        float pixelInWorld = _chunkManager.GetChunkSize() / WATER_RESOLUTION;

        // Store all chunks with their texture
        Dictionary<Chunk, Texture2D> chunkTextures = new Dictionary<Chunk, Texture2D>();
        ICollection allChunks = _chunkManager.GetAllChunksTotal();
        foreach (Chunk chunk in allChunks)
        {
            Texture2D texture2D = chunk.GetWaterTexture();
            if (texture2D.width != WATER_RESOLUTION)
            {
                texture2D = CreateNewWaterTexture(WATER_RESOLUTION, chunk.name);
            }
            chunkTextures.Add(chunk, texture2D);
        }

        // Reset terrain for a while to get correctly connect water
        foreach (var chunkTexturePair in chunkTextures)                                      
        {                                                                                    
            chunkTexturePair.Key.NotifyChunkToUpdateOnce();
            chunkTexturePair.Key.SetWaterParameters(0);
        }
        
        // River carve depth
        float lastTargetHeight = float.PositiveInfinity;
        
        // Cross drawing utils
        Chunk lastChunk = null;
        Texture2D[] neighborTextures = new Texture2D[9];
        Texture2D[] texturesToDrawIn = new Texture2D[3];

        // Distance utils
        float traveledDistance = 0;
        float totalDistance = 0;
        
        // Go through tuples of points and calculate distance
        for (int i = 0; i < controlPointsList.Count - 1; i++)
        {
            totalDistance += Vector3.Distance(controlPointsList[i], controlPointsList[i + 1]);
        }

        // Go through tuples of points
        for (int i = 0; i < controlPointsList.Count - 1; i++)
        {
            Vector3 currPoint = controlPointsList[i];
            Vector3 nextPoint = controlPointsList[i + 1];

            float currToNextDistance = Vector3.Distance(currPoint, nextPoint);

            // Draw on texture moving by one pixel at a time 
            while (currToNextDistance >= pixelInWorld)
            {
                bool hit = Physics.Raycast(currPoint + Vector3.up * 300f, Vector3.down, out RaycastHit hitInfoCenter);

                if (!hit)
                {
                    EnableAllWaterChunks();
                    Debug.Log("Current river couldn't be created! Please create a path that fits on the terrain.");
                    return;
                }

                float targetHeight;
                float targetWaterLevel = 0;
                
                float lerpTotal = traveledDistance / totalDistance;
                float currentRiverWidth = Mathf.Lerp(waterStartWidth, waterEndWidth, lerpTotal);

                
                // Carve terrain and raise water level
                if (drawOnWaterChunks)
                {
                    // Choose the lowest height from (lerp of start to end height, current point center, lastTargetHeight) to prevent upwards flow
                    targetHeight = Mathf.Min(Mathf.Min(currPoint.y, hitInfoCenter.point.y), lastTargetHeight - 0.01f);

                    lastTargetHeight = targetHeight;
                    
                    // Add carving
                    float currentRiverCarve = Mathf.SmoothStep(riverStartDepth, riverEndDepth, lerpTotal);
                    targetHeight += currentRiverCarve;
                    
                    // Water level
                    targetWaterLevel = Mathf.SmoothStep(waterLevelStart, waterLevelEnd, lerpTotal);
                }
                // Only carve terrain
                else
                {
                    targetHeight = waterLevel;
                }

                // Width of path in tex space
                int widthOfSmooth = (int)((1.0f / _chunkManager.GetChunkSize()) * currentRiverWidth * WATER_RESOLUTION);
                int widthOfSmoothTexSpaceDiv2 = widthOfSmooth / 2;
        
                Chunk chunk = hitInfoCenter.collider.GetComponent<Chunk>();

                // Store neighbor textures
                if (lastChunk != chunk)
                {
                    BuildNeighborTextures(ref neighborTextures, ref chunkTextures, chunk);
                    lastChunk = chunk;
                }

                // Get world coords to texture coords
                Vector2 texPosFloat = hitInfoCenter.textureCoord * WATER_RESOLUTION;
                Vector2Int texPosInt = new Vector2Int(Mathf.RoundToInt(texPosFloat.x), Mathf.RoundToInt(texPosFloat.y));

                // Draw
                int startX = texPosInt.x - widthOfSmoothTexSpaceDiv2;
                int endX = texPosInt.x + widthOfSmoothTexSpaceDiv2;
                int startY = texPosInt.y - widthOfSmoothTexSpaceDiv2;
                int endY = texPosInt.y + widthOfSmoothTexSpaceDiv2;
                for (int x = startX; x <= endX; x++)
                {
                    for (int y = startY; y <= endY; y++)
                    {
                        // Edge case where we need to duplicate the pixels on seams
                        int distX = x;
                        int distY = y;
                        if (distX <= -1) distX++;
                        if (distX >= WATER_RESOLUTION) distX--;
                        if (distY <= -1) distY++;
                        if (distY >= WATER_RESOLUTION) distY--;
                        float distanceFromPath = Vector2.Distance(texPosInt, new Vector2(distX, distY));
                        
                        // Get current textures and change coords
                        int tempX = x;
                        int tempY = y;

                        texturesToDrawIn[0] = null;
                        texturesToDrawIn[1] = null;
                        texturesToDrawIn[2] = null;
                        Vector2Int xy1 = new Vector2Int(x, y);
                        Vector2Int xy2 = new Vector2Int(x, y);
                        GetRightTextureForCoords(ref tempX, ref tempY, ref neighborTextures, ref texturesToDrawIn, ref xy1, ref xy2);
                        if (texturesToDrawIn[0] == null) continue;
                        
                        // Raycast on current pixel to get height
                        Vector3 pixelWorldPosition = chunk.transform.position 
                                                + Vector3.forward * (((y - WATER_RESOLUTION/2) / (float)WATER_RESOLUTION) * chunkSize)
                                                + Vector3.right * (((x - WATER_RESOLUTION/2) / (float)WATER_RESOLUTION) * chunkSize)
                                                + Vector3.up * 300f;  // Raycast a bit above the terrain
                        bool hitPixel = Physics.Raycast(pixelWorldPosition , Vector3.down, out RaycastHit hitInfoPixel);

                        // Is 0 when close to path 1 when on edge of path
                        float t = distanceFromPath / widthOfSmoothTexSpaceDiv2;
                        t = Mathf.Clamp(t, 0, 1);


                        // Intensity of set pixel not to override higher values
                        Color previousColor = texturesToDrawIn[0].GetPixel(tempX, tempY);
                        
                        Color newColor = Color.red/2;
                        newColor.a = 0;
                        if (!resetting)
                        {
                            // Red channel is for height changing of terrain chunks 
                            if (drawOnWaterChunks)
                            {
                                // Only carve the terrain if not under water level in case of river generation
                                float tempTargetHeight = targetHeight;
                                if (tempTargetHeight < waterLevel) tempTargetHeight = waterLevel + underWaterLevelDepth;
                                
                                newColor.r = 0.5f + ((tempTargetHeight - hitInfoPixel.point.y) / waterMaxYOffset);
                            }
                            else newColor.r = 0.5f + ((targetHeight - hitInfoPixel.point.y + carveDepth) / waterMaxYOffset);
                            
                            // Green channel is for height changing of water chunks
                            if (drawOnWaterChunks && hitPixel)
                            {                           
                                newColor.g = (targetHeight - waterLevel + targetWaterLevel)/waterMaxYOffset;
                            }

                            // Alpha is for path edge blending
                            newColor.a = 1 - t;

                            // Draw based on alpha intensity
                            if (previousColor.a > newColor.a)
                            {
                                newColor.g = previousColor.g;
                                newColor.r = previousColor.r;
                            }

                            // Set alpha
                            newColor.a = Mathf.Max(newColor.a, previousColor.a);
                        }

                        texturesToDrawIn[0].SetPixel(tempX, tempY, newColor);
                        if (texturesToDrawIn[1] != null) texturesToDrawIn[1].SetPixel(xy1.x, xy1.y, newColor);
                        if (texturesToDrawIn[2] != null) texturesToDrawIn[2].SetPixel(xy2.x, xy2.y, newColor);
                    }
                }

                // Move towards end point
                currPoint = Vector3.Slerp(currPoint, nextPoint, pixelInWorld/currToNextDistance);
                currToNextDistance = Vector3.Distance(currPoint, nextPoint);
                traveledDistance += pixelInWorld;
            }
        }
        
        EnableAllWaterChunks();
        
        // STORE ALL CHANGED TEXTURES, SMOOTH OUT THEN APPLY 
        foreach (var chunkTexturePair in chunkTextures)                                      
        {                                                                                    
            Texture2D tex = chunkTexturePair.Value;                                          
            tex.Apply();
            
            // Chunk update
            Chunk chunk = chunkTexturePair.Key;
            chunk.SetWaterParameters(waterMaxYOffset);
            chunk.SetWaterTexture(tex, true); 

            // Water chunk update
            WaterChunk waterChunk = (WaterChunk)_waterChunksTable[chunk.name];
            if (waterChunk == null) continue;
            waterChunk.SetWaterParameters(waterLevel, waterMaxYOffset);
            waterChunk.SetWaterTexture(tex);
        }
    }

    // Enables all water chunks, used so the water chunks do not interfere with other texture generation
    public void EnableAllWaterChunks()
    {
        if (_waterChunksTable.Count == 0)
        {
            ReAddWaterChunks();
            if (_waterChunksTable.Count == 0)
            {
                return;
            }
        }
        foreach (WaterChunk waterChunk in _waterChunksTable.Values)
        {
            waterChunk.GetComponent<MeshCollider>().enabled = true;
        }
    }
    
    // Disables all water chunks, used so the water chunks do not interfere with other texture generation
    public bool DisableAllWaterChunks()
    {
        if (_waterChunksTable.Count == 0)
        {
            ReAddWaterChunks();
            if (_waterChunksTable.Count == 0)
            {
                return false;
            }
        }
        foreach (WaterChunk waterChunk in _waterChunksTable.Values)
        {
            waterChunk.GetComponent<MeshCollider>().enabled = false;
        }

        return true;
    }

    // Builds the neighborTextures array for the given chunk
    private void BuildNeighborTextures(ref Texture2D[] neighborTextures, ref Dictionary<Chunk, Texture2D> chunkTextures, Chunk chunk)
    {
        Vector2Int indices = chunk.GetIndices();
        Chunk tempChunk = null;
        
        // Top left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y + 1);
        if (tempChunk) neighborTextures[0] = chunkTextures[tempChunk];
        else neighborTextures[0] = null;

        // Top middle
        tempChunk = _chunkManager.GetChunk(indices.x, indices.y + 1);
        if (tempChunk) neighborTextures[1] = chunkTextures[tempChunk];
        else neighborTextures[1] = null;

        // Top right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y + 1);
        if (tempChunk) neighborTextures[2] = chunkTextures[tempChunk];
        else neighborTextures[2] = null;

        // Middle left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y);
        if (tempChunk) neighborTextures[3] = chunkTextures[tempChunk];
        else neighborTextures[3] = null;

        // Middle middle
        neighborTextures[4] = chunkTextures[chunk];

        // Middle right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y);
        if (tempChunk) neighborTextures[5] = chunkTextures[tempChunk];
        else neighborTextures[5] = null;

        // Bottom left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y - 1);
        if (tempChunk) neighborTextures[6] = chunkTextures[tempChunk];
        else neighborTextures[6] = null;

        // Bottom middle
        tempChunk = _chunkManager.GetChunk(indices.x, indices.y - 1);
        if (tempChunk) neighborTextures[7] = chunkTextures[tempChunk];
        else neighborTextures[7] = null;

        // Bottom right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y - 1);
        if (tempChunk) neighborTextures[8] = chunkTextures[tempChunk];
        else neighborTextures[8] = null;
    }

    // Returns the right textures with coordinates to draw in
    private void GetRightTextureForCoords(ref int x, ref int y, ref Texture2D[] neighborTextures, ref Texture2D[] texturesToDrawIn, ref Vector2Int xy1, ref Vector2Int xy2)
    {
        // Top left
        if (x < 0 && y >= WATER_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[0];
            x += WATER_RESOLUTION;
            y -= WATER_RESOLUTION;
        }
        // Top middle
        else if (x >= 0 && x < WATER_RESOLUTION && y >= WATER_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[1];
            y -= WATER_RESOLUTION;
        }
        // Top right
        else if (x >= WATER_RESOLUTION && y >= WATER_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[2];
            x -= WATER_RESOLUTION;
            y -= WATER_RESOLUTION;
        }
        // Middle left
        else if (x < 0 && y >= 0 && y < WATER_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[3];
            x += WATER_RESOLUTION;
        }
        // Middle middle
        else if (x >= 0 && x < WATER_RESOLUTION && y >= 0 && y < WATER_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[4];
            
            // At edges we need to cross draw
            // Left side
            if (x == 0)
            {
                texturesToDrawIn[1] = neighborTextures[3];
                xy1.x = WATER_RESOLUTION - 1;
                xy1.y = y;
                
                // Bottom left
                if (y == 0)
                {
                    texturesToDrawIn[2] = neighborTextures[6];
                    xy2.x = WATER_RESOLUTION - 1;
                    xy2.y = WATER_RESOLUTION - 1;
                }
                // Top left
                else if (y == WATER_RESOLUTION - 1)
                {
                    texturesToDrawIn[2] = neighborTextures[0];
                    xy2.x = WATER_RESOLUTION - 1;
                    xy2.y = 0;
                }
            }

            // Right side
            else if (x == WATER_RESOLUTION-1)
            {
                texturesToDrawIn[1] = neighborTextures[5];
                xy1.x = 0;
                xy1.y = y;
                
                // Bottom right
                if (y == 0)
                {
                    texturesToDrawIn[2] = neighborTextures[8];
                    xy2.x = 0;
                    xy2.y = WATER_RESOLUTION - 1;
                }
                // Top right
                else if (y == WATER_RESOLUTION - 1)
                {
                    texturesToDrawIn[2] = neighborTextures[2];
                    xy2.x = 0;
                    xy2.y = 0;
                }
            }
            
            // Top side
            else if (y == WATER_RESOLUTION - 1)
            {
                texturesToDrawIn[1] = neighborTextures[1];
                xy1.x = x;
                xy1.y = 0;
            }
            
            // Bottom side
            else if (y == 0)
            {
                texturesToDrawIn[1] = neighborTextures[7];
                xy1.x = x;
                xy1.y = WATER_RESOLUTION - 1;
            }
        }
        // Middle right
        else if (x >= WATER_RESOLUTION && y >= 0 && y < WATER_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[5];
            x -= WATER_RESOLUTION;
        }
        // Bottom left
        else if (x < 0 && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[6];
            x += WATER_RESOLUTION;
            y += WATER_RESOLUTION;
        }
        // Bottom middle
        else if (x >= 0 && x < WATER_RESOLUTION && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[7];
            y += WATER_RESOLUTION;
        }
        // Bottom right
        else if (x >= WATER_RESOLUTION && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[8];
            x -= WATER_RESOLUTION;
            y += WATER_RESOLUTION;
        }
    }

    // Creates a new texture to draw in
    private Texture2D CreateNewWaterTexture(int resolution, string chunkName)
    {
        Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBAFloat, true);
        int resPow2 = resolution * resolution;
        Color[] pixels = new Color[resPow2];
        
        for (int i = 0; i < resPow2; i++)
        {
            pixels[i] = Color.black;
            pixels[i].a = 0;
        }
        
        tex.SetPixels(pixels);

        tex.filterMode = FilterMode.Bilinear;
        tex.wrapMode = TextureWrapMode.Clamp;
        tex.name = chunkName + "WaterTex";
        
        tex.Apply();

        return tex;
    }
    
    // Resets all water textures for all chunks and water chunks
    public void ResetWaterData()
    {
        _chunkManager.ResetWaterData();

        if (_waterChunksTable.Count == 0) ReAddWaterChunks();
        
        foreach (WaterChunk waterChunk in _waterChunksTable.Values)
        {
            if (waterChunk == null) continue;
            waterChunk.SetWaterTexture(null);
        }
    }

    // Returns if water chunks have been generated
    public bool IsWaterGenerated()
    {
        return _isWaterGenerated;
    }

    // Gets the current wrapping of the water chunks
    public int GetWaterChunkWrapping()
    {
        return waterWrapping;
    }

    //----------------------------Utils---------------------------------------------------------------------------------
    // Draws the control points as blue spheres using Gizmos
    private void OnDrawGizmos()
    {
        if (!showControlPoints) return;
        
        Gizmos.color = Color.blue;

        Vector3 lastPoint = Vector3.zero;
        
        foreach (Vector3 point in _controlPointsLinkedList)
        {
            if (lastPoint != Vector3.zero)
            {
                Debug.DrawLine(lastPoint, point, Color.blue, 0f, false);
            }
            Gizmos.DrawSphere(point, 1f);
            lastPoint = point;
        }
    }

}
