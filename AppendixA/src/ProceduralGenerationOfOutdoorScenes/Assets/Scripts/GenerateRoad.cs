using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

[ExecuteInEditMode]
public class GenerateRoad : MonoBehaviour
{
    [SerializeField] private List<Vector3> controlPointsList = new List<Vector3>();
    [SerializeField] private bool showControlPoints = true;
    [SerializeField] private float roadWidth = 8f;
    [SerializeField] private float roadSmoothWidth = 8f;
    [SerializeField] private float roadMaxYOffset = 20f;
    [SerializeField] private bool generateRoadLines = true;

    // Contatnt
    private const int ROAD_RESOLUTION = 256;

    // The inner representation of the controlPointsList, used so that I can easily insert points between other points
    private readonly LinkedList<Vector3> _controlPointsLinkedList = new LinkedList<Vector3>();
    private ChunkManager _chunkManager;
    private bool _isNotFreshlyOpened;
    private bool _update;

    // ---------------------------------------------Mono-behaviour------------------------------------------------------
    private void Start()
    {
        // References of private variables are not kept on entering play mode or reloading the
        if (!_isNotFreshlyOpened)
        {
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
            _isNotFreshlyOpened = true;

            _update = true;
        }
    }

    private void Update()
    {
        // if an update was posted update the road parameters in chunks
        if (_update)
        {
            _update = false;
            _chunkManager.UpdateRoadParameters(roadMaxYOffset);
        }
    }

    // On inspector change
    private float _lastYOffset;
    private void OnValidate()
    {        
        // Propagate control point update
        _controlPointsLinkedList.Clear();
        foreach (Vector3 controlPoint in controlPointsList)
        {
            _controlPointsLinkedList.AddLast(controlPoint);
        }
        
        // Check references
        if (_chunkManager == null)
        {
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
        }
    }
    
    // -----------------------------Initialization----------------------------------------------------------------------
    public void Initialize(ChunkManager chunkManager)
    {
        _chunkManager = chunkManager;
    }

    // --------------------------------Point-utils----------------------------------------------------------------------
    // Adds a road point to the control points
    public void AddRoadPoint(Vector3 point)
    {
        _controlPointsLinkedList.AddLast(point);
        controlPointsList.Add(point);
    }

    // Resets all the control points
    public void ResetControlPointsButton()
    {
        _controlPointsLinkedList.Clear();
        controlPointsList.Clear();
    }
    
    // Resets all but the first and last control points
    public void ResetControlPointsWithoutFirstLastButton()
    {
        if (controlPointsList.Count <= 0) return;
        if (_controlPointsLinkedList.Count <= 0) return;
        
        Vector3 first = _controlPointsLinkedList.First.Value;
        Vector3 last = _controlPointsLinkedList.Last.Value;
        _controlPointsLinkedList.Clear();
        controlPointsList.Clear();

        AddRoadPoint(first);
        AddRoadPoint(last);
    }
    
    // -------------------------Texture-Generation----------------------------------------------------------------------
    // Generates the road texture based on the control points and parameters
    public void GenerateRoadButton()
    {
        if (controlPointsList.Count < 2) return;

        transform.parent.GetComponentInChildren<GenerateWater>()?.DisableAllWaterChunks();
        
        // Width of path in tex space
        int widthOfRoadTexSpace = (int)((1.0f / _chunkManager.GetChunkSize()) * roadWidth * ROAD_RESOLUTION);
        int widthOfRoadTexSpaceDiv2 = widthOfRoadTexSpace / 2;
        
        // Width of smoothing
        int widthOfSmooth = (int)((1.0f / _chunkManager.GetChunkSize()) * roadSmoothWidth * ROAD_RESOLUTION);
        int widthOfSmoothTexSpaceDiv2 = widthOfSmooth / 2;
        
        int totalWidthDiv2 = widthOfRoadTexSpaceDiv2 + widthOfSmoothTexSpaceDiv2;
        
        float chunkSize = _chunkManager.GetChunkSize();
        
        // Size of a pixel from the texture in world coordinates 
        float pixelInWorld = _chunkManager.GetChunkSize() / ROAD_RESOLUTION;
        
        // Store all chunks with their texture
        Dictionary<Chunk, Texture2D> chunkTextures = new Dictionary<Chunk, Texture2D>();
        ICollection allChunks = _chunkManager.GetAllChunksTotal();
        foreach (Chunk chunk in allChunks)
        {
            Texture2D texture2D = chunk.GetRoadTexture();
            if (texture2D.width != ROAD_RESOLUTION)
            {
                texture2D = CreateNewRoadTexture(ROAD_RESOLUTION, chunk.name);
            }
            chunkTextures.Add(chunk, texture2D);
        }
        
        // Reset terrain for a while to get correctly connect roads
        foreach (var chunkTexturePair in chunkTextures)                                      
        {                                                                                    
            chunkTexturePair.Key.NotifyChunkToUpdateOnce();
            chunkTexturePair.Key.SetRoadParameters(0);
            chunkTexturePair.Key.NotifyChunkToUpdateOnce();
            chunkTexturePair.Key.SetRoadParameters(0);
        }

        // Cross drawing utils
        Chunk lastChunk = null;
        
        Texture2D[] neighborTextures = new Texture2D[9]; 
        Texture2D[] texturesToDrawIn = new Texture2D[3];

        Vector3 firstPoint = controlPointsList[0];
        Vector3 lastPoint = controlPointsList[controlPointsList.Count - 1];
        
        // start with first two points
        for (int i = 0; i < controlPointsList.Count - 1; i++)
        {
            Vector3 startPoint = controlPointsList[i];
            Vector3 endPoint = controlPointsList[i + 1];

            Vector3 currPoint = startPoint;

            // Distance calculation
            float startToEndDistance = Vector3.Distance(startPoint, endPoint);
            float currToEndDistance = Vector3.Distance(currPoint, endPoint);

            Vector3 direction = (endPoint - currPoint).normalized;
            
            // Draw on texture moving by one pixel at a time 
            while (currToEndDistance >= pixelInWorld)
            {
                bool hit = Physics.Raycast(currPoint + Vector3.up * 300f, Vector3.down, out RaycastHit hitInfoCenter);

                if (!hit)
                {
                    transform.parent.GetComponentInChildren<GenerateWater>()?.EnableAllWaterChunks();
                    Debug.Log("Current road couldn't be created! Please create a path that fits on the terrain.");
                    return;
                }
                
                // Setup the target height based on middle of the path
                float heightLerpStartEnd = currToEndDistance / startToEndDistance;
                heightLerpStartEnd = Mathf.Clamp(heightLerpStartEnd, 0, 1);
                float targetHeight = Mathf.Lerp(endPoint.y, startPoint.y, Mathf.Lerp(heightLerpStartEnd, Easing.InOutQuad(heightLerpStartEnd), 0.5f));

                Chunk chunk = hitInfoCenter.collider.GetComponent<Chunk>();
                
                // Store neighbor textures
                if (lastChunk != chunk)
                {
                    BuildNeighborTextures(ref neighborTextures, ref chunkTextures, chunk);
                    lastChunk = chunk;
                }

                // Get world coords to texture coords
                Vector2 texPosFloat = hitInfoCenter.textureCoord * ROAD_RESOLUTION;
                Vector2Int texPosInt = new Vector2Int(Mathf.RoundToInt(texPosFloat.x), Mathf.RoundToInt(texPosFloat.y));

                // Draw
                int startX = texPosInt.x - totalWidthDiv2;
                int endX = texPosInt.x + totalWidthDiv2;
                int startY = texPosInt.y - totalWidthDiv2;
                int endY = texPosInt.y + totalWidthDiv2;
                for (int x = startX; x <= endX; x++)
                {
                    for (int y = startY; y <= endY; y++)
                    {
                        // optimization, skips some pixels that are not needed to overdraw
                        float angle = Vector2.SignedAngle((new Vector2(x, y) - texPosFloat), new Vector2(direction.x, direction.z));
                        bool isOnPath =  (angle < 95 && angle > 85) || (angle < -85 && angle > -95);

                        // Edge case where we need to duplicate the pixels on seams
                        int distX = x;
                        int distY = y;
                        if (distX <= -1) distX++;
                        if (distX >= ROAD_RESOLUTION) distX--;
                        if (distY <= -1) distY++;
                        if (distY >= ROAD_RESOLUTION) distY--;
                        float distanceFromPath = Vector2.Distance(texPosInt, new Vector2(distX, distY));
                        
                        // Skip irrelevant pixels
                        if ((currPoint != firstPoint && Vector3.Distance(currPoint, lastPoint) < pixelInWorld * 2 && 1 - (distanceFromPath/ widthOfRoadTexSpaceDiv2) < 0.5f) && !isOnPath) continue;
                        
                        // Get current textures and change coords
                        int tempX = x;
                        int tempY = y;

                        texturesToDrawIn[0] = null;
                        texturesToDrawIn[1] = null;
                        texturesToDrawIn[2] = null;
                        Vector2Int xy1 = new Vector2Int(x, y);
                        Vector2Int xy2 = new Vector2Int(x, y);
                        GetRightTextureForCoords(ref tempX, ref tempY, ref neighborTextures, ref texturesToDrawIn, ref xy1, ref xy2);
                        if (texturesToDrawIn[0] == null) continue;
                        
                        // Raycast on current pixel to get height
                        Vector3 pixelWorldPosition = chunk.transform.position 
                                                + Vector3.forward * (((y - ROAD_RESOLUTION/2) / (float)ROAD_RESOLUTION) * chunkSize)
                                                + Vector3.right * (((x - ROAD_RESOLUTION/2) / (float)ROAD_RESOLUTION) * chunkSize)
                                                + Vector3.up * 300f;  // Raycast a bit above the terrain
                        bool hitPixel = Physics.Raycast(pixelWorldPosition , Vector3.down, out RaycastHit hitInfoPixel);

                        // Is 0 when close to path 1 when on edge of path
                        float t = (distanceFromPath - widthOfRoadTexSpaceDiv2) / widthOfSmoothTexSpaceDiv2;
                        t = Mathf.Clamp(t, 0, 1);


                        // Intensity of set pixel not to override higher values
                        Color previousColor = texturesToDrawIn[0].GetPixel(tempX, tempY);
                        
                        Color newColor = Color.green/2;
                        
                        // Green channel is for height changing
                        if (hitPixel)
                        {
                            newColor.g = 0.5f + ((targetHeight - hitInfoPixel.point.y)/roadMaxYOffset);
                        }

                        // Blue channel is for distance from center of path
                        newColor.b = 1 - (distanceFromPath/ widthOfRoadTexSpaceDiv2);
                        newColor.b = Mathf.Clamp(newColor.b, 0, 1);
                        
                        // Red channel is for Road lines
                        if (generateRoadLines)
                        {
                            // Interpolate in and out of the line
                            float lineLerpCoef = distanceFromPath / widthOfRoadTexSpaceDiv2;
                            if (lineLerpCoef >= 0.85f && lineLerpCoef <= 0.925f)
                            {
                                newColor.r = Mathf.SmoothStep(0f, 1f, (lineLerpCoef - 0.85f)/0.075f) * 1f;
                            }
                            else if (lineLerpCoef >= 0.925f && lineLerpCoef < 1f)
                            {
                                newColor.r = Mathf.SmoothStep(1f, 0f, (lineLerpCoef - 0.925f)/0.075f) * 1f;
                            }
                        }

                        // Alpha is for path edge blending
                        newColor.a = 1 - t;

                        if (previousColor.b > newColor.b)
                        {
                            newColor.g = previousColor.g;
                        }
                        
                        if (previousColor.a > newColor.a)
                        {
                            newColor.g = previousColor.g;
                        }
                        
                        // blue and red overdraw
                        if (previousColor.b > newColor.b)
                        {
                            newColor.r = previousColor.r;
                            newColor.b = previousColor.b;
                        }

                        // Set alpha
                        if (previousColor.a > newColor.a)
                        {
                            newColor.a = previousColor.a;
                        }

                        texturesToDrawIn[0].SetPixel(tempX, tempY, newColor);
                        
                        // Edge case for chunk edge and corner
                        if (texturesToDrawIn[1] != null) texturesToDrawIn[1].SetPixel(xy1.x, xy1.y, newColor);
                        if (texturesToDrawIn[2] != null) texturesToDrawIn[2].SetPixel(xy2.x, xy2.y, newColor);
                    }
                }

                // Move towards end point
                currPoint = Vector3.Lerp(endPoint, startPoint, (currToEndDistance-pixelInWorld*5)/startToEndDistance); //headroom
                currToEndDistance = Vector3.Distance(currPoint, endPoint);
            }
        }
        
        transform.parent.GetComponentInChildren<GenerateWater>()?.EnableAllWaterChunks();

        // STORE ALL CHANGED TEXTURES, SMOOTH OUT THEN APPLY 
        foreach (var chunkTexturePair in chunkTextures)                                      
        {                                                                                    
            Texture2D tex = chunkTexturePair.Value;                                          
            tex.Apply();        
            chunkTexturePair.Key.NotifyChunkToUpdateOnce();
            chunkTexturePair.Key.SetRoadTexture(tex, true); 
            chunkTexturePair.Key.NotifyChunkToUpdateOnce();
            chunkTexturePair.Key.SetRoadParameters(roadMaxYOffset);
        }
    }
    
    // Creates the textures of neighbouring chunks for a given chunk, saves it in neighborTextures    
    private void BuildNeighborTextures(ref Texture2D[] neighborTextures, ref Dictionary<Chunk, Texture2D> chunkTextures, Chunk chunk)
    {
        Vector2Int indices = chunk.GetIndices();
        Chunk tempChunk = null;
        
        // Top left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y + 1);
        if (tempChunk) neighborTextures[0] = chunkTextures[tempChunk];
        else neighborTextures[0] = null;

        // Top middle
        tempChunk = _chunkManager.GetChunk(indices.x, indices.y + 1);
        if (tempChunk) neighborTextures[1] = chunkTextures[tempChunk];
        else neighborTextures[1] = null;

        // Top right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y + 1);
        if (tempChunk) neighborTextures[2] = chunkTextures[tempChunk];
        else neighborTextures[2] = null;

        // Middle left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y);
        if (tempChunk) neighborTextures[3] = chunkTextures[tempChunk];
        else neighborTextures[3] = null;

        // Middle middle
        neighborTextures[4] = chunkTextures[chunk];

        // Middle right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y);
        if (tempChunk) neighborTextures[5] = chunkTextures[tempChunk];
        else neighborTextures[5] = null;

        // Bottom left
        tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y - 1);
        if (tempChunk) neighborTextures[6] = chunkTextures[tempChunk];
        else neighborTextures[6] = null;

        // Bottom middle
        tempChunk = _chunkManager.GetChunk(indices.x, indices.y - 1);
        if (tempChunk) neighborTextures[7] = chunkTextures[tempChunk];
        else neighborTextures[7] = null;

        // Bottom right
        tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y - 1);
        if (tempChunk) neighborTextures[8] = chunkTextures[tempChunk];
        else neighborTextures[8] = null;
    }

    // Gets the right textures and coordinates for given x and y
    private void GetRightTextureForCoords(ref int x, ref int y, ref Texture2D[] neighborTextures, ref Texture2D[] texturesToDrawIn, ref Vector2Int xy1, ref Vector2Int xy2)
    {
        // Top left
        if (x < 0 && y >= ROAD_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[0];
            x += ROAD_RESOLUTION;
            y -= ROAD_RESOLUTION;
        }
        // Top middle
        else if (x >= 0 && x < ROAD_RESOLUTION && y >= ROAD_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[1];
            y -= ROAD_RESOLUTION;
        }
        // Top right
        else if (x >= ROAD_RESOLUTION && y >= ROAD_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[2];
            x -= ROAD_RESOLUTION;
            y -= ROAD_RESOLUTION;
        }
        // Middle left
        else if (x < 0 && y >= 0 && y < ROAD_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[3];
            x += ROAD_RESOLUTION;
        }
        // Middle middle
        else if (x >= 0 && x < ROAD_RESOLUTION && y >= 0 && y < ROAD_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[4];
            
            // At edges we need to cross draw
            // Left side
            if (x == 0)
            {
                texturesToDrawIn[1] = neighborTextures[3];
                xy1.x = ROAD_RESOLUTION - 1;
                xy1.y = y;
                
                // Bottom left
                if (y == 0)
                {
                    texturesToDrawIn[2] = neighborTextures[6];
                    xy2.x = ROAD_RESOLUTION - 1;
                    xy2.y = ROAD_RESOLUTION - 1;
                }
                // Top left
                else if (y == ROAD_RESOLUTION - 1)
                {
                    texturesToDrawIn[2] = neighborTextures[0];
                    xy2.x = ROAD_RESOLUTION - 1;
                    xy2.y = 0;
                }
            }

            // Right side
            else if (x == ROAD_RESOLUTION-1)
            {
                texturesToDrawIn[1] = neighborTextures[5];
                xy1.x = 0;
                xy1.y = y;
                
                // Bottom right
                if (y == 0)
                {
                    texturesToDrawIn[2] = neighborTextures[8];
                    xy2.x = 0;
                    xy2.y = ROAD_RESOLUTION - 1;
                }
                // Top right
                else if (y == ROAD_RESOLUTION - 1)
                {
                    texturesToDrawIn[2] = neighborTextures[2];
                    xy2.x = 0;
                    xy2.y = 0;
                }
            }
            
            // Top side
            else if (y == ROAD_RESOLUTION - 1)
            {
                texturesToDrawIn[1] = neighborTextures[1];
                xy1.x = x;
                xy1.y = 0;
            }
            
            // Bottom side
            else if (y == 0)
            {
                texturesToDrawIn[1] = neighborTextures[7];
                xy1.x = x;
                xy1.y = ROAD_RESOLUTION - 1;
            }
        }
        // Middle right
        else if (x >= ROAD_RESOLUTION && y >= 0 && y < ROAD_RESOLUTION)
        {
            texturesToDrawIn[0] = neighborTextures[5];
            x -= ROAD_RESOLUTION;
        }
        // Bottom left
        else if (x < 0 && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[6];
            x += ROAD_RESOLUTION;
            y += ROAD_RESOLUTION;
        }
        // Bottom middle
        else if (x >= 0 && x < ROAD_RESOLUTION && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[7];
            y += ROAD_RESOLUTION;
        }
        // Bottom right
        else if (x >= ROAD_RESOLUTION && y < 0)
        {
            texturesToDrawIn[0] = neighborTextures[8];
            x -= ROAD_RESOLUTION;
            y += ROAD_RESOLUTION;
        }
    }

    // Creates a new road texture to draw in
    private Texture2D CreateNewRoadTexture(int resolution, string chunkName)
    {
        Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBAFloat, true);
        int resPow2 = resolution * resolution;
        Color[] pixels = new Color[resPow2];
        
        for (int i = 0; i < resPow2; i++)
        {
            pixels[i] = new Color(0,0.5f,0,1);
            pixels[i].a = 0;
        }
        
        tex.SetPixels(pixels);

        tex.filterMode = FilterMode.Bilinear;
        tex.wrapMode = TextureWrapMode.Clamp;
        tex.name = chunkName + "RoadTex";
        
        tex.Apply();

        return tex;
    }
    
    // Resets all the road textures for all chunks
    public void ResetRoadData()
    {
        _chunkManager.ResetRoadData();
    }

    // -----------------------------Utils-------------------------------------------------------------------------------
    // Draws the control points as gray spheres as gizmos
    private void OnDrawGizmos()
    {
        if (!showControlPoints) return;
        
        Gizmos.color = Color.gray;

        Vector3 lastPoint = Vector3.zero;
        
        foreach (Vector3 point in _controlPointsLinkedList)
        {
            if (lastPoint != Vector3.zero)
            {
                Debug.DrawLine(lastPoint, point, Color.gray, 0f, false);
            }
            Gizmos.DrawSphere(point, 1f);
            lastPoint = point;
        }
    }
}
