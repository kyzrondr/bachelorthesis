using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShot : MonoBehaviour
{
    // taken from https://stackoverflow.com/questions/71082188/how-to-take-a-screenshot-of-the-game-view-in-unity
    // A simple method that captures the current game view at an upscaled resolution upon left click
    private void Update(){
        if(Input.GetMouseButtonDown(0)){ // capture screen shot on left mouse button down

            // Screenshot folder
            string folderPath = "Assets/Screenshots/";

            // Creates the folder path if it doesn't exist
            if (!System.IO.Directory.Exists(folderPath))
                System.IO.Directory.CreateDirectory(folderPath);
            
            // Generates screenshot name based on current tie
            var screenshotName =
                "Screenshot_" +
                System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") +
                ".png";
            
            // Takes the screenshot, the "2" is for the scaled resolution
            ScreenCapture.CaptureScreenshot(System.IO.Path.Combine(folderPath, screenshotName),2); 
            Debug.Log(folderPath + screenshotName);
        }
    }
}
