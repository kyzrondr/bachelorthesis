This is the electronic appendix of Ondřej Kyzr's bachelor thesis.

src/ - The unity project in which the implentation part took in.

latex/ - The latex project in which the thesis was written in. The "latex/Images" is empty due to size constrains and should be populated with files from the "images/" folder.

images/ - All the images used in the thesis.

GitLab link
https://gitlab.fel.cvut.cz/kyzrondr/bachelorthesis