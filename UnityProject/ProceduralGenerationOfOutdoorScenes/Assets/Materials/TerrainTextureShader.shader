Shader "Custom/TerrainTextureShader"
{
    Properties
    {
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        [Space]
        [Header(Steepness Based Texturing)]
        [Space]
        _GrassTexture ("Grass Texture", 2D) = "white"{}
        _GrassScale ("Grass Texture Scale", float) = 1
        _ThresholdGD ("ThresholdGD", Range(0,1)) = 0.5
        _InterpolateGD ("InterpolateGD", Range(0.0,1.0)) = 0
        _DirtTexture ("Dirt Texture", 2D) = "white"{}
        _DirtScale ("Dirt Texture Scale", float) = 1
        _ThresholdDS ("ThresholdDS", Range(0,1)) = 0.5
        _InterpolateDS ("InterpolateDS", Range(0.0,1.0)) = 0
        _StoneTexture ("Stone Texture", 2D) = "white"{}
        _StoneScale ("Stone Texture Scale", float) = 1
        
        [Space]
        [Header(Mountain Top Texturing)]
        [Space]
        _AltitudeHeight ("Mountain Top Height", float) = 100
        _AltitudeTexture ("Mountain Top Texture", 2D) = "white"{}
        _AltitudeScale ("Mountain Top Texture Scale", float) = 1
        _AltitudeInterpolate ("Mountain Top Interpolate", Range(0, 30)) = 5
        
	    [Space]
        [Header(UnderWater)]
        [Space]
        _UnderWaterTextureUsed ("UnderWater Texture", 2D) = "white"{}
        _UnderWaterScale ("UnderWater Texture Scale", float) = 1
        _WaterLevel ("Water Level", float) = 1
        _WaterLevelInterpolate ("Water Level Interpolate", Range(0, 30)) = 5

        [HideInInspector] _UnderWaterTexture ("Water Texture DO NOT SET", 2D) = "white" {}
    	
        [Space]
        [Header(Path)]
        [Space]
        _PathTextureUsed ("Path Texture", 2D) = "white"{}
        _PathScale ("Path Texture Scale", float) = 1

        [HideInInspector] _PathTexture ("Path Texture DO NOT SET", 2D) = "white" {}

    	[Space]
        [Header(Road)]
        [Space]
        _RoadTextureUsed ("Road Texture", 2D) = "white"{}
        _RoadScale ("Road Texture Scale", float) = 1
	    _RoadLineColor ("Road Line Color", Color) = (0.8, 0.8, 0.8, 1)

        [HideInInspector] _RoadTexture ("Road Texture DO NOT SET", 2D) = "white" {}
    	
        [Space]
    	[Header(Grass)]
    	[Space]
    	[MaterialToggle] _UseGrass ("Use Grass", float) = 1
    	_TessellationUniform ("Grass Density", Range(0, 64)) = 10
	    _MinTessellationUniform ("Minimal Grass Density", Range(0, 64)) = 3
        [MaterialToggle] _CullAtMaxDistance ("Cull At Maximum Distance", float) = 1
    	_DistanceCulling ("High Detail Distance", float) = 10
    	_DistanceCullingLerp ("Detail Interpolate Dist.", float) = 100
        [Space]
        [Space]

        _BladeBottomColor ("Blade Bottom Color", color) = (0.1, 0.2, 0, 1)
	    _BladeTopColor ("Blade Top Color", color) = (0.6, 0.9, 0.3, 1)
        _BladeWidth("Blade Width", float) = 0.05
    	_BladeWidthOffsetRand("Random Offset of Width", float) = 0.02
    	_BladeHeight("Blade Height", float) = 0.5
    	_BladeHeightOffsetRand("Random Offset of Height", float) = 0.4
        _BladeBendAmount("Blade Bend Amount", float) = 0.15
    	[Space]
        [Space]
        _WindMap("Wind Map (RG)", 2D) = "white" {}
		_WindScaleX("Wind Scale X-axis", float) = 0.05
		_WindScaleZ("Wind Scale Z-axis", float) = 0.05
	    _WindPower("Wind Power", float) = 0.05
    }
    SubShader
    {
    	Tags { "RenderType"="Opaque" }
		LOD 200
    	
        // Texture pass
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows
        
        // Use shader model 4.6 target, to get nicer looking lighting
        #pragma target 4.6

        // Surface shader
        struct Input
        {
            float2 uv_PathTexture;
            float3 worldPos;
            float3 worldNormal;
        };

		// All the variables from the shader properties
        // Steepness properties
        half _Glossiness;
        half _Metallic;
        sampler2D _GrassTexture;
        half _GrassScale;
        half _ThresholdGD;
        half _InterpolateGD;
        sampler2D _DirtTexture;
        half _DirtScale;
        half _ThresholdDS;
        half _InterpolateDS;
        sampler2D _StoneTexture;
        half _StoneScale;

        // Path properties
        uniform sampler2D _PathTexture;
        sampler2D _PathTextureUsed;
        half _PathScale;

        // Road properties
        uniform sampler2D _RoadTexture;
        sampler2D _RoadTextureUsed;
        half _RoadScale;
        fixed4 _RoadLineColor;

        // Altitude low properties
		uniform sampler2D _UnderWaterTexture;
        sampler2D _UnderWaterTextureUsed;
        half _UnderWaterScale;
		half _WaterLevel;
        half _WaterLevelInterpolate;

        // Altitude high properties
        half _AltitudeHeight;
        sampler2D _AltitudeTexture;
        half _AltitudeScale;
        half _AltitudeInterpolate;

        // Calculates the angle between two given vectors
        float getAngle(float3 a, float3 b)
        {
            return acos(dot(a, b)/(length(a) * length(b)));
        }

        // Gets a color by trilinear mapping of a pixel from a texture based on the blendAxis
        fixed3 getColorFromProjTex(float3 blendAxis, sampler2D tex, float scale,  float3 pos)
        {
	        float3 projX = tex2D(tex, pos.yz * scale) * blendAxis.x;
            float3 projY = tex2D(tex, pos.xz * scale) * blendAxis.y;
            float3 projZ = tex2D(tex, pos.xy * scale) * blendAxis.z;

            return projX + projY + projZ; 
        }

        // Surface shader
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float angle = 1 - getAngle(IN.worldNormal,float3(0.f,1.f,0.f));

        	// Setup steepness thresholds
            float grassThresholdLower = _ThresholdGD + _InterpolateGD;
            float dirtThresholdUpper = _ThresholdGD - _InterpolateGD;
            float dirtThresholdLower = _ThresholdDS + _InterpolateDS;
            float stoneThresholdUpper = _ThresholdDS - _InterpolateDS;

            // Prepare the blending axis for triplanar mapping
            float3 axisForBlend = abs(IN.worldNormal);
            axisForBlend /= axisForBlend.x + axisForBlend.y + axisForBlend.z;

        	// Get all the colors for current position
			fixed3 grassColor = getColorFromProjTex(axisForBlend, _GrassTexture, _GrassScale, IN.worldPos);
        	fixed3 dirtColor = getColorFromProjTex(axisForBlend, _DirtTexture, _DirtScale, IN.worldPos);
        	fixed3 stoneColor = getColorFromProjTex(axisForBlend, _StoneTexture, _StoneScale, IN.worldPos);
        	fixed3 snowColor = getColorFromProjTex(axisForBlend, _AltitudeTexture, _AltitudeScale, IN.worldPos);
        	fixed3 pathColor = getColorFromProjTex(axisForBlend, _PathTextureUsed, _PathScale, IN.worldPos);
        	fixed3 roadColor = getColorFromProjTex(axisForBlend, _RoadTextureUsed, _RoadScale, IN.worldPos);
        	fixed3 underWaterColor = getColorFromProjTex(axisForBlend, _UnderWaterTextureUsed, _UnderWaterScale, IN.worldPos);

            // Steepness based coloring
            if (angle > grassThresholdLower)
            {        
                o.Albedo = grassColor;        
            }
            else if (angle > dirtThresholdUpper)
            {
                // Interpolate
                float divisor = grassThresholdLower - dirtThresholdUpper;
                float offset = dirtThresholdUpper/divisor;
                angle = lerp(0.0f, 1.f, angle/divisor - offset);
                o.Albedo = lerp(dirtColor, grassColor, angle);              
            }
            else if (angle > dirtThresholdLower)
            {
                o.Albedo = dirtColor;   
            }
            else if (angle > stoneThresholdUpper)
            {
                // Interpolate
                float divisor = dirtThresholdLower - stoneThresholdUpper;
                float offset = stoneThresholdUpper/divisor;
                angle = lerp(0.0f, 1.f, angle/divisor - offset);
                o.Albedo = lerp(stoneColor, dirtColor, angle); 
            }
            else
            {
                o.Albedo = stoneColor;  
            }

            // Altitude high based coloring
            if (IN.worldPos.y > _AltitudeHeight - _AltitudeInterpolate)
            {
                float lowerThresholdAlt = _AltitudeHeight - _AltitudeInterpolate;
                float upperThresholdAlt = _AltitudeHeight + _AltitudeInterpolate;

                float interpolator;
                
                if (IN.worldPos.y > upperThresholdAlt) interpolator = 1;
                else if (IN.worldPos.y < lowerThresholdAlt) interpolator = 0;
                else
                {
                    interpolator = clamp((IN.worldPos.y - lowerThresholdAlt) / _AltitudeInterpolate, 0, 1);
                }
                
                o.Albedo = lerp(o.Albedo, snowColor, interpolator);
            }
            
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;

        	// Path coloring
            float pathAtThisPoint = tex2D(_PathTexture, IN.uv_PathTexture).a;
            o.Albedo = lerp(o.Albedo, pathColor, abs(pathAtThisPoint));

        	// River uses dirt texture underwater 
        	float4 waterAtThisPoint = tex2D(_UnderWaterTexture, IN.uv_PathTexture);
            o.Albedo = lerp(o.Albedo, dirtColor, abs(waterAtThisPoint.a));
        	
			// Altitude low and water level coloring 
        	float lowerThresholdWater = _WaterLevel - _WaterLevelInterpolate;
        	float waterLerp;
        	if (IN.worldPos.y > _WaterLevel) waterLerp = 0;
        	else if (IN.worldPos.y < lowerThresholdWater) waterLerp = 1;
        	else
        	{
        		waterLerp = lerp(1, 0, (IN.worldPos.y - lowerThresholdWater) / _WaterLevelInterpolate);
        	}
            o.Albedo = lerp(o.Albedo, underWaterColor, waterLerp);

        	// Road coloring
        	float4 roadAtThisPoint = tex2D(_RoadTexture, IN.uv_PathTexture);
            o.Albedo = lerp(o.Albedo, roadColor, abs(roadAtThisPoint.a));
            o.Albedo = lerp(o.Albedo, _RoadLineColor, abs(roadAtThisPoint.r));
        }
        ENDCG
        
    	// Grass Pass
        Pass
		{
			Cull Off
			CGPROGRAM
	
			#pragma vertex vert
			#pragma fragment frag
			#pragma require geometry
			#pragma geometry geo
			#pragma hull hull
			#pragma domain domain
			#pragma target 4.6
			#pragma multi_compile_fwdbase
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			
			#define UNITY_PI 3.14159265359f
			#define UNITY_TWO_PI 6.28318530718f

			// Toggle property to use the shader pass
			float _UseGrass;

			// Input struct for the vertex shader
			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 uv_PathTexture : TEXCOORD0;
			};

			// Output struct for the vertex shader
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 uv_PathTexture : TEXCOORD0;
			};

			// Output struct for the geometry shader
			struct geometryOutput
			{
				float4 pos : SV_POSITION;
				float4 color : COLOR;
			};

			// -------------------------------VERTEX SHADER----------------------------------------------
			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;
				o.pos = v.vertex;
				o.normal = v.normal;
				o.tangent = v.tangent;
				o.uv_PathTexture = v.uv_PathTexture;
				return o;
			}

			// -------------------------------HULL SHADER----------------------------------------------
			// Tessallation shader hull
			// Used from here https://catlikecoding.com/unity/tutorials/advanced-rendering/tessellation/
			[UNITY_domain("tri")]
			[UNITY_outputtopology("triangle_cw")]
			[UNITY_outputcontrolpoints(3)]
			[UNITY_partitioning("integer")]
			[UNITY_patchconstantfunc("patchConstantFunc")]
			vertexOutput hull(InputPatch<vertexOutput, 3> patch, uint id: SV_OutputControlPointID)
			{
				return patch[id];
			}

			// Tessellation factors to set how much the mesh tessalates
			struct TessellationFactors 
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			// Calculates the angle between two given vectors
			float getAngle(float3 a, float3 b)
			{
				return acos(dot(a, b)/(length(a) * length(b)));
			}

			// Properties from the shader
			half _ThresholdGD;
			half _InterpolateGD;
			half _AltitudeHeight;
			half _AltitudeInterpolate;
			half _WaterLevel;
			half _WaterLevelInterpolate;
			half _CullAtMaxDistance;
			half _DistanceCulling;
			half _DistanceCullingLerp;
			sampler2D _PathTexture;
			sampler2D _RoadTexture;
			sampler2D _UnderWaterTexture;
			float _TessellationUniform;
			float _MinTessellationUniform;			
			// -------------------------------TESSELLATION SHADER----------------------------------------------
			// Tesselation factors setup function
			TessellationFactors patchConstantFunc(InputPatch<vertexOutput, 3> patch)
			{
				TessellationFactors f;
				float tessUniform = 0;

				// Throw away the mesh if the grass is turned off
				if (_UseGrass == 1)
				{
					// Tessellate based on steepness of terrain
					float angle = 1 - getAngle(patch[0].normal, float3(0,1,0));
					float lowerGrassTH = _ThresholdGD + _InterpolateGD + 0.1;
					float upperDirtTH = _ThresholdGD - _InterpolateGD + 0.1;

					float angleTessellation = 0;
					if (angle > lowerGrassTH) angleTessellation = _TessellationUniform;
					else if (angle > upperDirtTH && angle < lowerGrassTH)
						angleTessellation = lerp(_MinTessellationUniform, _TessellationUniform, ((angle - upperDirtTH)/(lowerGrassTH-upperDirtTH)) );	// TODO

					// Tessellate based on path
					float pathValue = tex2Dlod(_PathTexture,float4(patch[0].uv_PathTexture, 0, 0)).a;
					float pathTessellation = _TessellationUniform;
					if (pathValue > 0.1f) pathTessellation = lerp(_TessellationUniform, _MinTessellationUniform, pathValue);

					// Tessellate based on road
					float roadValue = tex2Dlod(_RoadTexture,float4(patch[0].uv_PathTexture, 0, 0)).a;
					float roadTessellation;
					if (roadValue < 0.6f) roadTessellation = lerp(_TessellationUniform, _MinTessellationUniform, roadValue/0.6f);
					else roadTessellation = 0;
					
					// Tessellate based on rivers
					float riverValue = tex2Dlod(_UnderWaterTexture,float4(patch[0].uv_PathTexture, 0, 0)).a;
					float riverTessellation;
					if (riverValue < 0.99f) riverTessellation = lerp(_TessellationUniform, _MinTessellationUniform, riverValue);
					else riverTessellation = 0;
					
					// Tessellate and Cull based on distance
					float distance = length(abs(_WorldSpaceCameraPos.xz - mul(unity_ObjectToWorld, patch[0].pos).xz));
					float distInterpolator = (distance - _DistanceCulling)/_DistanceCullingLerp;
					float distTessellation = clamp(lerp(_TessellationUniform, _MinTessellationUniform, distInterpolator), _MinTessellationUniform, _TessellationUniform);
					
					// Stop grass based on height
					float waterTessellation = _TessellationUniform;
					float mountainTessellation = _TessellationUniform;

					if (patch[0].pos.y > _AltitudeHeight - _AltitudeInterpolate/2) mountainTessellation = 0;
					
					// Stop grass based on water
					else if (patch[0].pos.y < _WaterLevel - _WaterLevelInterpolate)
					{
						waterTessellation = 0;
					}
					else if (patch[0].pos.y <= _WaterLevel)
					{
						waterTessellation = lerp(_TessellationUniform, _MinTessellationUniform,
							abs(_WaterLevel - patch[0].pos.y) / _WaterLevelInterpolate);
					}

					// Use the minimum tessellation of all the possible factors
					tessUniform = min(min(min(min(min(min(
						angleTessellation,
						pathTessellation),
						distTessellation),
						riverTessellation),
						waterTessellation),
						mountainTessellation),
						roadTessellation);
					
					// Stop grass based on distance
					if (_CullAtMaxDistance == 1 && distInterpolator > 1) tessUniform = 0;
				}

				f.edge[0] = tessUniform;
				f.edge[1] = tessUniform;
				f.edge[2] = tessUniform;
				f.inside = tessUniform;
				return f;
			}

			// -------------------------------DOMAIN SHADER----------------------------------------------
			// Interpolates the properties of the tessellated mesh
			[UNITY_domain("tri")]
			vertexOutput domain(TessellationFactors factors, OutputPatch<vertexInput, 3> patch, float3 barycentricCoordinates : SV_DomainLocation)
			{
				vertexInput v;

				#define MY_DOMAIN_PROGRAM_INTERPOLATE(fieldName) v.fieldName = \
					patch[0].fieldName * barycentricCoordinates.x + \
					patch[1].fieldName * barycentricCoordinates.y + \
					patch[2].fieldName * barycentricCoordinates.z;

				MY_DOMAIN_PROGRAM_INTERPOLATE(vertex)
				MY_DOMAIN_PROGRAM_INTERPOLATE(normal)
				MY_DOMAIN_PROGRAM_INTERPOLATE(tangent)
				MY_DOMAIN_PROGRAM_INTERPOLATE(uv_PathTexture);

				return vert(v);
			}

			// Grass blade parameters
			float4 _BladeBottomColor;
			float4 _BladeTopColor;
			float _BladeWidth;
    		float _BladeWidthOffsetRand;
    		float _BladeHeight;
    		float _BladeHeightOffsetRand;
			float _BladeBendAmount;

			// -------------------------------GEOMETRY SHADER----------------------------------------------

			// --------------UTILITY FUNCTIONS--------------
			// Following functions from Roystan's code:
			// (https://github.com/IronWarrior/UnityGrassGeometryShader)

			// Simple noise function, sourced from http://answers.unity.com/answers/624136/view.html
			// Extended discussion on this function can be found at the following link:
			// https://forum.unity.com/threads/am-i-over-complicating-this-random-function.454887/#post-2949326
			// Returns a number in the 0...1 range.
			float rand(float3 co)
			{
				return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 53.539))) * 43758.5453);
			}

			// Construct a rotation matrix that rotates around the provided axis, sourced from:
			// https://gist.github.com/keijiro/ee439d5e7388f3aafc5296005c8c3f33
			float3x3 angleAxis3x3(float angle, float3 axis)
			{
				float c, s;
				sincos(angle, s, c);

				float t = 1 - c;
				float x = axis.x;
				float y = axis.y;
				float z = axis.z;

				return float3x3
				(
					t * x * x + c, t * x * y - s * z, t * x * z + s * y,
					t * x * y + s * z, t * y * y + c, t * y * z - s * x,
					t * x * z - s * y, t * y * z + s * x, t * z * z + c
				);
			}

			// Wind map and properties
			sampler2D _WindMap;
			float4 _WindMap_ST;
			float _WindScaleX;
			float _WindScaleZ;
			float _WindPower;

			// Creates a grass blade based on the parameters and properties provided
			void createGrassBlade(vertexOutput IN : SV_POSITION, inout TriangleStream<geometryOutput> triStream)
			{
				geometryOutput o;

				// Utils to curve the blades with the terrain
				float3 normal = IN.normal;
				float4 tangent = IN.tangent;				
				float3 biNormal = cross(normal, tangent) * tangent.w;

				float3x3 tangentToLocal = float3x3(
					tangent.x, biNormal.x, normal.x,
					tangent.y, biNormal.y, normal.y,
					tangent.z, biNormal.z, normal.z
				);

				// scale the blace based on steepness of terrain
				float angle = 1 - getAngle(normal, float3(0,1,0));
				float lowerGrassTH = _ThresholdGD + _InterpolateGD;
				float upperDirtTH = _ThresholdGD - _InterpolateGD;

				// Change the size of grass blade based on steepness
				float steepSize = lerp(0, 1, pow((angle - upperDirtTH)/(lowerGrassTH-upperDirtTH), 1.2));
				
				// change size based on path closeness
				float pathSize = 1;
				float pathValue = tex2Dlod(_PathTexture,float4(IN.uv_PathTexture, 0, 0)).a;
				if (pathValue > 0.1) pathSize = lerp(1, 0, pathValue);

				// change size based on road closeness
				float roadSize = 1;
				float roadValue = tex2Dlod(_RoadTexture,float4(IN.uv_PathTexture, 0, 0)).a;
				if (roadValue > 0.01) roadSize = lerp(1, 0, roadValue);
				
				// change size based on river closeness
				float waterSize = 1;
				float waterValue = tex2Dlod(_UnderWaterTexture,float4(IN.uv_PathTexture, 0, 0)).a;
				if (waterValue > 0.001f) waterSize = lerp(1, 0, waterValue * 3);
				
				// change size based on water level
 				if (IN.pos.y <= _WaterLevel)
				{
					waterSize = min(waterSize, lerp(1, 0, (_WaterLevel - IN.pos.y) / _WaterLevelInterpolate));
				}

				// Size of the grass blade is the minimum of all the possible sizes from above properties
				float size = clamp(min(min(min(steepSize, pathSize), waterSize), roadSize), 0, 1);

				// Minimum blade size
				if (size < 0.05f) return;

				// Random grass blade rotation
				float3 position = IN.pos;
				float3x3 rotMatrix = angleAxis3x3(rand(position) * UNITY_TWO_PI, float3(0,0,1));
				float3x3 transformMatrix = mul(tangentToLocal, rotMatrix);

				// Random width and height addition
				float widthOffset = rand(position.zyx) * _BladeWidthOffsetRand;
				float heightOffset = rand(position.zyx) * _BladeHeightOffsetRand;

				// Wind movement
				float2 uv = position.xz * _WindMap_ST.xy + _WindMap_ST.zw + (float4(_WindScaleX, _WindScaleZ, 0, 0) * _Time.y);
				float2 windMapSample = (tex2Dlod(_WindMap, float4(uv, 0, 0)).xy * 2 - 1) * _WindPower;
				float3 wind = normalize(float3(windMapSample, 0));
				float3x3 rotWindMatrix = angleAxis3x3(UNITY_PI * windMapSample, wind);
				
				// Bottom right vertex
				float4 bladeVertexPosition = float4(0, (_BladeWidth + widthOffset) * size, 0, 1);
				o.pos = UnityObjectToClipPos(position + mul(transformMatrix, bladeVertexPosition));
				o.color = _BladeBottomColor;
				triStream.Append(o);

				// Bottom left vertex
				bladeVertexPosition = float4(0, -(_BladeWidth + widthOffset) * size, 0, 1);
				o.pos = UnityObjectToClipPos(position + mul(transformMatrix, bladeVertexPosition));
				o.color = _BladeBottomColor;
				triStream.Append(o);

				// Random grass blade bend
				float3x3 rotBendMatrix = angleAxis3x3((rand(position.xzx)) * _BladeBendAmount * UNITY_PI * 0.5, float3(0, -1, 0));
				transformMatrix = mul(mul(mul(tangentToLocal, rotWindMatrix), rotMatrix), rotBendMatrix);

				// Top vertex
				bladeVertexPosition = float4(0, 0, (_BladeHeight + heightOffset) * size, 1);
				o.pos = UnityObjectToClipPos(position + mul(transformMatrix, bladeVertexPosition));
				o.color = _BladeTopColor;
				triStream.Append(o);

			}

			// Geometry shader
			[maxvertexcount(9)]
			void geo(triangle vertexOutput IN[3] : SV_POSITION, inout TriangleStream<geometryOutput> triStream)
			{
				createGrassBlade(IN[1], triStream);
			}

			// -------------------------------FRAGMENT SHADER----------------------------------------------			
			float4 frag (geometryOutput i) : SV_Target
			{
				// Set the color based on set color from user in vertex color + ambient light
				return i.color * UNITY_LIGHTMODEL_AMBIENT + i.color;
			}
			ENDCG
		}
    }
    FallBack "Diffuse"
}
