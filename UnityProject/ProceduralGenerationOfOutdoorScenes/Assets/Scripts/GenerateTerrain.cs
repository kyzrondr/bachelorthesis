using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class GenerateTerrain : MonoBehaviour
{
    [SerializeField] public int seed;
    [SerializeField] public Transform parent;
    [SerializeField] public float perlinScale = 120f;
    [SerializeField] public float amplitude = 30f;
    [SerializeField] [Range(1, 15)] public int numberOfOctaves = 6;
    [SerializeField] public int wrapping = 23;
    [SerializeField] public Vector2Int numberOfChunks = new Vector2Int(5,5);
    [SerializeField] public Material material;
    [SerializeField][ReadOnlyInspectorAttribute] private ChunkManager chunkManager;
    
    // Readonly not to overwhelm the user, it can be set in the chunk manager after generation
    private float lacunarity = 1.8f;
    private float persistence = 0.4f;

    // Ensures the wrapping is constraint to [3, inf]
    public void SetNewWrappingButton(int newWrapping)
    {
        if (newWrapping < 3) newWrapping = 3;
        wrapping = newWrapping;
    }
    
    // Generates the terrain and feature gnerators
    public void GenerateButton()
    {
        double startTime = EditorApplication.timeSinceStartup;
        Random.InitState(seed);
        float randomOffset = (Random.value * 200000) - 100000;    // Unity's perlin noise works well only in range [-100 000, 100 000]

        if (parent == null)
        {
            parent = new GameObject().transform;
            parent.name = "Terrain";
        }
        
        // Creates chunk manager with set parameters
        Transform chunks = new GameObject().transform;
        chunks.parent = parent;
        chunks.name = "ChunksManager";
        chunkManager = chunks.gameObject.AddComponent<ChunkManager>();
        chunkManager.InitializeParameters(randomOffset, perlinScale, numberOfOctaves, amplitude, lacunarity, persistence, wrapping, material);
        
        // Creates water generator
        Transform generateWater = new GameObject().transform;
        generateWater.parent = parent;
        generateWater.name = "GenerateWater";
        GenerateWater waterGenerator = generateWater.gameObject.AddComponent<GenerateWater>();
        waterGenerator.Initialize(chunkManager);
        
        // Creates hydraulic erosion generator
        Transform generateHydraulicErosion = new GameObject().transform;
        generateHydraulicErosion.parent = parent;
        generateHydraulicErosion.name = "GenerateHydraulicErosion";
        generateHydraulicErosion.gameObject.AddComponent<GenerateHydroErosion>();
        
        // Creates road generator
        Transform generateRoad = new GameObject().transform;
        generateRoad.parent = parent;
        generateRoad.name = "GenerateRoad";
        GenerateRoad roadGenerator = generateRoad.gameObject.AddComponent<GenerateRoad>();
        roadGenerator.Initialize(chunkManager);
        
        // Creates path generator
        Transform generatePath = new GameObject().transform;
        generatePath.parent = parent;
        generatePath.name = "GeneratePath";
        GeneratePath pathGenerator = generatePath.gameObject.AddComponent<GeneratePath>();
        pathGenerator.Initialize(chunkManager);


        // Chunk creation
        for (int x = 0; x < numberOfChunks.x; x++)
        {
            for (int y = 0; y < numberOfChunks.y; y++)
            {
                chunkManager.CreateChunk(new Vector2Int(x,y));
            }
        }
        double endTime = EditorApplication.timeSinceStartup;

        Debug.Log("Generated Chunk Manager and Chunks. In " + Math.Round((endTime - startTime) * 1000) + " milliseconds.");
    }
}
