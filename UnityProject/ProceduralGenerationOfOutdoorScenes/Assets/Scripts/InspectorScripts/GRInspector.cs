using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenerateRoad))]
public class GRInspector : Editor
{
    private SerializedProperty _controlPoints;
    private SerializedProperty _roadWidth;
    private SerializedProperty _roadSmoothWidth;
    private SerializedProperty _roadMaxYOffset;
    private SerializedProperty _showControlPoints;
    private SerializedProperty _generateRoadLines;
    
    private bool _controlPointsFoldout = true;
    private bool _roadFoldout = true;
    private bool _isAddingControlPoints = false;

    // Set the used properties on enable
    private void OnEnable()
    {
        _controlPoints = serializedObject.FindProperty("controlPointsList");
        _roadWidth = serializedObject.FindProperty("roadWidth");
        _roadSmoothWidth = serializedObject.FindProperty("roadSmoothWidth");
        _roadMaxYOffset = serializedObject.FindProperty("roadMaxYOffset");
        _showControlPoints = serializedObject.FindProperty("showControlPoints");
        _generateRoadLines = serializedObject.FindProperty("generateRoadLines");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GenerateRoad generateRoad = (GenerateRoad) target;

        // Control Points Foldout
        _controlPointsFoldout = EditorGUILayout.Foldout(_controlPointsFoldout, "Road Control points", EditorStyles.foldoutHeader);
        if (_controlPointsFoldout)
        {
            // Set text of adding button
            string buttonText;
            if (_isAddingControlPoints)
            {
                buttonText = "Stop Manually Adding Points";
            }
            else
            {
                buttonText = "Start Manually Adding Points";
            }
        
            // Adding manual point button
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(buttonText))
            {
                _isAddingControlPoints = !_isAddingControlPoints;
            }
            EditorGUILayout.EndHorizontal();
        
            // Control point field
            EditorGUILayout.PropertyField(_controlPoints, new GUIContent("Control Points:", "Control points of the road"));

            EditorGUILayout.PropertyField(_showControlPoints, new GUIContent("Show Control points:", "Toggles showing of the control points."));
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
        
            // Reset points button
            if (GUILayout.Button("Reset points"))
            {
                generateRoad.ResetControlPointsButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            
            // Reset all expect 1st and last button
            if (GUILayout.Button("Reset except 1st and last"))
            {
                generateRoad.ResetControlPointsWithoutFirstLastButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            
        }
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);

        // Mesh alteration section
        _roadFoldout = EditorGUILayout.Foldout(_roadFoldout, "Road Mesh Alteration", EditorStyles.foldoutHeader);
        if (_roadFoldout)
        {
            // Road parameters
            EditorGUILayout.PropertyField(_generateRoadLines, new GUIContent("Generate Road Lines:", "Generates lines on the edges of the road. (color can be set in material)"));
            EditorGUILayout.PropertyField(_roadWidth, new GUIContent("Width Of Roads:", "With of all roads on the terrain."));
            EditorGUILayout.PropertyField(_roadSmoothWidth, new GUIContent("Road Smooth Width:", "Distance from road edge to smooth it with terrain."));
            EditorGUILayout.PropertyField(_roadMaxYOffset, new GUIContent("Road Max Y Offset:", "The maximum height difference the road can be offsetted from the terrain."));

            EditorGUILayout.Space();
        
            // Generate road button
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Generate Road"))
            {
                generateRoad.GenerateRoadButton();
                SceneView.RepaintAll();
            }
            // Reset road button
            if (GUILayout.Button("Reset Road"))
            {
                generateRoad.ResetRoadData();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);
        
        serializedObject.ApplyModifiedProperties();
    }

    // Used for raycasting from camera to terrain
    private void OnSceneGUI()
    {
        if(!_isAddingControlPoints) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray rayToCast = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            
            if (Physics.Raycast(rayToCast, out hitInfo))
            {
                GenerateRoad generateRoad = (GenerateRoad) target;
                generateRoad.AddRoadPoint(hitInfo.point);
                
            }
            Event.current.Use();
        }

    }
}
