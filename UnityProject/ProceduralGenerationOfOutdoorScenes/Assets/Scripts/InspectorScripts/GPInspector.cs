using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GeneratePath))]
public class GPInspector : Editor
{
    private SerializedProperty _controlPoints;
    private SerializedProperty _travelDistance;
    private SerializedProperty _maxWalkableSteepness;
    private SerializedProperty _maxStepsUntilFail;
    private SerializedProperty _maxWalkableSlope;
    private SerializedProperty _steepnessMult;
    private SerializedProperty _slopeMult;
    private SerializedProperty _directionGoalMult;
    private SerializedProperty _sameDirectionMult;
    private SerializedProperty _useDistanceInDirectionGoal;
    private SerializedProperty _pathCarveDepth;
    private SerializedProperty _pathWidth;
    private SerializedProperty _pathSmoothWidth;
    private SerializedProperty _pathYOffset;
    private SerializedProperty _nRandomPaths;
    private SerializedProperty _showControlPoints;

    private bool _isAddingControlPoints = false;
    private bool _controlPointsFoldout = true;
    private bool _pathFoldout = true;
    private bool _randomFoldout = true;
    
    // Set the used properties on enable
    private void OnEnable()
    {
        _controlPoints = serializedObject.FindProperty("controlPointsList");
        _travelDistance = serializedObject.FindProperty("travelDistance");
        _maxWalkableSteepness = serializedObject.FindProperty("maxWalkableSteepness");
        _maxWalkableSlope = serializedObject.FindProperty("maxWalkableSlope");
        _maxStepsUntilFail = serializedObject.FindProperty("maxStepsUntilFail");
        _sameDirectionMult = serializedObject.FindProperty("sameDirectionMult");
        _directionGoalMult = serializedObject.FindProperty("directionGoalMult");
        _slopeMult = serializedObject.FindProperty("slopeMult");
        _steepnessMult = serializedObject.FindProperty("steepnessMult");
        _useDistanceInDirectionGoal = serializedObject.FindProperty("useDistanceInDirectionGoal");
        _pathCarveDepth = serializedObject.FindProperty("pathCarveDepth");
        _pathWidth = serializedObject.FindProperty("pathWidth");
        _pathSmoothWidth = serializedObject.FindProperty("pathSmoothWidth");
        _pathYOffset = serializedObject.FindProperty("pathYOffset");
        _nRandomPaths = serializedObject.FindProperty("nRandomPaths");
        _showControlPoints = serializedObject.FindProperty("showControlPoints");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GeneratePath generatePath = (GeneratePath) target;

        // Control Points Foldout
        _controlPointsFoldout = EditorGUILayout.Foldout(_controlPointsFoldout, "Path Control points", EditorStyles.foldoutHeader);
        if (_controlPointsFoldout)
        {
            // Set text of adding button
            string buttonText;
            if (_isAddingControlPoints)
            {
                buttonText = "Stop Manually Adding Points";
            }
            else
            {
                buttonText = "Start Manually Adding Points";
            }
        
            // Adding manual point button
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(buttonText))
            {
                _isAddingControlPoints = !_isAddingControlPoints;
            }
            EditorGUILayout.EndHorizontal();
        
            // Control point field
            EditorGUILayout.PropertyField(_controlPoints, new GUIContent("Control Points:", "Control points of the path"));

            EditorGUILayout.PropertyField(_showControlPoints,
                new GUIContent("Show Control points:", "Toggles showing of the control points."));
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
        
            // Reset points button
            if (GUILayout.Button("Reset points"))
            {
                generatePath.ResetControlPointsButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            
            // Reset all expect 1st and last button
            if (GUILayout.Button("Reset except 1st and last"))
            {
                generatePath.ResetControlPointsWithoutFirstLastButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        
            EditorGUILayout.Space(20);

            // Pathfinding settings
            EditorGUILayout.PropertyField(_travelDistance, new GUIContent("Travel Distance:", "How far will the agent travel in one step."));
            EditorGUILayout.Space(20);

            // Local Pathfinding settings
            EditorGUILayout.LabelField("Local path finding:" , EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_maxStepsUntilFail, new GUIContent("No. steps until failing:", "Maximum number of steps between control points, that the agent can take."));
            EditorGUILayout.PropertyField(_maxWalkableSteepness, new GUIContent("Max walkable steepness:", "Maximum angle Y between two steps."));
            EditorGUILayout.PropertyField(_maxWalkableSlope, new GUIContent("Max walkable slope:", "Maximum steepness of terrain in control point."));

            // Reward parameters
            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("Reward Parameters:" , EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_steepnessMult, new GUIContent("Steepness mult.:", "Multiplier of steepness in reward function calculation in pathfinding."));
            EditorGUILayout.PropertyField(_slopeMult, new GUIContent("Slope mult.:", "Multiplier of slope in reward function calculation in pathfinding."));
            EditorGUILayout.PropertyField(_sameDirectionMult, new GUIContent("Same Direction mult.:", "Multiplier of walking in similar direction in reward function calculation in pathfinding."));
            EditorGUILayout.PropertyField(_directionGoalMult, new GUIContent("Direction mult.:", "Multiplier of direction to the goal in reward function calculation in pathfinding. Basically how much towards the goal the agent goes."));
            EditorGUILayout.PropertyField(_useDistanceInDirectionGoal, new GUIContent("Divide dir. by dist.:", "If enabled it divides the direction to the goal by distance. Thus making more winding and non-direct paths. However paths are less convergent."));

            EditorGUILayout.Space(20);
            // Global Pathfinding settings
            EditorGUILayout.LabelField("Global path finding:" , EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Due to using Unity's experimental package for global NavMesh pathfinding please use the component below to change the parameters of the NavMeshSurface.", MessageType.Warning);
                
            // Send agent button
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Send Agent To Generate Control Points"))
            {
                generatePath.SendAgentButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);

        // Path alteration section
        _pathFoldout = EditorGUILayout.Foldout(_pathFoldout, "Path Mesh Alteration", EditorStyles.foldoutHeader);
        if (_pathFoldout)
        {
            // Path width
            EditorGUILayout.PropertyField(_pathWidth, new GUIContent("Width Of Paths:", "With of all paths on the terrain."));
            EditorGUILayout.PropertyField(_pathSmoothWidth,
                new GUIContent("Path Smooth Width:", "Distance from path edge to smooth it."));

            EditorGUILayout.Space();
        
            // Interaction parameters
            EditorGUILayout.LabelField("Interactive parameters:" , EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_pathCarveDepth, new GUIContent("Path Incline Balance:", "Distance the path carves into the terrain."));
            EditorGUILayout.PropertyField(_pathYOffset,
                new GUIContent("Path Y Offset:", "Offset of the path on the y axis"));
        
            // Generate path button
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Generate Path"))
            {
                generatePath.GeneratePathButton();
                SceneView.RepaintAll();
            }
            // Reset path button
            if (GUILayout.Button("Reset Path"))
            {
                generatePath.ResetPathData();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.LabelField("..........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................." , EditorStyles.boldLabel);

        // Random paths section
        _randomFoldout = EditorGUILayout.Foldout(_randomFoldout, "Random Path", EditorStyles.foldoutHeader);
        if (_randomFoldout)
        {
            EditorGUILayout.PropertyField(_nRandomPaths, new GUIContent("Num. of paths to generate.:", "Number of path to be generated during random generation."));
            
            // Generate random paths button
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Generate Random Paths"))
            {
                generatePath.GenerateRandomPathsButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

        }
        
        serializedObject.ApplyModifiedProperties();
    }

    // Used for raycasting from camera to terrain
    private void OnSceneGUI()
    {
        if(!_isAddingControlPoints) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray rayToCast = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            
            if (Physics.Raycast(rayToCast, out hitInfo))
            {
                GeneratePath generatePath = (GeneratePath) target;
                generatePath.AddPathPoint(hitInfo.point);
                
            }
            Event.current.Use();
        }

    }
}
