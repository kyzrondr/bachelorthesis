using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[ExecuteInEditMode]
public class ChunkManager : MonoBehaviour
{
    [Header("Chunks")]
    [SerializeField] private float offsetFromSeed;
    [SerializeField] private bool isHilly;
    [SerializeField] [Range(1, 15)] private int numberOfOctaves;
    [SerializeField] [Range(1f, 500f)] private float amplitude;
    [SerializeField] [Range(1f, 4f)] private float lacunarity = 1.8f;
    [SerializeField] [Range(0.3f, 0.8f)] private float persistence = 0.4f;
    [SerializeField] public Material material;
    [SerializeField] private int wrapping;

    [SerializeField] [Tooltip("X and Y are offsets, Z is scale of octave.")]
    private Vector3[] editOfOctaves;

    private readonly Hashtable _chunksTable = new Hashtable(); // Name => Chunk
    private readonly Hashtable _heightChangingChunks = new Hashtable(); // Name => Chunk
    private bool _isNotFreshlyOpened;
    private bool _postedWrappingChunkUpdate;
    private bool _postedParamChunkUpdate;

    // Constants
    private const float CHUNK_SIZE = 30;

    // ---------------------------------------------Initialization------------------------------------------------------
    // Sets the parameters during initialization
    public void InitializeParameters(float newOffsetFromSeed, float newPerlinScale, int newNumberOfOctaves,
        float newAmplitude, float newLacunarity, float newPersistence, int newWrapping, Material newMaterial)
    {
        offsetFromSeed = newOffsetFromSeed;
        numberOfOctaves = newNumberOfOctaves;
        amplitude = newAmplitude;
        lacunarity = newLacunarity;
        persistence = newPersistence;
        material = newMaterial;
        _lastWrapping = newWrapping;
        wrapping = newWrapping;
        editOfOctaves = new Vector3[numberOfOctaves];
        for (int i = 0; i < numberOfOctaves; i++)
        {
            editOfOctaves[i] = new Vector3(0, 0, newPerlinScale);
        }
    }

    // ---------------------------------------------Mono-behaviour------------------------------------------------------
    // In case of unity reload we need to put chunks back into tables
    private void Awake()
    {
        // Readd chunks
        if (_chunksTable.Count == 0)
        {
            foreach (Chunk chunk in transform.GetComponentsInChildren<Chunk>())
            {
                _chunksTable.Add(chunk.name, chunk);

                if (chunk.enableTerrainChange)
                {
                    if (!_heightChangingChunks.Contains(chunk.name)) _heightChangingChunks.Add(chunk.name, chunk);
                }
            }
        }

        // When unity project is opened private variables are not kept
        // I use this to refresh the chunks
        if (!_isNotFreshlyOpened)
        {
            RecalculateChunkWrapping();
            UpdateHeightOfChunks(true);
            _isNotFreshlyOpened = true;
        }
    }

    private void Update()
    {
        // Readd chunks if they are lost
        if (_chunksTable.Count == 0)
        {
            foreach (Chunk chunk in transform.GetComponentsInChildren<Chunk>())
            {
                _chunksTable.Add(chunk.name, chunk);

                if (chunk.enableTerrainChange)
                {
                    if (!_heightChangingChunks.Contains(chunk.name)) _heightChangingChunks.Add(chunk.name, chunk);
                }
            }
        }
        
        // If a wrapping update was posted recalculate the meshes of the chunks
        if (_postedWrappingChunkUpdate)
        {
            _postedWrappingChunkUpdate = false;
            RecalculateChunkWrapping();
        }
        
        // If a parameter update was posted recalculate the heights of vertices in chunks
        if (_postedParamChunkUpdate)
        {
            _postedParamChunkUpdate = false;
            UpdateHeightOfChunks(true);
        }
    }

    // Change in inspector
    private int _lastWrapping;

    private void OnValidate()
    {
        // Changed wrapping
        if (_lastWrapping != wrapping)
        {
            _postedWrappingChunkUpdate = true;
        }
        
        // Changed parameters
        else
        {
            _postedParamChunkUpdate = true;
        }

        _lastWrapping = wrapping;
    }

    // ---------------------------------------------Chunk-and-Water-Chunk-Utils-----------------------------------------
    private IDictionaryEnumerator _currentChunkEnumerator;

    // Creates a chunk name based on indices
    public static string CreateChunkName(int x, int y)
    {
        return "Chunk[" + x + "," + y + "]";
    }

    // Factory method to create a chunk at idxs and add it to chunks table
    public Chunk CreateChunk(Vector2Int idxs)
    {
        string chunkName = CreateChunkName(idxs.x, idxs.y);
     
        // Search the chunks table and don't create a new chunk if it already exists
        if (_chunksTable.Contains(chunkName))
        {
            Chunk prevChunk = (Chunk)_chunksTable[chunkName];
            if (prevChunk == null)
            {
                _chunksTable.Remove(chunkName);
            }
            else
            {
                return null;
            }
        }

        // Chunk creation
        GameObject chunk = new GameObject();
        chunk.transform.parent = transform;
        chunk.name = chunkName;
        Chunk chunkComponent = chunk.AddComponent<Chunk>();
        chunkComponent.Initialize(this, offsetFromSeed, idxs, editOfOctaves, numberOfOctaves, amplitude,
            lacunarity, persistence, wrapping, material, isHilly);
        _chunksTable.Add(chunkName, chunkComponent);

        // If user generated water level, we create a water chunk with a new chunk
        GenerateWater generateWater = transform.parent.GetComponentInChildren<GenerateWater>();
        if (generateWater != null && generateWater.IsWaterGenerated())
        {
            generateWater.GenerateWaterChunk(chunkComponent);
        }
        
        return chunkComponent;
    }

    // Retrieves the chunk component of given chunk from hashtable
    public Chunk GetChunk(int x, int y)
    {
        string chunkName = CreateChunkName(x, y);
        if (_chunksTable.Contains(chunkName))
        {
            return (Chunk) _chunksTable[chunkName];
        }

        return null;
    }

    // Recalculate chunk wrapping
    private void RecalculateChunkWrapping()
    {
        int tempWrap = wrapping;

        // Only odd wrapping is allowed
        if (wrapping % 2 != 1)
        {
            wrapping += 1;
            tempWrap += 1;
        }

        // 3 is the lowest possible odd mesh size
        if (wrapping < 3)
        {
            wrapping = 3;
            tempWrap = 3;
        }

        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null)
            {
                _chunksTable.Remove(chunk);
                continue;
            }
            chunk.ChangeWrapping(tempWrap);
        }
    }

    // Goes through all the chunks a notifies them to update their height
    private void UpdateHeightOfChunks(bool allChunks)
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null)
            {
                _chunksTable.Remove(chunk);
                continue;
            }
            if (allChunks) chunk.NotifyChunkToUpdateOnce();
            chunk.UpdateParameters(this, offsetFromSeed, editOfOctaves, numberOfOctaves, amplitude,
                lacunarity, persistence, material, isHilly);
        }
    }

    // Can be used by chunks to send a terrain request update
    public void SendTerrainUpdateRequest()
    {
        if (_currentChunkEnumerator == null || !_currentChunkEnumerator.MoveNext())
            _currentChunkEnumerator = _chunksTable.GetEnumerator();

        _currentChunkEnumerator.Reset();

        UpdateHeightOfChunks(false);
    }

    // Returns the size of a chunk
    public float GetChunkSize()
    {
        return CHUNK_SIZE;
    }

    // Returns all chunks as a ICollection (can contain nulls)
    public ICollection GetAllChunks()
    {
        return _chunksTable.Values;
    }
    
    // Returns an up-to-date list of all chunks as a ICollection (can contain nulls)
    public ICollection GetAllChunksTotal()
    {
        _chunksTable.Clear();
        
        foreach (Chunk chunk in transform.GetComponentsInChildren<Chunk>())
        {
            _chunksTable.Add(chunk.name, chunk);
        }
        
        return _chunksTable.Values;
    }

    // -----------------------------------Height-Changing-Chunks-Utils--------------------------------------------------
    // Returns the height changing chunks. Called by chunks to update height.
    public ICollection GetHeightChangingChunks()
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk.enableTerrainChange && !_heightChangingChunks.ContainsKey(chunk.name))
                _heightChangingChunks.Add(chunk.name, chunk);
        }
        return _heightChangingChunks.Values;
    }

    // Adds a chunk to the heightChanging chunks
    public void AddHeightChangingChunk(Chunk chunk)
    {
        if (_heightChangingChunks.Contains(chunk.name)) return;

        _heightChangingChunks.Add(chunk.name, chunk);
        _currentChunkEnumerator = _heightChangingChunks.GetEnumerator();

        UpdateHeightOfChunks(false);
    }

    // Removes a chunk from the heightChanging chunks
    public void RemoveHeightChangingChunk(Chunk chunk)
    {
        if (!_heightChangingChunks.Contains(chunk.name)) return;

        _heightChangingChunks.Remove(chunk.name);
        _currentChunkEnumerator = _heightChangingChunks.GetEnumerator();

        UpdateHeightOfChunks(true);
    }

    public int GetWrapping()
    {
        return wrapping;
    }

    // ---------------------------------------------Path-Utils----------------------------------------------------------
    // Updates the path parameters in all chunks
    public void UpdatePathParameters(float newDepth, float yOffset)
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetPathCarveDepthAndOffset(newDepth, yOffset);
        }
    }

    // Resets the path texture in all chunks
    public void ResetPathData()
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetPathTexture(null, true);
        }
    }

    // ---------------------------------------------Road-Utils----------------------------------------------------------
    // Updates the road parameters in all chunks
    public void UpdateRoadParameters(float yOffset)
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetRoadParameters(yOffset);
        }
    }

    // Resets the road texture in all chunks
    public void ResetRoadData()
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetRoadTexture(null, true);
        }
    }

    // ---------------------------------------------Erosion-Utils-------------------------------------------------------
    // Updates the erosion parameters in all chunks
    public void UpdateErosionParameters(float erosionStrength)
    {
        
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetErosionStrength(erosionStrength);
        }
    }

    // Resets the erosion texture in all chunks
    public void ResetErosionData()
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetErosionTexture(null, true);
        }
    }

    // ---------------------------------------------Water-Utils---------------------------------------------------------
    // Updates the water parameters in all chunks
    public void UpdateWaterParameters(float waterMaxYOffset)
    {
        // Chunk carving
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetWaterParameters(waterMaxYOffset);
        }
    }

    // Resets the water texture in all chunks
    public void ResetWaterData()
    {
        foreach (Chunk chunk in _chunksTable.Values)
        {
            if (chunk == null) continue;
            chunk.NotifyChunkToUpdateOnce();
            chunk.SetWaterTexture(null, true);
        }
    }
    
    // ----------------------------------------------Utils--------------------------------------------------------------
    public float GetAmplitude()
    {
        return amplitude;
    }

}
