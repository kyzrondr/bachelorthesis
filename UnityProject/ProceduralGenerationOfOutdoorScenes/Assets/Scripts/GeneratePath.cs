using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class GeneratePath : MonoBehaviour
{
    // I use a list to show the user the individual points
    [SerializeField] private List<Vector3> controlPointsList = new List<Vector3>();
    [SerializeField] private bool showControlPoints = true;
    
    [SerializeField] private float travelDistance = 5f;
    [SerializeField] [Range(0.0f, 90.0f)] private float maxWalkableSteepness = 30f;
    [SerializeField] [Range(0.0f, 90.0f)] private float maxWalkableSlope = 45f;
    [SerializeField] private int maxStepsUntilFail = 300;
    
    [SerializeField] [Range(0.1f, 5f)] private float steepnessMult = 2f;
    [SerializeField] [Range(0.1f, 5f)] private float slopeMult = 3f;
    [SerializeField] [Range(0.1f, 5f)] private float directionGoalMult = 4f;
    [SerializeField] [Range(0.1f, 5f)] private float sameDirectionMult = 1f;
    [SerializeField] private bool useDistanceInDirectionGoal = true;
    
    [SerializeField] private float pathWidth = 1f;
    [SerializeField] private float pathSmoothWidth = 4f;
    [SerializeField] private float pathCarveDepth = -15f;
    [SerializeField] private float pathYOffset = -0.2f;

    [SerializeField] private int nRandomPaths = 5;

    // The inner representation of the controlPointsList, used so that I can easily insert points between other points
    private LinkedList<Vector3> _controlPointsLinkedList = new LinkedList<Vector3>();
    private ChunkManager _chunkManager;
    private bool _isNotFreshlyOpened;
    private bool _update;

    // Constants
    private const int PATH_RESOLUTION = 128;

    // ---------------------------------------------Mono-behaviour------------------------------------------------------
    private void Start()
    {
        // When entering play mode or reloading project the references n private variables are not kept
        if (!_isNotFreshlyOpened)
        {
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
            _isNotFreshlyOpened = true;

            _update = true;
            
            if (_navMeshSurface == null)
            {
                _navMeshSurface = GetComponent<NavMeshSurface>();
            }
        }
    }

    private void Update()
    {
        // If interactive parameters update was posted this update the chunks
        if (_update)
        {
            _update = false;
            _chunkManager.UpdatePathParameters(pathCarveDepth, pathYOffset);
        }
    }

    // On inspector change
    private float _lastCarveHeight;
    private float _lastYOffset;
    private void OnValidate()
    {        
        _controlPointsLinkedList.Clear();
        foreach (Vector3 controlPoint in controlPointsList)
        {
            _controlPointsLinkedList.AddLast(controlPoint);
        }

        // Check references
        if (_navMeshSurface == null)
        {
            _navMeshSurface = GetComponent<NavMeshSurface>();
        }
        if (_chunkManager == null)
        {
            _chunkManager = transform.parent.GetComponentInChildren<ChunkManager>();
        }

        // Check changes
        if (_lastCarveHeight != pathCarveDepth || _lastYOffset != pathYOffset)
        {
            _lastCarveHeight = pathCarveDepth;
            _lastYOffset = pathYOffset;
            _chunkManager.UpdatePathParameters(pathCarveDepth, pathYOffset);
        }

        // Validate
        if (nRandomPaths < 0)
        {
            nRandomPaths = 0;
        }

    }

    // ---------------------------------------------Initialization------------------------------------------------------
    // Initializes the generator
    public void Initialize(ChunkManager chunkManager)
    {
        _chunkManager = chunkManager;
        if(_navMeshSurface == null) _navMeshSurface = gameObject.AddComponent<NavMeshSurface>();
    }

    // ----------------------------------------------Point-utils--------------------------------------------------------
    // Add a control point to the end of the control point list and linked list
    public void AddPathPoint(Vector3 point)
    {
        _controlPointsLinkedList.AddLast(point);
        controlPointsList.Add(point);
    }

    // Copies the data from the linked list to the normal list after agent point generation
    private void CopyFromLinkedListToList()
    {
        controlPointsList.Clear();
        foreach (Vector3 controlPoint in _controlPointsLinkedList)
        {
            controlPointsList.Add(controlPoint);
        }
    }

    // Resets all added control points
    public void ResetControlPointsButton()
    {
        _controlPointsLinkedList.Clear();
        controlPointsList.Clear();
    }
    
    // Resets all control points except the first and last one
    public void ResetControlPointsWithoutFirstLastButton()
    {
        if (controlPointsList.Count <= 0) return;
        if (_controlPointsLinkedList.Count <= 0) return;
        
        Vector3 first = _controlPointsLinkedList.First.Value;
        Vector3 last = _controlPointsLinkedList.Last.Value;
        _controlPointsLinkedList.Clear();
        controlPointsList.Clear();

        AddPathPoint(first);
        AddPathPoint(last);
    }

    // Sends the global and local agent
    public void SendAgentButton()
    {
        SendGlobalAgentButton(true);
        SendLocalAgentButton();
    }

    //---------------------------LOCAL-AGENT----------------------------------------------------------------------------
    // Sends an agent through the control points to better approximate the path using the least steepness
    private void SendLocalAgentButton()
    {
        // Path needs two points
        if (_controlPointsLinkedList.Count < 2) return;

        // Temp var
        float tolerantDistance = travelDistance + 1;
        
        // Enumerator
        IEnumerator enumerator = _controlPointsLinkedList.GetEnumerator();
        enumerator.MoveNext();
        Vector3 currentPosition = (Vector3)enumerator.Current;
        
        // New linked list for result
        LinkedList<Vector3> newControlPoints = new LinkedList<Vector3>();
        newControlPoints.AddLast(currentPosition);
        
        // Go through all the control points
        while (enumerator.MoveNext())
        {
            Vector3 currentTarget = (Vector3)enumerator.Current;
            Vector3 lastDirection = Vector3.zero;

            // Find points between two points
            bool foundSolution = false;
            for (int i = 0; i < maxStepsUntilFail; i++)
            {
                float distanceToControlPoint = Vector3.Distance(currentTarget, currentPosition);
                
                // Break if too close to control point (No need to generate new points)
                if (distanceToControlPoint <= tolerantDistance)
                {
                    currentPosition = currentTarget;
                    newControlPoints.AddLast(currentTarget);
                    foundSolution = true;
                    break;
                }
                
                Vector3 newPoint = FindNextStep(currentTarget, currentPosition, lastDirection);
                if (newPoint == Vector3.zero) continue;

                // Adds the new point to new control points
                newControlPoints.AddLast(newPoint);
                lastDirection = newPoint - currentPosition;
                currentPosition = newPoint;
                
                // Break if too close to control point (control point == next Point)
                if (Vector3.Distance(currentTarget, currentPosition) <= tolerantDistance)
                {
                    currentPosition = currentTarget;
                    newControlPoints.AddLast(currentTarget);
                    foundSolution = true;
                    break;
                }
            }

            // If no solution was found stop the path-finding
            if (!foundSolution)
            {
                Debug.LogError("Couldn't a good path with current configuration!");
                return;
            }
        }

        _controlPointsLinkedList = newControlPoints;
        CopyFromLinkedListToList();
    }

    // Calculates the next best step from current point in the direction of the target
    private Vector3 FindNextStep(Vector3 currentTarget, Vector3 currentPosition, Vector3 lastDirection)
    {
        // Setup the direction rotation
        int rotationAngle = 3;
        int tries = 180/rotationAngle + 1;
        
        Vector3 bestPosition = Vector3.zero;
        float bestSteepnessCoef = float.PositiveInfinity;

        // Rotate agent the 90 left
        Vector3 candidatePosition = currentPosition;
        Vector3 candidateDirection = Quaternion.AngleAxis(-90 - rotationAngle, Vector3.up) * (currentTarget - currentPosition).normalized;
        candidateDirection.y = 0;
        
        // Calculate all possible direction
        float candidateSteepnessCoef;
        while (tries >= 0)
        {
            // Try different direction
            candidateDirection = (Quaternion.AngleAxis(rotationAngle, Vector3.up) * candidateDirection).normalized;
            candidateDirection.y = 0;
            tries--;
            
            // Raycast to get candidate real position
            RaycastHit hitInfo;
            candidatePosition = currentPosition + candidateDirection * travelDistance + Vector3.up * 500;
            if (!Physics.Raycast(candidatePosition, Vector3.down, out hitInfo))
            {
                continue;
            }
            candidatePosition = hitInfo.point;
            
            // Abs to successfully test steepness 
            Vector3 temp = candidatePosition - currentPosition;
            temp.y = Mathf.Abs(temp.y);

            // See if we find better steepness
            float steepness = 90 - Mathf.Abs(Vector3.Angle(Vector3.up, temp));
            float slope = Vector3.Angle(Vector3.up, hitInfo.normal);
            
            // If the current point is within the restrictions of slope and steepness evaluate it
            if (steepness < maxWalkableSteepness && slope < maxWalkableSlope)
            {
                float distanceFromTarget = 1;
                if (useDistanceInDirectionGoal)
                {
                    distanceFromTarget = Vector3.Distance(candidatePosition, currentTarget);
                }
                
                // Reward function
                candidateSteepnessCoef = steepness * steepnessMult
                                         + slope * slopeMult
                                         + Mathf.Abs(Vector3.Angle(candidateDirection,
                                             currentTarget - currentPosition)) * directionGoalMult * 7 / distanceFromTarget
                                         + Vector3.Angle(candidateDirection, lastDirection) * sameDirectionMult;
                
                // Set the new best candidate
                if (candidateSteepnessCoef <= bestSteepnessCoef)
                {
                    bestPosition = candidatePosition;
                    bestSteepnessCoef = candidateSteepnessCoef;
                }
            }
        }

        return bestPosition;
    }

    //---------------------------GLOBAL-AGENT---------------------------------------------------------------------------
    private NavMeshSurface _navMeshSurface;
    
    // Calculates the NavMesh and sends the agent to find a path between all control points
    private void SendGlobalAgentButton(bool updateNavMesh)
    {
        // Need start and end
        if (controlPointsList.Count < 2) return;

        // Enable NavMesh surface and build NavMesh
        if (!_navMeshSurface.enabled) _navMeshSurface.enabled = true;
        if (updateNavMesh) _navMeshSurface.BuildNavMesh();
        
        Vector3 currentPosition = _controlPointsLinkedList.First.Value;
        NavMeshPath path = new NavMeshPath();
        
        // For all set control points, find a path between them
        _controlPointsLinkedList.Clear();
        foreach (Vector3 point in controlPointsList)
        {
            NavMesh.CalculatePath(currentPosition, point, NavMesh.AllAreas, path);
            
            // For all found corner points add them to the control list
            foreach (Vector3 cornerPoint in path.corners)
            {
                _controlPointsLinkedList.AddLast(cornerPoint);

                currentPosition = cornerPoint;
            }     
        }
        
        CopyFromLinkedListToList();

        _navMeshSurface.enabled = false;
    }

    // -------------------------Texture-Generation----------------------------------------------------------------------
    // Generates a path based on the given control points in the chunk textures
    public void GeneratePathButton()
    {
        if (controlPointsList.Count < 2) return;
        
        transform.parent.GetComponentInChildren<GenerateWater>()?.DisableAllWaterChunks();

        // Width of path in tex space
        int widthOfPathTexSpace = (int)((1.0f / _chunkManager.GetChunkSize()) * pathWidth * PATH_RESOLUTION);
        int widthOfPathTexSpaceDiv2 = widthOfPathTexSpace / 2;

        // Width of smoothing in tex space
        int widthOfSmooth = (int)((1.0f / _chunkManager.GetChunkSize()) * pathSmoothWidth * PATH_RESOLUTION);
        int widthOfSmoothTexSpaceDiv2 = widthOfSmooth / 2;
        
        int totalWidthDiv2 = widthOfPathTexSpaceDiv2 + widthOfSmoothTexSpaceDiv2;
        
        // Size of one texture pixel in world units
        float pixelInWorld = _chunkManager.GetChunkSize() / PATH_RESOLUTION;

        // Store all chunks with their texture
        Dictionary<Chunk, Texture2D> chunkTextures = new Dictionary<Chunk, Texture2D>();
        ICollection allChunks = _chunkManager.GetAllChunksTotal();
        foreach (Chunk chunk in allChunks)
        {
            Texture2D texture2D = chunk.GetPathTexture();
            if (texture2D.width != PATH_RESOLUTION)
            {
                texture2D = CreateNewPathTexture(PATH_RESOLUTION, chunk.name);
            }
            chunkTextures.Add(chunk, texture2D);
        }

        // Cross drawing utils
        Chunk lastChunk = null;
        Texture2D[] neighborTextures = new Texture2D[9];
        
        // Go through the doubles of points
        for (int i = 0; i < controlPointsList.Count - 1; i++)
        {
            Vector3 startPoint = controlPointsList[i];
            Vector3 endPoint = controlPointsList[i + 1];
            Vector3 currPoint = startPoint;

            // Calculate direction
            Vector3 direction = (endPoint - currPoint).normalized;
            Vector3 directionRight = (Quaternion.AngleAxis(90, Vector3.up) * direction).normalized;
            directionRight.y = 0;
            
            float currToEndDistance = Vector3.Distance(currPoint, endPoint);
            
            // Draw on texture moving by one pixel at a time 
            while (currToEndDistance >= pixelInWorld)
            {
                // Look to right and left for gradient
                bool hit = Physics.Raycast(currPoint + Vector3.up * 30f, Vector3.down, out RaycastHit hitInfoCurr);
                bool hitRight = Physics.Raycast(currPoint + Vector3.up * 30f + directionRight * pathWidth/2, Vector3.down, out RaycastHit hitInfoRight);
                bool hitLeft = Physics.Raycast(currPoint + Vector3.up * 30f - directionRight * pathWidth/2, Vector3.down, out RaycastHit hitInfoLeft);

                // If raycast fails the path is outside of the terrain
                if (!hit || !hitRight || !hitLeft)
                {
                    transform.parent.GetComponentInChildren<GenerateWater>()?.EnableAllWaterChunks();
                    Debug.Log("Current path couldn't be created! Please create a path that fits on the terrain.");
                    return;
                }
                
                Chunk chunk = hitInfoCurr.collider.GetComponent<Chunk>();
                
                // Get texture
                Texture2D texture = chunkTextures[chunk];

                // Store neighbor textures
                if (lastChunk != chunk)
                {
                    Vector2Int indices = chunk.GetIndices();
                    Chunk tempChunk = null;

                    // Top left
                    tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y + 1);
                    if (tempChunk) neighborTextures[0] = chunkTextures[tempChunk];
                    else neighborTextures[0] = null;
                    
                    // Top middle
                    tempChunk = _chunkManager.GetChunk(indices.x, indices.y + 1);
                    if (tempChunk) neighborTextures[1] = chunkTextures[tempChunk];
                    else neighborTextures[1] = null;

                    // Top right
                    tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y + 1);
                    if (tempChunk) neighborTextures[2] = chunkTextures[tempChunk];
                    else neighborTextures[2] = null;

                    // Middle left
                    tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y);
                    if (tempChunk) neighborTextures[3] = chunkTextures[tempChunk];
                    else neighborTextures[3] = null;

                    // Middle middle
                    neighborTextures[4] = texture;

                    // Middle right
                    tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y);
                    if (tempChunk) neighborTextures[5] = chunkTextures[tempChunk];
                    else neighborTextures[5] = null;

                    // Bottom left
                    tempChunk = _chunkManager.GetChunk(indices.x - 1, indices.y - 1);
                    if (tempChunk) neighborTextures[6] = chunkTextures[tempChunk];
                    else neighborTextures[6] = null;

                    // Bottom middle
                    tempChunk = _chunkManager.GetChunk(indices.x, indices.y - 1);
                    if (tempChunk) neighborTextures[7] = chunkTextures[tempChunk];
                    else neighborTextures[7] = null;

                    // Bottom right
                    tempChunk = _chunkManager.GetChunk(indices.x + 1, indices.y - 1);
                    if (tempChunk) neighborTextures[8] = chunkTextures[tempChunk];
                    else neighborTextures[8] = null;

                    lastChunk = chunk;
                }
                
                
                // Get gradient in the line
                // Negative == terrain is lower on the right than left
                // Positive == terrain is higher on the right than left
                float gradient = hitInfoRight.point.y - hitInfoLeft.point.y;
                gradient = gradient > 0 ? 0.5f : -0.5f; 
                
                // Changes height on one side divided by the height difference between right and left
                float rightSideLerp = Mathf.Abs(hitInfoRight.point.y - hitInfoCurr.point.y);
                float leftSideLerp = Mathf.Abs(hitInfoLeft.point.y - hitInfoCurr.point.y);
                rightSideLerp /= pathWidth;
                leftSideLerp /= pathWidth;                                                    

                rightSideLerp = Mathf.Clamp(rightSideLerp, 0, 1);
                leftSideLerp = Mathf.Clamp(leftSideLerp, 0, 1);

                // Get world coords to texture coords
                Vector2 texPosFloat = hitInfoCurr.textureCoord * PATH_RESOLUTION;
                Vector2Int texPosInt = new Vector2Int(Mathf.RoundToInt(texPosFloat.x), Mathf.RoundToInt(texPosFloat.y));

                // Draw each pixel
                int startX = texPosInt.x - totalWidthDiv2;
                int endX = texPosInt.x + totalWidthDiv2;
                int startY = texPosInt.y - totalWidthDiv2;
                int endY = texPosInt.y + totalWidthDiv2;
                for (int x = startX; x <= endX; x++)
                {
                    for (int y = startY; y <= endY; y++)
                    {
                        int tempX = x;
                        int tempY = y;

                        texture = GetRightTextureForCoords(ref tempX, ref tempY, ref neighborTextures);
                        if (!texture) continue;

                        // Edge case where we need to duplicate the pixels on seams
                        int distX = x;
                        int distY = y;
                        if (distX <= -1) distX++;
                        if (distX >= PATH_RESOLUTION) distX--;
                        if (distY <= -1) distY++;
                        if (distY >= PATH_RESOLUTION) distY--;
                        float distanceFromPath = Vector2.Distance(texPosInt, new Vector2(distX, distY));
                        
                        // Is 0 when close to path 1 when on edge of path
                        float t = (distanceFromPath - widthOfPathTexSpaceDiv2) / widthOfSmoothTexSpaceDiv2;
                        t = Mathf.Clamp(t, 0, 1);
                        
                        // Intensity of set pixel not to override higher values
                        Color previousColor = texture.GetPixel(tempX, tempY);                                                
                        float prevIntensity = previousColor.a;

                        // Calculate the angle between the center of the path and current pixel
                        float angle = Vector2.SignedAngle((new Vector2(x, y) - texPosFloat), new Vector2(direction.x, direction.z));
                        bool isRightToPath = angle > 0;
                        
                        // Calculate the new color to be drawn
                        Color newColor;
                        if (isRightToPath)
                        {
                           newColor = Color.gray + Color.gray * t * (gradient * rightSideLerp);
                        }
                        else
                        {
                            newColor = Color.gray + Color.gray * t * (-gradient)* leftSideLerp;
                        }
                        
                        newColor.a = 1 - t;
                        
                        // Draw the pixel if intensity (alpha) is bigger
                        if (newColor.a >= prevIntensity) texture.SetPixel(tempX, tempY, newColor);
                    }
                }

                // Move towards end point
                currPoint = Vector3.Lerp(currPoint, endPoint, pixelInWorld/currToEndDistance * 1f); //headroom
                currToEndDistance = Vector3.Distance(currPoint, endPoint);
            }
        }
        
        transform.parent.GetComponentInChildren<GenerateWater>()?.EnableAllWaterChunks();

        // STORE ALL CHANGED TEXTURES, SMOOTH OUT THEN APPLY 
        foreach (var chunkTexturePair in chunkTextures)                                      
        {                                                                                    
            Texture2D tex = chunkTexturePair.Value;                                          
            tex.Apply();      
            chunkTexturePair.Key.NotifyChunkToUpdateOnce();
            chunkTexturePair.Key.SetPathTexture(tex, true); 
            chunkTexturePair.Key.SetPathCarveDepthAndOffset(pathCarveDepth, pathYOffset);
        }
    }

    // Returns the correct texture to draw in based on x and y coorinates
    private Texture2D GetRightTextureForCoords(ref int x, ref int y, ref Texture2D[] neighborTextures)
    {
        Texture2D texture = null;

        // Top left
        if (x < 0 && y >= PATH_RESOLUTION)
        {
            texture = neighborTextures[0];
            x += PATH_RESOLUTION;
            y -= PATH_RESOLUTION;
        }
        // Top middle
        else if (x >= 0 && x < PATH_RESOLUTION && y >= PATH_RESOLUTION)
        {
            texture = neighborTextures[1];
            y -= PATH_RESOLUTION;
        }
        // Top right
        else if (x >= PATH_RESOLUTION && y >= PATH_RESOLUTION)
        {
            texture = neighborTextures[2];
            x -= PATH_RESOLUTION;
            y -= PATH_RESOLUTION;
        }
        // Middle left
        else if (x < 0 && y >= 0 && y < PATH_RESOLUTION)
        {
            texture = neighborTextures[3];
            x += PATH_RESOLUTION;
        }
        // Middle middle
        else if (x >= 0 && x < PATH_RESOLUTION && y >= 0 && y < PATH_RESOLUTION)
        {
            texture = neighborTextures[4];
        }
        // Middle right
        else if (x >= PATH_RESOLUTION && y >= 0 && y < PATH_RESOLUTION)
        {
            texture = neighborTextures[5];
            x -= PATH_RESOLUTION;
        }
        // Bottom left
        else if (x < 0 && y < 0)
        {
            texture = neighborTextures[6];
            x += PATH_RESOLUTION;
            y += PATH_RESOLUTION;
        }
        // Bottom middle
        else if (x >= 0 && x < PATH_RESOLUTION && y < 0)
        {
            texture = neighborTextures[7];
            y += PATH_RESOLUTION;
        }
        // Bottom right
        else if (x >= PATH_RESOLUTION && y < 0)
        {
            texture = neighborTextures[8];
            x -= PATH_RESOLUTION;
            y += PATH_RESOLUTION;
        }

        return texture;
    }

    // Creates a new path texture to draw in
    private Texture2D CreateNewPathTexture(int resolution, string chunkName)
    {
        Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBAFloat, true);
        int resPow2 = resolution * resolution;
        Color[] pixels = new Color[resPow2];
        
        for (int i = 0; i < resPow2; i++)
        {
            pixels[i] = Color.black;
            pixels[i].a = 0;
        }
        
        tex.SetPixels(pixels);

        tex.filterMode = FilterMode.Bilinear;
        tex.wrapMode = TextureWrapMode.Clamp;
        tex.name = chunkName + "PathTex";
        
        tex.Apply();

        return tex;
    }

    // Generates random paths on the terrain
    public void GenerateRandomPathsButton()
    {
        // Get chunks
        ICollection chunksCollection = _chunkManager.GetAllChunks();
        int nChunks = chunksCollection.Count;
        float halfChunkSize = _chunkManager.GetChunkSize() / 2; 
        
        // Get all chunks
        List<Chunk> chunks = new List<Chunk>(nChunks);
        foreach (Chunk chunk in chunksCollection)
        {
            chunks.Add(chunk);
        }
        
        // Generate random paths
        for (int n = 0; n < nRandomPaths; n++)
        {
            // Reset control points after each path
            controlPointsList.Clear();
            _controlPointsLinkedList.Clear();

            // Choose random two chunks
            Chunk chunk1 = chunks[Random.Range(0,nChunks)];
            Chunk chunk2 = chunks[Random.Range(0,nChunks)];
            Vector3 chunk1Position = chunk1.transform.position;
            Vector3 chunk2Position = chunk2.transform.position;
            
            // Set begin and end point
            Vector3 chunk1Point = new Vector3(Random.Range(-halfChunkSize, halfChunkSize), 0, Random.Range(-halfChunkSize, halfChunkSize));
            Vector3 chunk2Point = new Vector3(Random.Range(-halfChunkSize, halfChunkSize), 0, Random.Range(-halfChunkSize, halfChunkSize));

            // Add position of chunk
            chunk1Point += chunk1Position;
            chunk2Point += chunk2Position;

            // Raycast to get correct height
            RaycastHit hitInfo1;
            RaycastHit hitInfo2;
            bool hit1 = Physics.Raycast(chunk1Point + Vector3.up * 500, Vector3.down, out hitInfo1);
            bool hit2 = Physics.Raycast(chunk2Point + Vector3.up * 500, Vector3.down, out hitInfo2);

            if (!hit1 || !hit2)
            {
                Debug.LogError("Raycast failed, should not happen, try again.");
                return;
            }
            
            AddPathPoint(hitInfo1.point);
            AddPathPoint(hitInfo2.point);
            
            // Global path find
            if (n == 0) SendGlobalAgentButton(true);
            else SendGlobalAgentButton(false);
            
            // Local path find to fill gaps
            SendLocalAgentButton();
            
            // Generate path texture
            GeneratePathButton();
        }
    }
    
    // Resets all the path data
    public void ResetPathData()
    {
        _chunkManager.ResetPathData();
    }

    //--------------------------------Utils-----------------------------------------------------------------------------
    // Draws the path points as red spheres on the terrain using Gizmos
    private void OnDrawGizmos()
    {
        if (!showControlPoints) return;
        
        Gizmos.color = Color.red;

        Vector3 lastPoint = Vector3.zero;
        
        foreach (Vector3 point in _controlPointsLinkedList)
        {
            if (lastPoint != Vector3.zero)
            {
                Debug.DrawLine(lastPoint, point, Color.red, 0f, false);
            }
            Gizmos.DrawSphere(point, 1f);
            lastPoint = point;
        }
    }
}
