using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class Chunk : MonoBehaviour
{
    // Height effector parameters
    [SerializeField] public bool enableTerrainChange = false;
    [SerializeField] public float heightOffset;
    [SerializeField] [Range(0.0f, float.PositiveInfinity)] public float heightRange;
    
    private ChunkManager _chunkManager;

    // Chunk parameters
    private float _offsetFromSeed;
    private int _numberOfOctaves;
    private int _wrappingOfPoints;
    private Vector3[] _editOfOctaves;
    private float _amplitude;
    private float _lacunarity;
    private float _persistence;
    private bool _isHilly;

    // Variables of chunk and component references
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;
    private MeshCollider _meshCollider;
    private Vector2Int _indexes;
    private MaterialPropertyBlock _materialPropertyBlock;
    
    // Path variables
    private const string PATH_FOLDER = "PathTextures/";
    private const int PATH_RESOLUTION = 128;
    private Texture2D _pathTexture;
    private float _pathBalance;
    private float _pathYOffset;
    
    // Road variables
    private const string ROAD_FOLDER = "RoadTextures/";
    private const int ROAD_RESOLUTION = 256;
    private Texture2D _roadTexture;
    private float _roadMaxYOffset;

    // Erosion variables
    private const string EROS_FOLDER = "ErosTextures/";
    private const int EROS_RESOLUTION = 256;
    private Texture2D _erosionTexture;
    private float _erosionStrength;
    
    // Water variables
    private const string WATER_FOLDER = "WaterTextures/";
    private const int WATER_RESOLUTION = 128;
    private Texture2D _waterTexture;
    private float _waterMaxYOffset;

    // Chunk constants
    private const float DEFAULT_SCALE = 90f;
    private const float CHUNK_SIZE = 30;
    private const int TEX128_RESOLUTION = 128;
    private const int TEX256_RESOLUTION = 256;

    // Generable texture types
    private enum TextureType
    {
        Path,
        Erosion,
        Road,
        Water
    }

    // ----------------------------------Mono-behaviour-----------------------------------------------------------------
    // Inspector change check variables
    private bool _lastEnableTerrainChange = false;
    private float _lastHeightOffset = 0f;
    private float _lastHeightRange = 0f;
    private bool _updateHeightNextFrame = false;
    // On Inspector change
    private void OnValidate()
    {
        // Add or remove chunk to height effectors
        if (enableTerrainChange != _lastEnableTerrainChange)
        {
            if (_chunkManager == null) return;
            
            if (enableTerrainChange)
            {
                _chunkManager.AddHeightChangingChunk(this);
            }
            else
            {
                _chunkManager.RemoveHeightChangingChunk(this);
            }

        }
        _lastEnableTerrainChange = enableTerrainChange;

        // Upon changing the height setting we need to push an update to chunk manager
        if (heightOffset != _lastHeightOffset || heightRange != _lastHeightRange)
        {
            if (heightRange != 0 && heightOffset != 0)
            {
                if (_chunkManager == null) return;
                _chunkManager.SendTerrainUpdateRequest();
            }
        }
        _lastHeightOffset = heightOffset;
        _lastHeightRange = heightRange;
    }
    
    private void Update()
    {
        // If update was posted, update the chunk mesh
        if (_updateHeightNextFrame)
        {
            UpdateHeight();
        }
    }

    //--------------------------------------------Chunk-Creation--------------------------------------------------------
    // Initializes all the essential variables of the chunk
    public void Initialize(ChunkManager chunkManager, float offsetFromSeed, Vector2Int indexes, Vector3[] editOfOctaves, int numberOfOctaves,
        float amplitude, float lacunarity, float persistence, int wrapping, Material material, bool isHilly)
    {
        
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();
        _meshFilter = gameObject.AddComponent<MeshFilter>();
        _meshCollider = gameObject.AddComponent<MeshCollider>();

        _chunkManager = chunkManager;
        _offsetFromSeed = offsetFromSeed;
        _indexes = indexes;
        _editOfOctaves = editOfOctaves;
        _wrappingOfPoints = wrapping;
        _numberOfOctaves = numberOfOctaves;
        _amplitude = amplitude;
        _lacunarity = lacunarity;
        _persistence = persistence;
        _isHilly = isHilly;
        
        _meshRenderer.material = material;
        _materialPropertyBlock = new MaterialPropertyBlock();
        
        // path texture
        _pathTexture = GetPathEmptyTexture();
        SetPathTexture(_pathTexture, true);

        // road texture
        _roadTexture = GetPathEmptyTexture();
        SetRoadTexture(_roadTexture, true);

        // erosion texture
        _erosionTexture = GetEmptyErosionTexture();
        SetErosionTexture(_erosionTexture, true);
        
        // water texture
        _waterTexture = GetEmptyWaterTexture();
        SetWaterTexture(_waterTexture, true);
        
        transform.position = new Vector3(indexes.x, 0.0f, indexes.y) * CHUNK_SIZE;
        CalculateMesh();
    }

    // Gets the indexes of the chunk from its name
    private void RecalculateIndexes()
    {
        string indexesString = name.Substring(6, name.Length-1-6);
        string[] twoIndexes = indexesString.Split(',');
        int x, y;
        int.TryParse(twoIndexes[0], out x);
        int.TryParse(twoIndexes[1], out y);
                
        _indexes = new Vector2Int(x,y);
    }

    // Creates the mesh of the chunk
    private void CalculateMesh()
    {
        CheckReferences();
        
        // Calculate the offset (distance) between points
        float pointPositionOffset;
        if (_wrappingOfPoints % 2 == 0) // Special case for even numbers, shouldn't be possible
        {
            pointPositionOffset = CHUNK_SIZE;
        }
        else
        {
            pointPositionOffset = CHUNK_SIZE / (_wrappingOfPoints - 1);
        }

        // Arrays for storing the mesh
        int numberOfVertices = _wrappingOfPoints * _wrappingOfPoints;
        int numberOfTriangles = (numberOfVertices - (_wrappingOfPoints + _wrappingOfPoints - 1)) * 2;
        Vector3[] vertices = new Vector3[numberOfVertices];
        Vector2[] uvCoords = new Vector2[numberOfVertices];
        int[] triangles = new int[numberOfTriangles * 3];
        int triangleIdx = 0;

        
        // Get used effectors
        List<Chunk> usedEffectors = GetUsedEffectors();
        
        // Neighbour chunks
        Chunk[] neighbours = new Chunk[8];
        neighbours[0] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y + 1);       // Top left
        neighbours[1] = _chunkManager.GetChunk(_indexes.x, _indexes.y + 1);             // Top
        neighbours[2] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y + 1);       // Top right
        neighbours[3] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y);             // Middle left
        neighbours[4] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y);             // Middle right
        neighbours[5] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y - 1);       // Bottom left
        neighbours[6] = _chunkManager.GetChunk(_indexes.x, _indexes.y - 1);             // Bottom
        neighbours[7] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y - 1);       // Bottom right
        
        // Calculate integer division by 2 of wrapping points, used in calculation the offset 
        int wrapDiv2 = _wrappingOfPoints / 2;

        // Create vertices, faces and uv coords
        for (int vertexIdx = 0; vertexIdx < numberOfVertices; vertexIdx++) 
        {
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset = (vertexIdx % _wrappingOfPoints) - wrapDiv2;
            int yGridOffset = (vertexIdx / _wrappingOfPoints) - wrapDiv2;

            // Vertices
            float x = xGridOffset * pointPositionOffset;
            float z = yGridOffset * pointPositionOffset;

            // Perlin noise calculation
            float height = GetHeightOfPoint(new Vector3(x, 0, z), ref usedEffectors);

            height = ConnectEdges(vertexIdx, new Vector3(x, 0, z), height, ref neighbours, ref usedEffectors);

            Vector3 vertexPosition = new Vector3(x, height, z);
            vertices[vertexIdx] = vertexPosition;

            // UV coords
            uvCoords[vertexIdx] = new Vector2((x + CHUNK_SIZE/2) / CHUNK_SIZE, (z + CHUNK_SIZE/2) / CHUNK_SIZE);
            
            // Faces (only when not last row and not last column)
            if ((vertexIdx + 1) % _wrappingOfPoints != 0 && vertexIdx / _wrappingOfPoints != _wrappingOfPoints - 1)
            {
                // First face of quad 
                triangles[triangleIdx++] = vertexIdx;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints + 1;
                
                // Second face of quad
                triangles[triangleIdx++] = vertexIdx;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints + 1;
                triangles[triangleIdx++] = vertexIdx + 1;
            }
        }
        
        // Set the generated vertices, tringles and uvs
        _meshFilter.sharedMesh = new Mesh {vertices = vertices, triangles = triangles, uv = uvCoords, name = this.name};
        RecalculateNormals();
        _meshFilter.sharedMesh.RecalculateBounds();
        _meshCollider.sharedMesh = _meshFilter.sharedMesh;
    }

    // Recalculates the normal vectors of the mesh
    private void RecalculateNormals()
    {
        List<Chunk> usedEffectors = GetUsedEffectors();
        Vector3[] vertices = _meshFilter.sharedMesh.vertices;
        Vector3[] vertexNormals = new Vector3[_wrappingOfPoints * _wrappingOfPoints];
        int triangleCount = (_wrappingOfPoints - 1) * (_wrappingOfPoints - 1) * 2;
        int numberOfVerticesBorder = (_wrappingOfPoints + 2) * (_wrappingOfPoints + 2);
        
        // Calculation is:   points in one row * faces * four sides + corner faces
        int numberOfBorderTriangles = (_wrappingOfPoints - 1) * 2 * 4 + 8;
        int borderWrapping = _wrappingOfPoints + 2;
        int borderWrapDiv2 = borderWrapping / 2;
            
        // Calculate the offset (distance) between points
        float pointPositionOffset;
        if (borderWrapping % 2 == 0) // Special case for even numbers shouldn't be possible
        {
            pointPositionOffset = CHUNK_SIZE + (CHUNK_SIZE/_wrappingOfPoints) * 2;
        }
        else
        {
            pointPositionOffset = (CHUNK_SIZE + (CHUNK_SIZE/(_wrappingOfPoints-1)) * 2)  / (borderWrapping - 1) ;
        }
        
        // Store the neighbouring chunks
        Chunk[] neighbours = new Chunk[8];
        neighbours[0] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y + 1);       // Top left
        neighbours[1] = _chunkManager.GetChunk(_indexes.x, _indexes.y + 1);             // Top
        neighbours[2] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y + 1);       // Top right
        neighbours[3] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y);             // Middle left
        neighbours[4] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y);             // Middle right
        neighbours[5] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y - 1);       // Bottom left
        neighbours[6] = _chunkManager.GetChunk(_indexes.x, _indexes.y - 1);             // Bottom
        neighbours[7] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y - 1);       // Bottom right
        
        // Loop through faces including an outer layer (to calculate normals without seams)
        int triangleCountWithBorders = triangleCount + numberOfBorderTriangles;
        for (int triangleIdx = 0; triangleIdx < triangleCountWithBorders; triangleIdx++)
        {
            int triangleRowIdx = triangleIdx / ((borderWrapping-1) * 2);
            
            int vertexIdx1;
            int vertexIdx2;
            int vertexIdx3;
            
            // Odd Triangles
            if (triangleIdx % 2 == 0)
            {
                vertexIdx1 = triangleIdx / 2 + triangleRowIdx;
                vertexIdx2 = vertexIdx1 + borderWrapping;
                vertexIdx3 = vertexIdx2 + 1;
            }
            // Even Triangles
            else
            {
                vertexIdx1 = (triangleIdx - 1) / 2 + triangleRowIdx;
                vertexIdx2 = vertexIdx1 + 1;
                vertexIdx3 = vertexIdx2 + borderWrapping;
            }

            // ----------------------------------------------- Vertex 1 -------------------------------------
            
            // Coordinates in the grid from [0,0] to [wrapping+2, wrapping+2]
            int xGridOffset1 = (vertexIdx1 % borderWrapping) - borderWrapDiv2;
            int yGridOffset1 = (vertexIdx1 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of verticies
            float x1 = xGridOffset1 * pointPositionOffset;
            float z1 = yGridOffset1 * pointPositionOffset;
            

            // If vertex1 isn't a border point
            int vertIdxModulo = vertexIdx1 % borderWrapping;
            int vertexRowIdx = vertexIdx1 / borderWrapping;

            bool vertex1Inside = vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1;

            // When the vertex is inside, get it from vertices, else gets the height of a point from the neighboring chunk
            Vector3 vertex1 = vertex1Inside ?
                vertices[vertexIdx1 - borderWrapping - (vertexRowIdx * 2 - 1)]
                : new Vector3(x1, GetPointFromOtherChunk(ref neighbours, vertexIdx1, new Vector3(x1, 0, z1), ref usedEffectors, numberOfVerticesBorder), z1);
            
            
            // ----------------------------------------------- Vertex 2 ------------------------------------
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset2 = (vertexIdx2 % borderWrapping) - borderWrapDiv2;
            int yGridOffset2 = (vertexIdx2 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of verticies
            float x2 = xGridOffset2 * pointPositionOffset;
            float z2 = yGridOffset2 * pointPositionOffset;
            
            // If vertex2 isn't a border point
            vertIdxModulo = vertexIdx2 % borderWrapping;
            vertexRowIdx = vertexIdx2 / borderWrapping;

            bool vertex2Inside = vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1;

            // When the vertex is inside, get it from vertices, else gets the height of a point from the neighboring chunk
            Vector3 vertex2 = vertex2Inside ?
                vertices[vertexIdx2 - borderWrapping - (vertexRowIdx * 2 - 1)]
                : new Vector3(x2, GetPointFromOtherChunk(ref neighbours, vertexIdx2, new Vector3(x2, 0, z2), ref usedEffectors, numberOfVerticesBorder), z2);
            
            
            // ----------------------------------------------- Vertex 3 -----------------------------------------------
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset3 = (vertexIdx3 % borderWrapping) - borderWrapDiv2;
            int yGridOffset3 = (vertexIdx3 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of verticies
            float x3 = xGridOffset3 * pointPositionOffset;
            float z3 = yGridOffset3 * pointPositionOffset;
            
            // If vertex3 isn't a border point
            vertIdxModulo = vertexIdx3 % borderWrapping;
            vertexRowIdx = vertexIdx3 / borderWrapping;

            bool vertex3Inside = vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1;
            
            // When the vertex is inside, get it from vertices, else gets the height of a point from the neighboring chunk
            Vector3 vertex3 = vertex3Inside ?
                vertices[vertexIdx3 - borderWrapping - (vertexRowIdx * 2 - 1)]
                : new Vector3(x3, GetPointFromOtherChunk(ref neighbours, vertexIdx3, new Vector3(x3, 0, z3), ref usedEffectors, numberOfVerticesBorder), z3);

            
            // Get normal vector of the triangle made up of the vertices
            Vector3 normalVector = GetUpNormalFromPoints(vertex1, vertex2, vertex3);
            

            // If vertex1 isn't a border point add it to the normals
            if (vertex1Inside)
            {
                vertexRowIdx = vertexIdx1 / borderWrapping;

                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx1 - borderWrapping - (vertexRowIdx * 2 - 1);
                
                vertexNormals[oldIdx] += normalVector;
            }
            
            // If vertex2 isn't a border point add it to the normals
            if (vertex2Inside)
            {
                vertexRowIdx = vertexIdx2 / borderWrapping;

                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx2 - borderWrapping - (vertexRowIdx * 2 - 1);

                vertexNormals[oldIdx] += normalVector;
            }
            
            // If vertex3 isn't a border point add it to the normals
            if (vertex3Inside)
            {
                vertexRowIdx = vertexIdx3 / borderWrapping;

                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx3 - borderWrapping - (vertexRowIdx * 2 - 1);

                vertexNormals[oldIdx] += normalVector;
            }
        }

        // Normalize all normal vectors
        for (int i = 0; i < vertices.Length; i++)
        {
            vertexNormals[i] = vertexNormals[i].normalized;
        }

        _meshFilter.sharedMesh.normals = vertexNormals;
    }

    // Gets a height of a vertex from another chunk, used for the correct normal calculation
    private float GetPointFromOtherChunk(ref Chunk[] neighbours, int vertexIdx, Vector3 vertex, ref List<Chunk> usedEffectors, int numberOfVertices)
    {
        float height = 0;
        Vector3 chunkSizeX = Vector3.right * CHUNK_SIZE;
        Vector3 chunkSizeZ = Vector3.forward * CHUNK_SIZE;
        Vector3 chunkSizeXZ = chunkSizeX + chunkSizeZ;

        // Bottom Left corner
        if (vertexIdx == 0 && neighbours[5] != null)
        {
            height = neighbours[5].GetHeightOfPoint(vertex + chunkSizeXZ, ref usedEffectors);
        }
        
        // Bottom Right corner
        else if (vertexIdx == _wrappingOfPoints + 1 && neighbours[7] != null)
        {
            height = neighbours[7].GetHeightOfPoint(vertex + chunkSizeZ - chunkSizeX, ref usedEffectors);
        }
        
        // Top left corner
        else if (vertexIdx == (numberOfVertices - 1) - (_wrappingOfPoints + 1) && neighbours[0] != null)
        {
            height = neighbours[0].GetHeightOfPoint(vertex - chunkSizeZ + chunkSizeX, ref usedEffectors);
        }
        
        // Top Right corner
        else if (vertexIdx == numberOfVertices - 1 && neighbours[2] != null)
        {
            height = neighbours[2].GetHeightOfPoint(vertex - chunkSizeXZ, ref usedEffectors);
        }
        
        // Bottom
        else if (vertexIdx < _wrappingOfPoints + 1 && neighbours[6] != null)
        {
            height = neighbours[6].GetHeightOfPoint(new Vector3(vertex.x, 0, vertex.z + CHUNK_SIZE), ref usedEffectors);
        }
        
        // Top
        else if (vertexIdx >= numberOfVertices - (_wrappingOfPoints + 2) && neighbours[1] != null)
        {
            height = neighbours[1].GetHeightOfPoint(new Vector3(vertex.x, 0, vertex.z - CHUNK_SIZE), ref usedEffectors);
        }
        
        // Right
        else if (vertexIdx % (_wrappingOfPoints + 2) == _wrappingOfPoints + 1 && neighbours[4] != null)
        {
            height = neighbours[4].GetHeightOfPoint(new Vector3(vertex.x - CHUNK_SIZE, 0, vertex.z), ref usedEffectors);
        }
        
        // left
        else if (vertexIdx % (_wrappingOfPoints + 2) == 0 && neighbours[3] != null)
        {
            height = neighbours[3].GetHeightOfPoint(new Vector3((vertex.x + CHUNK_SIZE), 0, vertex.z), ref usedEffectors);
        }

        // edge case
        if (height == 0)
        {
            height = GetHeightOfPoint(vertex, ref usedEffectors);
        }

        return height;
    }

    // Calculates the normal vector of a triangle made up from 3 vertices, considers only up facing normal vectors
    private Vector3 GetUpNormalFromPoints(Vector3 point1, Vector3 point2, Vector3 point3)
    {
        Vector3 fromOneToTwo = point2 - point1;
        Vector3 fromOneToThree = point3 - point1;

        Vector3 normalVector = Vector3.Cross(fromOneToTwo, fromOneToThree).normalized;
        
        // Only consider upwards facing normals
        return normalVector.y >= 0 ? normalVector : -normalVector;
    }
    
    // Gets the height of a point in another chunk to connect the edges of chunks
    private float ConnectEdges(int vertexIdx, Vector3 point, float height, ref Chunk[] neighbours, ref List<Chunk> usedEffectors)
    {
        int numberOfVertices = _wrappingOfPoints * _wrappingOfPoints;

        // Top Left corner
        if (vertexIdx == 0)
        {
            if (neighbours[5] != null)
            {
                height = Mathf.Max(neighbours[5].GetHeightOfPoint(-point, ref usedEffectors), height);
            }
            if (neighbours[3] != null)
            {
                height = Mathf.Max(neighbours[3].GetHeightOfPoint(new Vector3(-point.x, 0, point.z), ref usedEffectors), height);
            }
            if (neighbours[6] != null)
            {
                height = Mathf.Max(neighbours[6].GetHeightOfPoint(new Vector3(point.x, 0, -point.z), ref usedEffectors), height);
            }
        }
        
        // Top Right corner
        else if (vertexIdx == _wrappingOfPoints - 1)
        {
            if (neighbours[7] != null)
            {
                height = Mathf.Max(neighbours[7].GetHeightOfPoint(-point, ref usedEffectors), height);
            }
            if (neighbours[4] != null)
            {
                height = Mathf.Max(neighbours[4].GetHeightOfPoint(new Vector3(-point.x, 0, point.z), ref usedEffectors), height);
            }
            if (neighbours[6] != null)
            {
                height = Mathf.Max(neighbours[6].GetHeightOfPoint(new Vector3(point.x, 0, -point.z), ref usedEffectors), height);
            }
        }
        
        // Bottom left corner
        else if (vertexIdx == (numberOfVertices - 1) - (_wrappingOfPoints - 1))
        {
            if (neighbours[0] != null)
            {
                height = Mathf.Max(neighbours[0].GetHeightOfPoint(-point, ref usedEffectors), height);
            }
            if (neighbours[3] != null)
            {
                height = Mathf.Max(neighbours[3].GetHeightOfPoint(new Vector3(-point.x, 0, point.z), ref usedEffectors), height);
            }
            if (neighbours[1] != null)
            {
                height = Mathf.Max(neighbours[1].GetHeightOfPoint(new Vector3(point.x, 0, -point.z), ref usedEffectors), height);
            }
        }
        
        // Bottom Right corner
        else if (vertexIdx == numberOfVertices - 1)
        {
            // Bottom right
            if (neighbours[2] != null)
            {
                height = Mathf.Max(neighbours[2].GetHeightOfPoint(-point, ref usedEffectors), height);
            }
            if (neighbours[4] != null)
            {
                height = Mathf.Max(neighbours[4].GetHeightOfPoint(new Vector3(-point.x, 0, point.z), ref usedEffectors), height);
            }
            if (neighbours[1] != null)
            {
                height = Mathf.Max(neighbours[1].GetHeightOfPoint(new Vector3(point.x, 0, -point.z), ref usedEffectors), height);
            }
        }
        
        // Bottom
        else if (vertexIdx < _wrappingOfPoints)
        {
            if (neighbours[6] != null)
            {
                height = Mathf.Max(neighbours[6].GetHeightOfPoint(new Vector3(point.x, 0, -point.z), ref usedEffectors), height);
            }
        }
        
        // Top
        else if (vertexIdx >= numberOfVertices - _wrappingOfPoints)
        {
            if (neighbours[1] != null)
            {
                height = Mathf.Max(neighbours[1].GetHeightOfPoint(new Vector3(point.x, 0, -point.z), ref usedEffectors), height);
            }
        }
        
        // Right
        else if (vertexIdx % _wrappingOfPoints == _wrappingOfPoints - 1)
        {
            if (neighbours[4] != null)
            {
                height = Mathf.Max(neighbours[4].GetHeightOfPoint(new Vector3(-point.x, 0, point.z), ref usedEffectors), height);
            }
        }
        
        // Right
        else if (vertexIdx % _wrappingOfPoints == 0)
        {
            if (neighbours[3] != null)
            {
                height = Mathf.Max(neighbours[3].GetHeightOfPoint(new Vector3(-point.x, 0, point.z), ref usedEffectors), height);
            }
        }

        return height;
    }

    // New chunk generation in all up direction
    public Chunk GenerateTopChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x, _indexes.y + 1));
    }
    
    // New chunk generation in all right direction
    public Chunk GenerateRightChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x + 1, _indexes.y));
    }
    
    // New chunk generation in all bottom direction
    public Chunk GenerateBottomChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x, _indexes.y - 1));
    }
    
    // New chunk generation in all left direction
    public Chunk GenerateLeftChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x - 1, _indexes.y));
    }
    
    //-------------------------------------------Chunk-Editing----------------------------------------------------------
    // Updates the chunk and recalculates height
    public void UpdateParameters(ChunkManager chunkmanager, float offsetFromSeed, Vector3[] editOfOctaves, int numberOfOctaves,
        float amplitude, float lacunarity, float persistence, Material material, bool isHilly)
    {
        _offsetFromSeed = offsetFromSeed;
        _editOfOctaves = editOfOctaves;
        _numberOfOctaves = numberOfOctaves;
        _amplitude = amplitude;
        _lacunarity = lacunarity;
        _persistence = persistence;
        _chunkManager = chunkmanager;
        _isHilly = isHilly;

        CheckReferences();
        _meshRenderer.material = material;

        // Posts an update for the next update frame
        _updateHeightNextFrame = true;
    }
    
    // Ensures that the chunks height will be updated during the next frame
    public void NotifyChunkToUpdateOnce()
    {
        _updateHeightNextFrame = true;
    }
    
    // Updates height of vertices based on chunks that change height (effectors) and texture features
    private void UpdateHeight()
    {
        CheckReferences();
        
        // Goes through the effectors and updates only the ones that reach this chunk
        List<Chunk> usedEffectors = GetUsedEffectors();

        // Needed to reset effect of height effectors
        if (usedEffectors.Count == 0)
        {
            if (!_updateHeightNextFrame)
            {
                return;
            }
        }

        // Get the neighbouring chunks
        Chunk[] neighbours = new Chunk[8];
        neighbours[0] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y + 1);       // Top left
        neighbours[1] = _chunkManager.GetChunk(_indexes.x, _indexes.y + 1);             // Top
        neighbours[2] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y + 1);       // Top right
        neighbours[3] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y);             // Middle left
        neighbours[4] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y);             // Middle right
        neighbours[5] = _chunkManager.GetChunk(_indexes.x - 1, _indexes.y - 1);       // Bottom left
        neighbours[6] = _chunkManager.GetChunk(_indexes.x, _indexes.y - 1);             // Bottom
        neighbours[7] = _chunkManager.GetChunk(_indexes.x + 1, _indexes.y - 1);       // Bottom right
        
        Vector3[] vertices = _meshFilter.sharedMesh.vertices;

        int numberOfVertices = vertices.Length;
        for (int vertexIdx = 0; vertexIdx < numberOfVertices; vertexIdx++)
        {
            Vector3 point = vertices[vertexIdx];

            // Perlin noise and height effectors calculation
            float height = GetHeightOfPoint(point, ref usedEffectors);

            height = ConnectEdges(vertexIdx, point, height, ref neighbours, ref usedEffectors);
            
            point.y = height;
            if (float.IsInfinity(point.y)) point.y = 0;
            vertices[vertexIdx] = point;
        }
        
        _updateHeightNextFrame = false;

        _meshFilter.sharedMesh.vertices = vertices;
        RecalculateNormals();
        _meshFilter.sharedMesh.RecalculateBounds();
        _meshCollider.sharedMesh = _meshFilter.sharedMesh;
    }

    // Get a list of height effectors based on their effect and distance to them
    private List<Chunk> GetUsedEffectors()
    {
        if (_chunkManager == null) _chunkManager = GetComponentInParent<ChunkManager>();
        ICollection allEffectors = _chunkManager.GetHeightChangingChunks();
        
        List<Chunk> usedEffectors = new List<Chunk>();
        foreach (Chunk effector in allEffectors)
        {
            if (effector == null) continue;
            if (Vector3.Distance(effector.transform.position, transform.position) < effector.heightRange + CHUNK_SIZE * 0.8f)
            {
                usedEffectors.Add(effector);
            }
        }

        return usedEffectors;
    }

    // Changes the number of vertices per line in a chunk and recalculates the chunk
    public void ChangeWrapping(int newWrapping)
    {
        if (newWrapping % 2 != 1)
        {
            newWrapping += 1;
        }
        _wrappingOfPoints = newWrapping;
        CalculateMesh();
    }

    // Calculates height of point from X and Z (uses Perlin noise, Height effector chunks, and all the textures for features)
    public float GetHeightOfPoint(Vector3 point, ref List<Chunk> usedEffectors)
    {
        Vector3 chunkPosition = transform.position;
        point.y = 0;
        
        // Perlin noise coordinates
        float perlinX = point.x + chunkPosition.x + _offsetFromSeed;
        float perlinZ = point.z + chunkPosition.z + _offsetFromSeed;
        float height = 0;

        float tempFrequency = 1;
        float tempAmplitude = _amplitude;
        
        // Recursive perlin noise detail
        for (int i = 0; i < _numberOfOctaves; i++)
        {
            Vector3 editOfOctave = new Vector3(0, 0, DEFAULT_SCALE);
            if (i < _editOfOctaves.Length)
            {
                editOfOctave = _editOfOctaves[i];
                if (editOfOctave.z == 0) editOfOctave.z = DEFAULT_SCALE;
            }
            
            float tempX = perlinX / editOfOctave.z * tempFrequency;
            float tempZ = perlinZ / editOfOctave.z * tempFrequency;

            // Change height calculation based on isHilly
            if (_isHilly) height += (Mathf.PerlinNoise(tempX + editOfOctave.x, tempZ + editOfOctave.y) * 2 - 1) * tempAmplitude / 2;
            else height += MakeCubic(Mathf.PerlinNoise(tempX + editOfOctave.x, tempZ + editOfOctave.y) * 2 - 1) * tempAmplitude / 5;

            tempAmplitude *= _persistence;
            tempFrequency *= _lacunarity;
        }
        
        Vector2 chunkPositionXZ = new Vector2(chunkPosition.x, chunkPosition.z);
        
        // Point coordinates inside the chunk
        Vector2 pointXZ = new Vector2(point.x, point.z);
        
        
        // ----------------Height offset from effectors
        float heightFromEffectors = 0f;
        foreach (Chunk effector in usedEffectors)
        {
            Vector3 effectorPosition = effector.transform.position;
            Vector2 epicenter = new Vector2(effectorPosition.x, effectorPosition.z);
                
            // Too far from epicenter
            float distanceFromEpicenter = Mathf.Abs((epicenter - (pointXZ + chunkPositionXZ)).magnitude);
            if (distanceFromEpicenter > effector.heightRange) continue;

            // Range is zero
            if (effector.heightRange == 0) continue;

            float interpolationCoefficient = 1 - distanceFromEpicenter / effector.heightRange;

            heightFromEffectors += MakeCubic(interpolationCoefficient) * effector.heightOffset;
        }
        
        // --------------Texture coordinates calcualtion
        Vector2 textureCoordsFloat = pointXZ;
        textureCoordsFloat /= (CHUNK_SIZE / 2);
        textureCoordsFloat += Vector2.one;
        textureCoordsFloat /= 2;
        
        textureCoordsFloat.x = Mathf.Clamp(textureCoordsFloat.x, 0, 1);
        textureCoordsFloat.y = Mathf.Clamp(textureCoordsFloat.y, 0, 1);
        Vector2 textureCoordsFloat128 = textureCoordsFloat * TEX128_RESOLUTION;
        int texX128 = Mathf.Clamp(Mathf.RoundToInt(textureCoordsFloat128.x), 0, TEX128_RESOLUTION-1);
        int texY128 = Mathf.Clamp(Mathf.RoundToInt(textureCoordsFloat128.y), 0, TEX128_RESOLUTION-1);
        
        Vector2 textureCoordsFloat256 = textureCoordsFloat * TEX256_RESOLUTION;
        int texX256 = Mathf.Clamp((int)textureCoordsFloat256.x, 0, TEX256_RESOLUTION-1);
        int texY256 = Mathf.Clamp((int)textureCoordsFloat256.y, 0, TEX256_RESOLUTION-1);
        
        // ----------------Height offset path texture
        float heightFromPath = 0;
        if (_pathTexture != null)
        {
            Color pixelPath = _pathTexture.GetPixel(texX128, texY128);
            float tempHeightFromPath = pixelPath.a * _pathBalance * (pixelPath.r - 0.5f) + _pathYOffset * pixelPath.a;
            heightFromPath = tempHeightFromPath * 2 - 1;
        }

        // ----------------Height offset road texture
        float heightFromRoad = 0;
        if (_roadTexture != null)
        {
            // Best of 9 pixels to make transitions more smooth
            Color pixelRoadTopLeft = _roadTexture.GetPixel(Mathf.Clamp(texX256 - 1, 0, ROAD_RESOLUTION - 1), Mathf.Clamp(texY256 + 1, 0, ROAD_RESOLUTION - 1));
            Color pixelRoadTop = _roadTexture.GetPixel(texX256, Mathf.Clamp(texY256 + 1, 0, ROAD_RESOLUTION - 1));
            Color pixelRoadTopRight = _roadTexture.GetPixel(Mathf.Clamp(texX256 + 1, 0, ROAD_RESOLUTION - 1), Mathf.Clamp(texY256 + 1, 0, ROAD_RESOLUTION - 1));
            Color pixelRoadLeft = _roadTexture.GetPixel(Mathf.Clamp(texX256 - 1, 0, ROAD_RESOLUTION - 1), texY256);
            Color pixelRoadCenter = _roadTexture.GetPixel(texX256, texY256);
            Color pixelRoadRight = _roadTexture.GetPixel(Mathf.Clamp(texX256 + 1, 0, ROAD_RESOLUTION - 1), texY256);
            Color pixelRoadBottomLeft = _roadTexture.GetPixel(Mathf.Clamp(texX256 - 1, 0, ROAD_RESOLUTION - 1), Mathf.Clamp(texY256 - 1, 0, ROAD_RESOLUTION - 1));
            Color pixelRoadBottom = _roadTexture.GetPixel(texX256, Mathf.Clamp(texY256 - 1, 0, ROAD_RESOLUTION - 1));
            Color pixelRoadBottomRight = _roadTexture.GetPixel(Mathf.Clamp(texX256 + 1, 0, ROAD_RESOLUTION - 1), Mathf.Clamp(texY256 - 1, 0, ROAD_RESOLUTION - 1));

            float pixelAlpha = pixelRoadTopLeft.a + pixelRoadTop.a + pixelRoadTopRight.a + pixelRoadLeft.a + pixelRoadCenter.a 
                               + pixelRoadRight.a + pixelRoadBottomLeft.a + pixelRoadBottom.a + pixelRoadBottomRight.a;

            float pixelGreen = pixelRoadTopLeft.g + pixelRoadTop.g + pixelRoadTopRight.g + pixelRoadLeft.g + pixelRoadCenter.g 
                               + pixelRoadRight.g + pixelRoadBottomLeft.g + pixelRoadBottom.g + pixelRoadBottomRight.g;

            pixelAlpha /= 9;
            pixelGreen /= 9;

            float lerp = EaseInOutCubic(pixelAlpha);
            heightFromRoad = (pixelGreen - 0.5f) * _roadMaxYOffset * Mathf.Clamp(lerp, 0, 1);
        }
        
        // ----------------Height offset erosion texture
        float heightFromErosion = 0;
        if (_erosionTexture != null)
        {
            // Best of 9 pixels to make transitions more smooth
            Color pixelErosTopLeft = _erosionTexture.GetPixel(Mathf.Clamp(texX128 - 1, 0, EROS_RESOLUTION - 1), Mathf.Clamp(texY128 + 1, 0, EROS_RESOLUTION - 1));
            Color pixelErosTop = _erosionTexture.GetPixel(texX256, Mathf.Clamp(texY128 + 1, 0, EROS_RESOLUTION - 1));
            Color pixelErosTopRight = _erosionTexture.GetPixel(Mathf.Clamp(texX128 + 1, 0, EROS_RESOLUTION - 1), Mathf.Clamp(texY128 + 1, 0, EROS_RESOLUTION - 1));
            Color pixelErosLeft = _erosionTexture.GetPixel(Mathf.Clamp(texX128 - 1, 0, EROS_RESOLUTION - 1), texY128);
            Color pixelErosCenter = _erosionTexture.GetPixel(texX128, texY128);
            Color pixelErosRight = _erosionTexture.GetPixel(Mathf.Clamp(texX128 + 1, 0, EROS_RESOLUTION - 1), texY128);
            Color pixelErosBottomLeft = _erosionTexture.GetPixel(Mathf.Clamp(texX128 - 1, 0, EROS_RESOLUTION - 1), Mathf.Clamp(texY128 - 1, 0, EROS_RESOLUTION - 1));
            Color pixelErosBottom = _erosionTexture.GetPixel(texX128, Mathf.Clamp(texY128 - 1, 0, EROS_RESOLUTION - 1));
            Color pixelErosBottomRight = _erosionTexture.GetPixel(Mathf.Clamp(texX128 + 1, 0, EROS_RESOLUTION - 1), Mathf.Clamp(texY128 - 1, 0, EROS_RESOLUTION - 1));

            float pixelAlpha = pixelErosTopLeft.a + pixelErosTop.a + pixelErosTopRight.a + pixelErosLeft.a + pixelErosCenter.a 
                               + pixelErosRight.a + pixelErosBottomLeft.a + pixelErosBottom.a + pixelErosBottomRight.a;

            float pixelColor = pixelErosTopLeft.g + pixelErosTop.g + pixelErosTopRight.g + pixelErosLeft.g + pixelErosCenter.g 
                               + pixelErosRight.g + pixelErosBottomLeft.g + pixelErosBottom.g + pixelErosBottomRight.g;

            pixelAlpha /= 9;
            pixelColor /= 9;
            
            float tempHeightFromErosion = ((pixelColor - 0.5f)) * _amplitude / 200 * _erosionStrength * pixelAlpha;
            heightFromErosion = tempHeightFromErosion * 2 - 1;
        }
        
        // ----------------Height offset water texture
        float heightFromWater = 0;
        if (_waterTexture != null)
        {
            Color pixelWater = _waterTexture.GetPixel(texX128, texY128);
            float lerp = MakeCubic(pixelWater.a);
            heightFromWater = (pixelWater.r - 0.5f) * _waterMaxYOffset * Mathf.Clamp(lerp, 0, 1);
        }

        return height + heightFromEffectors + heightFromPath + heightFromRoad + heightFromErosion + heightFromWater;
    }
    
    // Bicubic interpolation
    private float MakeCubic(float x)
    {
        return -2 * (x*x*x) + 3 * (x*x);
    }

    // Taken from https://easings.net/en#easeInOutCubic
    private float EaseInOutCubic(float x){
        return x < 0.5 ? 4 * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 3) / 2;
    }

    // ----------------------------------------Path-Utils---------------------------------------------------------------
    public Texture2D GetPathTexture()
    {
        return _pathTexture;
    }

    // Set the path parameters and post an height update
    public void SetPathCarveDepthAndOffset(float carveDepth, float yOffset)
    {
        _pathBalance = carveDepth;
        _pathYOffset = yOffset;
        _updateHeightNextFrame = true;
    }
    
    // Sets the path texture, save variable controls if the new path texture will be saved
    public void SetPathTexture(Texture2D newPathTexture, bool save)
    {
        _pathTexture = newPathTexture;
        
        if (newPathTexture == null)
        {
            _pathTexture = GetPathEmptyTexture();   
        }  
        
        _pathTexture.filterMode = FilterMode.Bilinear;
        _pathTexture.wrapMode = TextureWrapMode.Clamp;
        _pathTexture.name = name + "PathTex";
        _pathTexture.Apply();
        
        if (save) SaveTexture(_pathTexture, PATH_FOLDER);

        if (_materialPropertyBlock == null)
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
        }

        if (_meshRenderer == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
        }
        
        int idxPath = Shader.PropertyToID("_PathTexture");
        int idxRoad = Shader.PropertyToID("_RoadTexture");
        int idxWater = Shader.PropertyToID("_UnderWaterTexture");
        if (_materialPropertyBlock == null) _materialPropertyBlock = new MaterialPropertyBlock();
        if (_waterTexture != null)_materialPropertyBlock.SetTexture(idxWater, _waterTexture);
        if (_roadTexture != null)_materialPropertyBlock.SetTexture(idxRoad, _roadTexture);
        _materialPropertyBlock.SetTexture(idxPath, _pathTexture);
        _meshRenderer.SetPropertyBlock(_materialPropertyBlock);
        _updateHeightNextFrame = true;
    }
    
    // ----------------------------------------Road-Utils---------------------------------------------------------------
    public Texture2D GetRoadTexture()
    {
        return _roadTexture;
    }
    
    // Set the road parameters and post an height update
    public void SetRoadParameters(float yOffset)
    {
        _roadMaxYOffset = yOffset;
        _updateHeightNextFrame = true;
        if (yOffset == 0) UpdateHeight();   // Special case during for road generation
    }
    
    // Sets the road texture, save variable controls if the new road texture will be saved
    public void SetRoadTexture(Texture2D newRoadTexture, bool save)
    {
        _roadTexture = newRoadTexture;
        
        if (newRoadTexture == null)
        {
            _roadTexture = GetEmptyRoadTexture();   
        }  
        
        _roadTexture.filterMode = FilterMode.Bilinear;
        _roadTexture.wrapMode = TextureWrapMode.Clamp;
        _roadTexture.name = name + "RoadTex";
        _roadTexture.Apply();
        
        if (save) SaveTexture(_roadTexture, ROAD_FOLDER);

        if (_meshRenderer == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
        }
        
        if (_materialPropertyBlock == null)
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
        }

        int idxPath = Shader.PropertyToID("_PathTexture");
        int idxRoad = Shader.PropertyToID("_RoadTexture");
        int idxWater = Shader.PropertyToID("_UnderWaterTexture");
        if (_materialPropertyBlock == null) _materialPropertyBlock = new MaterialPropertyBlock();
        if (_pathTexture != null) _materialPropertyBlock.SetTexture(idxPath, _pathTexture);
        _materialPropertyBlock.SetTexture(idxRoad, _roadTexture);
        if (_waterTexture != null) _materialPropertyBlock.SetTexture(idxWater, _waterTexture);
        _meshRenderer.SetPropertyBlock(_materialPropertyBlock);
        _updateHeightNextFrame = true;
    }
    
    // ----------------------------------------Erosion-Utils------------------------------------------------------------
    public Texture2D GetErosionTexture()
    {
        return _erosionTexture;
    }

    // Set the erosion parameters and post an height update
    public void SetErosionStrength(float newErosionStrength)
    {
        _erosionStrength = newErosionStrength;
        _updateHeightNextFrame = true;
    }

    // Sets the erosion texture, save variable controls if the new erosion texture will be saved
    public void SetErosionTexture(Texture2D newErosionTexture, bool save)
    {
        _erosionTexture = newErosionTexture;
        if (_erosionTexture == null)
        {
            _erosionTexture = GetEmptyErosionTexture();
        }
        _erosionTexture.filterMode = FilterMode.Bilinear;
        _erosionTexture.wrapMode = TextureWrapMode.Clamp;
        _erosionTexture.name = name + "ErosTex";
        _erosionTexture.Apply();
        
        if (save) SaveTexture(_erosionTexture, EROS_FOLDER);
        
        _updateHeightNextFrame = true;
    }
    
    // ----------------------------------------Water-Utils--------------------------------------------------------------
    public Texture2D GetWaterTexture()
    {
        return _waterTexture;
    }
    
    // Set the water parameters and post an height update
    public void SetWaterParameters(float newWaterMaxYOffset)
    {
        if (_waterMaxYOffset == newWaterMaxYOffset) return;
        
        _waterMaxYOffset = newWaterMaxYOffset;
        _updateHeightNextFrame = true;
        if (newWaterMaxYOffset == 0){
            UpdateHeight();   // Special case for water generation
        }
    }
    
    // Sets the water texture, save variable controls if the new water texture will be saved
    public void SetWaterTexture(Texture2D newWaterTexture, bool save)
    {
        _waterTexture = newWaterTexture;
        
        if (newWaterTexture == null)
        {
            _waterTexture = GetEmptyWaterTexture();   
        }  
        
        _waterTexture.filterMode = FilterMode.Bilinear;
        _waterTexture.wrapMode = TextureWrapMode.Clamp;
        _waterTexture.name = name + "WaterTex";
        _waterTexture.Apply();
        
        if (save) SaveTexture(_waterTexture, WATER_FOLDER);

        if (_meshRenderer == null)
        {
            _meshRenderer = GetComponent<MeshRenderer>();
        }
        // tODO sand underwater
        
        int idxPath = Shader.PropertyToID("_PathTexture");
        int idxRoad = Shader.PropertyToID("_RoadTexture");
        int idxWater = Shader.PropertyToID("_UnderWaterTexture");
        if (_materialPropertyBlock == null) _materialPropertyBlock = new MaterialPropertyBlock();
        if (_pathTexture != null) _materialPropertyBlock.SetTexture(idxPath, _pathTexture);
        if (_roadTexture != null) _materialPropertyBlock.SetTexture(idxRoad, _roadTexture);
        _materialPropertyBlock.SetTexture(idxWater, _waterTexture);
        _meshRenderer.SetPropertyBlock(_materialPropertyBlock);
        _updateHeightNextFrame = true;
    }

    // -------------------------------------Texture-Saving-and-Loading-Utils--------------------------------------------
    // Saves a given texture to a given folder
    private void SaveTexture(Texture2D texture, string folder)
    {
        byte[] bytes = texture.EncodeToPNG();

        string pathWithFolder = Application.dataPath + "/ChunkTextures/" + SceneManager.GetActiveScene().name + "/" + folder;
        if (!System.IO.Directory.Exists(pathWithFolder)) System.IO.Directory.CreateDirectory(pathWithFolder);
        System.IO.File.WriteAllBytes( pathWithFolder + texture.name + ".png", bytes);
    }
    
    // Loads a texture type 
    private Texture2D LoadTexture(TextureType type)
    {
        string textureFile = "";
        string textureFolder = "";
        int textureResolution = TEX128_RESOLUTION;
        if (type == TextureType.Path)
        {
            textureFile =  name + "PathTex";
            textureFolder = PATH_FOLDER;
            textureResolution = PATH_RESOLUTION;
        }
        else if (type == TextureType.Erosion)
        {
            textureFile = name + "ErosTex";
            textureFolder = EROS_FOLDER;
            textureResolution = EROS_RESOLUTION;
        }
        else if (type == TextureType.Road)
        {
            textureFile = name + "RoadTex";
            textureFolder = ROAD_FOLDER;
            textureResolution = ROAD_RESOLUTION;
        }
        else if (type == TextureType.Water)
        {
            textureFile = name + "WaterTex";
            textureFolder = WATER_FOLDER;
            textureResolution = WATER_RESOLUTION;
        }
        
        byte[] bytes;

        // Try to load a texture if it exists
        try
        {
            string pathWithFolder = Application.dataPath + "/ChunkTextures/" + SceneManager.GetActiveScene().name + "/" + textureFolder;
            if (!System.IO.Directory.Exists(pathWithFolder)) System.IO.Directory.CreateDirectory(pathWithFolder);
            bytes = System.IO.File.ReadAllBytes(pathWithFolder + textureFile + ".png");
        }
        catch 
        {
            return null;
        }
        
        // RGBAFloat format for float precision
        Texture2D tex = new Texture2D(textureResolution,textureResolution, TextureFormat.RGBAFloat, false, true);
        tex.LoadImage(bytes);
        tex.Apply();
        
        return tex;
    }

    // Creates a 1 by 1 placeholder texture to reset path texture
    private Texture2D GetPathEmptyTexture()
    {
        Texture2D newPathTex = new Texture2D(1,1, TextureFormat.RGBAFloat, true);

        Color pixel = Color.black;
        pixel.a = 0;

        newPathTex.filterMode = FilterMode.Bilinear;
        newPathTex.wrapMode = TextureWrapMode.Clamp;
        newPathTex.SetPixel(0,0, pixel);
        newPathTex.name = name + "PathTex";
        newPathTex.Apply();

        return newPathTex;
    }
    
    // Creates a 1 by 1 placeholder texture to reset road texture
    private Texture2D GetEmptyRoadTexture()
    {
        Texture2D newRoadTex = new Texture2D(1,1, TextureFormat.RGBAFloat, true);

        Color pixel = new Color(0,0.5f,0,0);

        newRoadTex.filterMode = FilterMode.Bilinear;
        newRoadTex.wrapMode = TextureWrapMode.Clamp;
        newRoadTex.SetPixel(0,0, pixel);
        newRoadTex.name = name + "RoadTex";
        newRoadTex.Apply();

        return newRoadTex;
    }

    // Creates a 1 by 1 placeholder texture to reset erosion texture
    private Texture2D GetEmptyErosionTexture()
    {
        Texture2D tex = new Texture2D(1,1, TextureFormat.RGBAFloat, true);
        Color pixel = Color.gray;
        pixel.a = 0;
        
        tex.filterMode = FilterMode.Bilinear;
        tex.wrapMode = TextureWrapMode.Clamp;
        tex.SetPixel(0,0, pixel);
        tex.name = name + "ErosTex";
        tex.Apply();

        return tex;
    }
    
    // Creates a 1 by 1 placeholder texture to reset water texture
    private Texture2D GetEmptyWaterTexture()
    {
        Texture2D newWaterTex = new Texture2D(1,1, TextureFormat.RGBAFloat, true);

        Color pixel = new Color(0,0,0,0);

        newWaterTex.filterMode = FilterMode.Bilinear;
        newWaterTex.wrapMode = TextureWrapMode.Clamp;
        newWaterTex.SetPixel(0,0, pixel);
        newWaterTex.name = name + "WaterTex";
        newWaterTex.Apply();

        return newWaterTex;
    }
    
    // -----------------------------------------------Chunk-Utils-------------------------------------------------------
    public float GetChunkSize()
    {
        return CHUNK_SIZE;
    }

    // Returns the indices of the chunk
    public Vector2Int GetIndices()
    {
        return _indexes;
    }

    // When opening the project or entering play mode references are not kept and need to be reassigned
    private void CheckReferences()
    {
        if (_meshRenderer == null)
        { 
            _meshRenderer = GetComponent<MeshRenderer>();
        }
        
        if (_meshFilter == null)
        {
            _meshFilter = GetComponent<MeshFilter>();
        }

        if (_meshCollider == null)
        {
            _meshCollider = GetComponent<MeshCollider>();
        }
        
        if (_chunkManager == null)
        {
            _chunkManager = GetComponentInParent<ChunkManager>();
            RecalculateIndexes();
        }

        if (_wrappingOfPoints == 0)
        {
            _wrappingOfPoints = _chunkManager.GetWrapping();
        }
        
        if (_materialPropertyBlock == null)
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
        }
        
        if (_pathTexture == null)
        {
            Texture2D newPathTex = LoadTexture(TextureType.Path);
            NotifyChunkToUpdateOnce();
            SetPathTexture(newPathTex, false);
        }
        
        if (_roadTexture == null)
        {
            Texture2D newRoadTex = LoadTexture(TextureType.Road);
            NotifyChunkToUpdateOnce();
            SetRoadTexture(newRoadTex, false);
        }

        if (_erosionTexture == null)
        {
            Texture2D newErosTex = LoadTexture(TextureType.Erosion);
            NotifyChunkToUpdateOnce();
            SetErosionTexture(newErosTex, false);
        }
        
        if (_waterTexture == null)
        {
            Texture2D newWaterTex = LoadTexture(TextureType.Water);
            NotifyChunkToUpdateOnce();
            SetWaterTexture(newWaterTex, false);
        }
    }
}
