using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

[ExecuteInEditMode]
public class ChunkManager : MonoBehaviour
{
    [SerializeField] public float offsetFromSeed;
    [SerializeField] [Range(1, 15)] public int numberOfOctaves;
    [SerializeField] [Range(1f, 100f)]public float amplitude;
    [SerializeField] [Range(1f, 4f)] public float lacunarity = 1.8f;
    [SerializeField] [Range(0.3f, 0.8f)] public float persistence = 0.4f;
    [SerializeField] public Material material;
    [SerializeField] private int wrapping;
    [SerializeField] 
    [Tooltip("X and Y are offsets, Z is scale of octave.")]
    private Vector3[] editOfOctaves;
    
    
    private float _chunkSize;
    private Hashtable _chunksTable = new Hashtable();
    private Hashtable _heightChangingChunks = new Hashtable();
    
    // In case of unity reload we need to put chunks back into tables
    private void Update()
    {
        if (_chunksTable.Count == 0)
        {
            foreach (Chunk chunk in transform.GetComponentsInChildren<Chunk>())
            {
                _chunksTable.Add(chunk.name, chunk);
                if (_chunkSize == 0)
                {
                    _chunkSize = chunk.GetChunkSize();
                }
                if (chunk.enableTerrainChange)
                {
                    _heightChangingChunks.Add(chunk.name, chunk);
                }
            }
        }
    }

    // Update the parameters during initialization
    public void InitializeParameters(float newOffsetFromSeed, float newChunkSize, float newPerlinScale, int newNumberOfOctaves,
        float newAmplitude, float newLacunarity,float newPersistence, int newWrapping, Material newMaterial)
    {
        offsetFromSeed = newOffsetFromSeed;
        _chunkSize = newChunkSize;
        numberOfOctaves = newNumberOfOctaves;
        amplitude = newAmplitude;
        lacunarity = newLacunarity;
        persistence = newPersistence;
        material = newMaterial;
        _lastWrapping = newWrapping;
        wrapping = newWrapping;
        editOfOctaves = new Vector3[numberOfOctaves];
        for (int i = 0; i < numberOfOctaves; i++)
        {
            editOfOctaves[i] = new Vector3(0,0, newPerlinScale);
        }
    }
    
    public static string CreateChunkName(int x, int y)
    {
        return "chunk[" + x + "," + y + "]";
    }

    // Factory method to create a chunk and add it to chunks table
    public Chunk CreateChunk(Vector2Int idxs)
    {
        string chunkName = CreateChunkName(idxs.x, idxs.y);
        if (_chunksTable.Contains(chunkName)) return null;

        GameObject chunk = new GameObject();
        chunk.transform.parent = transform;
        chunk.name = chunkName;
        Chunk chunkComponent = chunk.AddComponent<Chunk>();
        chunkComponent.Initialize(this, offsetFromSeed, idxs, _chunkSize, editOfOctaves, numberOfOctaves, amplitude, lacunarity, persistence, wrapping, material);
        _chunksTable.Add(chunkName, chunkComponent);

        return chunkComponent;
    }
/*
    // Retrieves the chunk component of given chunk from hashtable
    public Chunk GetChunk(int x, int y)
    {
        string chunkName = CreateChunkName(x, y);
        Debug.Log("getting " + chunkName);
        if (_chunksTable.Contains(chunkName))
        {
            Debug.Log("GOT IT");
            return (Chunk) _chunksTable[chunkName];
        }

        return null;
    }
*/
   

    private void RecalculateNewWrapping()
    {
        int tempWrap = wrapping;

        if (wrapping % 2 != 1)
        {
            wrapping += 1;
            tempWrap += 1;
        }

        foreach (Chunk chunk in _chunksTable.Values)
        {
            chunk.ChangeWrapping(tempWrap);
        }
    }
    
    // Updates height of verticies in chunks based on _heightChanging chunks and chunksUpdatedPerFrame
    private IDictionaryEnumerator _currentChunkEnumerator;
    
    // Goes through all the chunks a notifies them to update their height
    private void UpdateHeightOfChunks()
    {
        ICollection chunks = _chunksTable.Values;
        // Maybe do only some per frame
        foreach (Chunk chunk in chunks)
        {
            if(chunk == null) _chunksTable.Remove(chunk);
            chunk.NotifyChunkToUpdateOnce();
            chunk.UpdateParameters(this, offsetFromSeed, _chunkSize, editOfOctaves, numberOfOctaves,  amplitude, lacunarity, persistence, material);
        }
    }
    
    // Returns the height changing chunks. Usually called by chunks to update height.
    public ICollection GetHeightChangingChunks()
    {
        return _heightChangingChunks.Values;
    }

    // Adds a chunk from heightChanging chunks
    public void AddHeightChangingChunk(Chunk chunk)
    {
        if (_heightChangingChunks.Contains(chunk.name)) return;

        _heightChangingChunks.Add(chunk.name, chunk);
        Debug.Log(chunk.name + " Added to effectors!");
        _currentChunkEnumerator = _heightChangingChunks.GetEnumerator();
        
        UpdateHeightOfChunks();
    }

    // Removes a chunk from heightChanging chunks
    public void RemoveHeightChangingChunk(Chunk chunk)
    {
        if (!_heightChangingChunks.Contains(chunk.name)) return;

        _heightChangingChunks.Remove(chunk.name);
        Debug.Log(chunk.name + " Removed from effectors!");
        _currentChunkEnumerator = _heightChangingChunks.GetEnumerator();

        UpdateHeightOfChunks();
    }
    
    // Can be used by chunks to send a terrain request update
    public void SendUpdateRequest()
    {
        if (_currentChunkEnumerator == null || !_currentChunkEnumerator.MoveNext())
            _currentChunkEnumerator = _chunksTable.GetEnumerator();
        
        _currentChunkEnumerator.Reset();
        
        UpdateHeightOfChunks();
    }

    // Change in inspector
    private int _lastWrapping;
    private void OnValidate()
    {
        if (_lastWrapping != wrapping)
        {
            RecalculateNewWrapping();
            _lastWrapping = wrapping;
        }
        else
        {
            UpdateHeightOfChunks();
        }
    }
}
