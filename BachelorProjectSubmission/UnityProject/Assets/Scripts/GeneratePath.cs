using System;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using NavMeshBuilder = UnityEditor.AI.NavMeshBuilder;

public class GeneratePath : MonoBehaviour
{
    [SerializeField] private bool useLocalPathfinding;
    [SerializeField] private bool isAddingControlPoints = false;
    [SerializeField] private List<Vector3> controlPointsList = new List<Vector3>();
    [SerializeField] private float travelDistance = 5f;
    [SerializeField] private float tolerantDistance = 7f;
    [SerializeField] [Range(0.0f, 90.0f)] private float maxWalkableSteepness = 30f;
    [SerializeField] [Range(0.0f, 90.0f)] private float maxWalkableSlope = 45f;
    [SerializeField] private int maxStepsUntilFail = 300;
    [SerializeField] [Range(0.1f, 5f)] private float steepnessMult = 2f;
    [SerializeField] [Range(0.1f, 5f)] private float slopeMult = 3f;
    [SerializeField] [Range(0.1f, 5f)] private float directionGoalMult = 4f;
    [SerializeField] [Range(0.1f, 5f)] private float sameDirectionMult = 1f;

    [SerializeField] private bool useDistanceInDirectionGoal = true;
    
    private LinkedList<Vector3> controlPointsLinkedList = new LinkedList<Vector3>();
    private ChunkManager _chunkManager;

    public void Initialize(ChunkManager chunkManager)
    {
        _chunkManager = chunkManager;
        _navMeshSurface = gameObject.AddComponent<NavMeshSurface>();
    }
    
    public void AddPathPoint(Vector3 point)
    {
        controlPointsLinkedList.AddLast(point);
        controlPointsList.Add(point);
        //CopyFromLinkedListToList();
    }

    private void OnValidate()
    {
        controlPointsLinkedList.Clear();
        foreach (Vector3 controlPoint in controlPointsList)
        {
            controlPointsLinkedList.AddLast(controlPoint);
        }

        if (_navMeshSurface == null)
        {
            _navMeshSurface = GetComponent<NavMeshSurface>();
            if (_navMeshSurface == null)
            {
                _navMeshSurface = gameObject.AddComponent<NavMeshSurface>();
            }
        }
        
        if (useLocalPathfinding)
        {
            _navMeshSurface.enabled = false;
        }
        else
        {
            _navMeshSurface.enabled = true;
        }
    }

    private void CopyFromLinkedListToList()
    {
        controlPointsList.Clear();
        foreach (Vector3 controlPoint in controlPointsLinkedList)
        {
            controlPointsList.Add(controlPoint);
        }
    }

    public void ResetControlPointsButton()
    {
        controlPointsLinkedList.Clear();
        controlPointsList.Clear();
    }
    
    public void ResetControlPointsWithoutFirstLastButton()
    {
        Vector3 first = controlPointsLinkedList.First.Value;
        Vector3 last = controlPointsLinkedList.Last.Value;
        controlPointsLinkedList.Clear();
        controlPointsList.Clear();

        AddPathPoint(first);
        AddPathPoint(last);
    }

    //---------------------------LOCAL-AGENT------------------------------------------------------------------------------------------------------------
    // Sends an agent through the control points to better approximate the path using the least steepness
    public void SendLocalAgentButton()
    {
        // Path needs two points
        if (controlPointsLinkedList.Count < 2) return;

        // Enumerator
        IEnumerator enumerator = controlPointsLinkedList.GetEnumerator();
        enumerator.MoveNext();
        Vector3 currentPosition = (Vector3)enumerator.Current;
        
        // New linked list for result
        LinkedList<Vector3> newControlPoints = new LinkedList<Vector3>();
        newControlPoints.AddLast(currentPosition);
        
        // Go through all the control points
        while (enumerator.MoveNext())
        {
            
            Vector3 currentTarget = (Vector3)enumerator.Current;
            Vector3 lastDirection = Vector3.zero;

            // Find points between two points
            bool foundSolution = false;
            for (int i = 0; i < maxStepsUntilFail; i++)
            {
                float distanceToControlPoint = Vector3.Distance(currentTarget, currentPosition);
                
                // Break if too close to control point (No need to generate new points)
                if (distanceToControlPoint <= tolerantDistance)
                {
                    currentPosition = currentTarget;
                    newControlPoints.AddLast(currentTarget);
                    foundSolution = true;
                    break;
                }
                
                Vector3 newPoint = FindNextStep(currentTarget, currentPosition, lastDirection);
                if (newPoint == Vector3.zero) continue;

                // Adds the new point to new control points
                newControlPoints.AddLast(newPoint);
                lastDirection = newPoint - currentPosition;
                currentPosition = newPoint;
                
                // Break if too close to control point (control point == next Point)
                if (Vector3.Distance(currentTarget, currentPosition) <= tolerantDistance)
                {
                    currentPosition = currentTarget;
                    newControlPoints.AddLast(currentTarget);
                    foundSolution = true;
                    break;
                }
            }

            if (!foundSolution)
            {
                Debug.LogError("Couldn't a good path with current configuration!");
                return;
            }
        }

        controlPointsLinkedList = newControlPoints;
        CopyFromLinkedListToList();
    }

    private Vector3 FindNextStep(Vector3 currentTarget, Vector3 currentPosition, Vector3 lastDirection)
    {
        int rotationAngle = 3;
        int tries = 180/rotationAngle + 1;
        
        Vector3 bestPosition = Vector3.zero;
        float bestSteepnessCoef = float.PositiveInfinity;

        Vector3 candidatePosition = currentPosition;
        Vector3 candidateDirection = Quaternion.AngleAxis(-90 - rotationAngle, Vector3.up) * (currentTarget - currentPosition).normalized;
        candidateDirection.y = 0;
        
        float candidateSteepnessCoef;
        while (tries >= 0)
        {
            // Try different direction
            candidateDirection = (Quaternion.AngleAxis(rotationAngle, Vector3.up) * candidateDirection).normalized;
            candidateDirection.y = 0;
            tries--;
            
            // Raycast to get candidate real position
            RaycastHit hitInfo;
            candidatePosition = currentPosition + candidateDirection * travelDistance + Vector3.up * 500;
            if (!Physics.Raycast(candidatePosition, Vector3.down, out hitInfo))
            {
                continue;
            }
            candidatePosition = hitInfo.point;
            
            // Abs to successfully test steepness 
            Vector3 temp = candidatePosition - currentPosition;
            temp.y = Mathf.Abs(temp.y);

            // See if we find better steepness
            float steepness = 90 - Mathf.Abs(Vector3.Angle(Vector3.up, temp));
            float slope = Vector3.Angle(Vector3.up, hitInfo.normal);
            
            if (steepness < maxWalkableSteepness && slope < maxWalkableSlope)
            {
                float distanceFromTarget = 1;
                if (useDistanceInDirectionGoal)
                {
                    distanceFromTarget = Vector3.Distance(candidatePosition, currentTarget);
                }
                
                // Reward function
                candidateSteepnessCoef = steepness * steepnessMult
                                         + slope * slopeMult
                                         + Mathf.Abs(Vector3.Angle(candidateDirection,
                                             currentTarget - currentPosition)) * directionGoalMult * 5 / distanceFromTarget
                                         + Vector3.Angle(candidateDirection, lastDirection) * sameDirectionMult;
                
                if (candidateSteepnessCoef <= bestSteepnessCoef)
                {
                    bestPosition = candidatePosition;
                    bestSteepnessCoef = candidateSteepnessCoef;
                }
            }
        }

        return bestPosition;
    }

    //---------------------------GLOBAL-AGENT------------------------------------------------------------------------------------------------------------
    private NavMeshSurface _navMeshSurface;
    
    public void SendGlobalAgentButton()
    {
        // Need start and end
        if (controlPointsList.Count < 2) return;

        // Build new navMeshBuildSettings
        _navMeshSurface.BuildNavMesh();

        Vector3 currentPosition = controlPointsLinkedList.First.Value;
        NavMeshPath path = new NavMeshPath();
        
        controlPointsLinkedList.Clear();
        foreach (Vector3 point in controlPointsList)
        {
            NavMesh.CalculatePath(currentPosition, point, NavMesh.AllAreas, path);
            
            foreach (Vector3 cornerPoint in path.corners)
            {
                FillSpaceBetweenBorderPoints(currentPosition, cornerPoint);
                currentPosition = cornerPoint;
            }     
        }
        
        CopyFromLinkedListToList();
    }

    private void FillSpaceBetweenBorderPoints(Vector3 currentPosition, Vector3 cornerPoint)
    {
        float distanceToCornerPoint = (currentPosition - cornerPoint).magnitude;
        while (distanceToCornerPoint > travelDistance)
        {
            currentPosition = Vector3.Lerp(currentPosition, cornerPoint, travelDistance/distanceToCornerPoint);

            RaycastHit hit;
            if (Physics.Raycast(currentPosition + Vector3.up * 5, Vector3.down, out hit))
            {
                currentPosition = hit.point;
            }

            controlPointsLinkedList.AddLast(currentPosition);
            distanceToCornerPoint = (currentPosition - cornerPoint).magnitude;
        }

        controlPointsLinkedList.AddLast(cornerPoint);
    }

    public void GeneratePathButton()
    {
        //TODO curve
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 lastPoint = Vector3.zero;
        
        foreach (Vector3 point in controlPointsLinkedList)
        {
            if (lastPoint != Vector3.zero)
            {
                Debug.DrawLine(lastPoint, point, Color.red, 0f, false);
            }
            Gizmos.DrawSphere(point, 1f);
            lastPoint = point;
        }
    }
}
