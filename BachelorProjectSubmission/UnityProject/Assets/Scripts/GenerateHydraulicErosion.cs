using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateHydraulicErosion : MonoBehaviour
{
    [SerializeField] private bool isAddingControlPoints = false;
    private LinkedList<Vector3> controlPointsLinkedList = new LinkedList<Vector3>();
    [SerializeField] private List<Vector3> controlPointsList = new List<Vector3>();
    [SerializeField] private float travelDistance = 5f;
    [SerializeField] private float maxAngle = 45f;
    [SerializeField] private int maxStepsUntilFail = 100;
    [SerializeField] [Range(0, 1)] private float inertia = 0.6f;

    public void AddHydroPoint(Vector3 point)
    {
        DropADroplet(point);
    }

    private void OnValidate()
    {
        controlPointsLinkedList.Clear();
        foreach (Vector3 controlPoint in controlPointsList)
        {
            controlPointsLinkedList.AddLast(controlPoint);
        }
    }

    private void CopyFromLinkedListToList()
    {
        controlPointsList.Clear();
        foreach (Vector3 controlPoint in controlPointsLinkedList)
        {
            controlPointsList.Add(controlPoint);
        }
    }

    public void ResetControlPointsButton()
    {
        controlPointsLinkedList.Clear();
        controlPointsList.Clear();
    }
    
    // Simulate a droplets movement
    public void DropADroplet(Vector3 start)
    {
        Vector3 currentPosition = start;

        Vector3 direction = Vector3.zero;
        
        // Start going from control point down
        for (int i = 0; i < maxStepsUntilFail; i++)
        {
            RaycastHit hitInfo;
            if (!Physics.Raycast(currentPosition + Vector3.up * 500, Vector3.down, out hitInfo))
            {
                continue;
            }

            Vector3 rotationAxis = Vector3.Cross(hitInfo.normal, Vector3.up).normalized;
            Vector3 gradient = (Quaternion.AngleAxis(-90, rotationAxis) * hitInfo.normal).normalized;
            Vector3 nextDirection = gradient * (1 - inertia) + direction * inertia;

            Vector3 nextNoY = nextDirection;
            nextNoY.y = 0;
            direction.y = 0;
            if (Vector3.Angle(direction, nextNoY) > maxAngle)
            {
                break;
            }

            if (nextDirection.y > 0)
            {
                break;
            }

            currentPosition += nextDirection.normalized * travelDistance;

            controlPointsLinkedList.AddLast(hitInfo.point);

            direction = nextDirection;
        }

        CopyFromLinkedListToList();
    }

    public void GeneratePathButton()
    {
        //TODO curve
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Vector3 lastPoint = Vector3.zero;
        
        foreach (Vector3 point in controlPointsLinkedList)
        {
            Gizmos.DrawSphere(point, 1f);
            lastPoint = point;
        }
    }
}
