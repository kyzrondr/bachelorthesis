using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class Chunk : MonoBehaviour
{
    [SerializeField] public bool enableTerrainChange = false;
    [SerializeField] public float heightOffset;
    [SerializeField] [Range(0.0f, float.PositiveInfinity)] public float heightRange;
    
    private ChunkManager _chunkManager;

    [SerializeField] [ReadOnly] private float _chunkSize;
    private float _offsetFromSeed;
    private int _numberOfOctaves;
    private int _wrappingOfPoints;
    private Vector3[] _editOfOctaves;
    private float _amplitude;
    private float _lacunarity;
    private float _persistence;
    
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;
    private MeshCollider _meshCollider;
    private Vector2Int _indexes;
    private Material _material;
    
    private const float DEFAULT_SCALE = 90f;
    
    //----------------------------------------------------------------Chunk-Creation---------------------------------------------------------------------------------------------
    // Initializes all the essential information of the chunk
    public void Initialize(ChunkManager chunkManager, float offsetFromSeed, Vector2Int indexes, float size, Vector3[] editOfOctaves, int numberOfOctaves,
        float amplitude, float lacunarity, float persistence, int wrapping, Material material)
    {
        _chunkSize = size;
        
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();
        _meshFilter = gameObject.AddComponent<MeshFilter>();
        _meshCollider = gameObject.AddComponent<MeshCollider>();

        _chunkManager = chunkManager;
        _offsetFromSeed = offsetFromSeed;
        _indexes = indexes;
        _editOfOctaves = editOfOctaves;
        _wrappingOfPoints = wrapping;
        _numberOfOctaves = numberOfOctaves;
        _amplitude = amplitude;
        _lacunarity = lacunarity;
        _persistence = persistence;
        
        _material = material;
        _meshRenderer.material = material;
        
        transform.position = new Vector3(indexes.x, 0.0f, indexes.y) * _chunkSize;
        CalculateMesh();
        
    }

    private void Update()
    {
        // Upon loading the project references are lost
        if (_chunkManager == null)
        {
            _chunkManager = GetComponentInParent<ChunkManager>();
            RecalculateIndexes();
            CalculateMesh();
        }
    }

    private void RecalculateIndexes()
    {
        string indexesString = name.Substring(6, name.Length-1-6);
        string[] twoIndexes = indexesString.Split(',');
        int x, y;
        int.TryParse(twoIndexes[0], out x);
        int.TryParse(twoIndexes[1], out y);
                
        _indexes = new Vector2Int(x,y);
    }

    private void CalculateMesh()
    {
        // Calculate the offset (distance) between points
        float pointPositionOffset;
        if (_wrappingOfPoints % 2 == 0) // Special case for even numbers shouldn't be possible
        {
            pointPositionOffset = _chunkSize;
        }
        else
        {
            pointPositionOffset = _chunkSize / (_wrappingOfPoints - 1);
        }

        // Arrays for storing the mesh
        int numberOfVerticies = _wrappingOfPoints * _wrappingOfPoints;
        int numberOfTriangles = (numberOfVerticies - (_wrappingOfPoints + _wrappingOfPoints - 1)) * 2;
        Vector3[] verticies = new Vector3[numberOfVerticies];
        Vector2[] uvCoords = new Vector2[numberOfVerticies];
        int[] triangles = new int[numberOfTriangles * 3];
        int triangleIdx = 0;

        // Get used effectors
        List<Chunk> usedEffector = GetUsedEffectors();
        
        // Calculate integer division by 2 of wrapping points, used in calculation the offset 
        int wrapDiv2 = _wrappingOfPoints / 2;

        // Create verticies, faces and uv coords
        for (int vertexIdx = 0; vertexIdx < numberOfVerticies; vertexIdx++) 
        {
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset = (vertexIdx % _wrappingOfPoints) - wrapDiv2;
            int yGridOffset = (vertexIdx / _wrappingOfPoints) - wrapDiv2;

            // Verticies
            float x = xGridOffset * pointPositionOffset;
            float z = yGridOffset * pointPositionOffset;

            // Perlin noise calculation
            float height = GetHeightOfPoint(new Vector2(x, z), usedEffector);

            Vector3 vertexPosition = new Vector3(x, height, z);
            verticies[vertexIdx] = vertexPosition;

            // UV coords
            uvCoords[vertexIdx] = new Vector2((float)xGridOffset / _wrappingOfPoints, (float)yGridOffset / _wrappingOfPoints);

            // Faces (only when not last row and not last column)
            if ((vertexIdx + 1) % _wrappingOfPoints != 0 && vertexIdx / _wrappingOfPoints != _wrappingOfPoints - 1)
            {
                // First face of quad 
                triangles[triangleIdx++] = vertexIdx;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints + 1;
                
                // Second face of quad
                triangles[triangleIdx++] = vertexIdx;
                triangles[triangleIdx++] = vertexIdx + _wrappingOfPoints + 1;
                triangles[triangleIdx++] = vertexIdx + 1;
            }
        }
        
        _meshFilter.sharedMesh = new Mesh {vertices = verticies, triangles = triangles, uv = uvCoords, name = this.name};
        RecalculateNormals();
        _meshFilter.sharedMesh.RecalculateBounds();
        _meshCollider.sharedMesh = _meshFilter.sharedMesh;
    }

    private void RecalculateNormals()
    {
        List<Chunk> usedEffectors = GetUsedEffectors();
        Vector3[] verticies = new Vector3[_wrappingOfPoints * _wrappingOfPoints];
        Vector3[] vertexNormals = new Vector3[_wrappingOfPoints * _wrappingOfPoints];
        int triangleCount = (_wrappingOfPoints - 1) * (_wrappingOfPoints - 1) * 2;
        
        // Calculation is:   points in one row * faces * four sides + corner faces
        int numberOfBorderTriangles = (_wrappingOfPoints - 1) * 2 * 4 + 8;
        int borderWrapping = _wrappingOfPoints + 2;
        int borderWrapDiv2 = borderWrapping / 2;
            
        // Calculate the offset (distance) between points
        float pointPositionOffset;
        if (borderWrapping % 2 == 0) // Special case for even numbers shouldn't be possible
        {
            pointPositionOffset = _chunkSize + (_chunkSize/_wrappingOfPoints) * 2;
        }
        else
        {
            pointPositionOffset = (_chunkSize + (_chunkSize/(_wrappingOfPoints-1)) * 2)  / (borderWrapping - 1) ;
        }
        
        // Loop through faces including an outer layer (to calculate normals without seams)
        int triangleCountWithBorders = triangleCount + numberOfBorderTriangles;
        for (int triangleIdx = 0; triangleIdx < triangleCountWithBorders; triangleIdx++)
        {
            int triangleRowIdx = triangleIdx / ((borderWrapping-1) * 2);
            
            int vertexIdx1;
            int vertexIdx2;
            int vertexIdx3;
            
            // Odd Triangles
            if (triangleIdx % 2 == 0)
            {
                vertexIdx1 = triangleIdx / 2 + triangleRowIdx;
                vertexIdx2 = vertexIdx1 + borderWrapping;
                vertexIdx3 = vertexIdx2 + 1;
            }
            // Even Triangles
            else
            {
                vertexIdx1 = (triangleIdx - 1) / 2 + triangleRowIdx;
                vertexIdx2 = vertexIdx1 + 1;
                vertexIdx3 = vertexIdx2 + borderWrapping;
            }
            
            // Coordinates in the grid from [0,0] to [wrapping-1, wrapping-1]
            int xGridOffset1 = (vertexIdx1 % borderWrapping) - borderWrapDiv2;
            int yGridOffset1 = (vertexIdx1 / borderWrapping) - borderWrapDiv2;
            int xGridOffset2 = (vertexIdx2 % borderWrapping) - borderWrapDiv2;
            int yGridOffset2 = (vertexIdx2 / borderWrapping) - borderWrapDiv2;
            int xGridOffset3 = (vertexIdx3 % borderWrapping) - borderWrapDiv2;
            int yGridOffset3 = (vertexIdx3 / borderWrapping) - borderWrapDiv2;
            
            // X and Z coordinates of verticies
            float x1 = xGridOffset1 * pointPositionOffset;
            float z1 = yGridOffset1 * pointPositionOffset;
            float x2 = xGridOffset2 * pointPositionOffset;
            float z2 = yGridOffset2 * pointPositionOffset;
            float x3 = xGridOffset3 * pointPositionOffset;
            float z3 = yGridOffset3 * pointPositionOffset;
            
            // Create 3 verticies using idx
            Vector3 vertex1 = new Vector3(x1, GetHeightOfPoint(new Vector2(x1,z1), usedEffectors), z1);
            Vector3 vertex2 = new Vector3(x2, GetHeightOfPoint(new Vector2(x2,z2), usedEffectors), z2);
            Vector3 vertex3 = new Vector3(x3, GetHeightOfPoint(new Vector2(x3,z3), usedEffectors), z3);

            Vector3 normalVector = GetUpNormalFromPoints(vertex1, vertex2, vertex3);

            // If vertex1 isn't a border point
            int vertIdxModulo = vertexIdx1 % borderWrapping;
            int vertexRowIdx = vertexIdx1 / borderWrapping;

            if (vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1)
            {
                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx1 - borderWrapping - (vertexRowIdx * 2 - 1);
                
                vertexNormals[oldIdx] += normalVector;
            }
            
            // If vertex2 isn't a border point
            vertIdxModulo = vertexIdx2 % borderWrapping;
            vertexRowIdx = vertexIdx2 / borderWrapping;
            if (vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1)
            {
                // convert vertex idx to old wrapping and add normal to normals
                int oldIdx = vertexIdx2 - borderWrapping - (vertexRowIdx * 2 - 1);

                vertexNormals[oldIdx] += normalVector;
            }
            
            // If vertex3 isn't a border point
            vertIdxModulo = vertexIdx3 % borderWrapping;
            vertexRowIdx = vertexIdx3 / borderWrapping;
            if (vertIdxModulo != 0 && vertIdxModulo != borderWrapping-1 && vertexRowIdx != 0 && vertexRowIdx != borderWrapping-1)
            {
                int oldIdx = vertexIdx3 - borderWrapping - (vertexRowIdx * 2 - 1);

                vertexNormals[oldIdx] += normalVector;
            }
        }

        // Normalize all normal vectors
       for (int i = 0; i < verticies.Length; i++)
        {
            vertexNormals[i] = vertexNormals[i].normalized;
        }

        _meshFilter.sharedMesh.normals = vertexNormals;
    }

    private Vector3 GetUpNormalFromPoints(Vector3 point1, Vector3 point2, Vector3 point3)
    {
        Vector3 fromOneToTwo = point2 - point1;
        Vector3 fromOneToThree = point3 - point1;

        Vector3 normalVector = Vector3.Cross(fromOneToTwo, fromOneToThree).normalized;
        return normalVector.y >= 0 ? normalVector : -normalVector;
    }

    // New chunk generation
    public Chunk GenerateTopChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x, _indexes.y + 1));
    }
    
    public Chunk GenerateRightChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x + 1, _indexes.y));
    }
    
    public Chunk GenerateBottomChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x, _indexes.y - 1));
    }
    
    public Chunk GenerateLeftChunk()
    {
        RecalculateIndexes();
        return _chunkManager.CreateChunk(new Vector2Int(_indexes.x - 1, _indexes.y));
    }
    
    //-------------------------------------------------------------------------Chunk-Editing------------------------------------------------------------------------
    
    private bool _lastEnableTerrainChange = false;
    private float _lastHeightOffset = 0f;
    private float _lastHeightRange = 0f;
    private bool _changedHeightLastUpdate = false;
    
    // On Inspector change
    private void OnValidate()
    {
        if (_meshFilter == null)
        {
            _meshFilter = GetComponent<MeshFilter>();
            RecalculateIndexes();
        }

        // Add or remove chunk to height effectors
        if (enableTerrainChange != _lastEnableTerrainChange)
        {
            if (enableTerrainChange)
            {
                _chunkManager.AddHeightChangingChunk(this);
            }
            else
            {
                _chunkManager.RemoveHeightChangingChunk(this);
            }

        }
        _lastEnableTerrainChange = enableTerrainChange;

        // Upon changing the height setting we need to push an update to chunk manager
        if (heightOffset != _lastHeightOffset || heightRange != _lastHeightRange)
        {
            _chunkManager.SendUpdateRequest();
        }
        _lastHeightOffset = heightOffset;
        _lastHeightRange = heightRange;
    }

    public void NotifyChunkToUpdateOnce()
    {
        _changedHeightLastUpdate = true;
    }

    public void UpdateParameters(ChunkManager chunkmanager, float offsetFromSeed, float size, Vector3[] editOfOctaves, int numberOfOctaves,
        float amplitude, float lacunarity, float persistence, Material material)
    {
        _offsetFromSeed = offsetFromSeed;
        _chunkSize = size;
        _editOfOctaves = editOfOctaves;
        _numberOfOctaves = numberOfOctaves;
        _amplitude = amplitude;
        _lacunarity = lacunarity;
        _persistence = persistence;
        _material = material;
        if (_meshRenderer == null)
        {
            RecalculateIndexes();
            _meshRenderer = GetComponent<MeshRenderer>();  // When opening the project references are not kept
        }
        _meshRenderer.material = material;

        _chunkManager = chunkmanager;
        UpdateHeight();
    }
    
    // Updates height of verticies based on chunks that change height (effectors)
    private void UpdateHeight()
    {
        // When opening the project references are not kept------
        if (_meshFilter == null)
        {
            _meshFilter = GetComponent<MeshFilter>();
            RecalculateIndexes();
        }

        if (_meshCollider == null)
        {
            _meshCollider = GetComponent<MeshCollider>();
            RecalculateIndexes();            
        }
        //-------------------------------------------------------

        // Goes through the effectors and updates only the ones that reach this chunk
        List<Chunk> usedEffectors = GetUsedEffectors();

        // Needed to reset effect of height effectors
        if (usedEffectors.Count == 0)
        {
            if (!_changedHeightLastUpdate)
            {
                return;
            }
        }
        else
        {
            _changedHeightLastUpdate = true;
        }
        
        Vector3[] verticies = _meshFilter.sharedMesh.vertices;
        
        for (int i = 0; i < verticies.Length; i++)
        {
            Vector3 point = verticies[i];

            // Perlin noise and height effectors calculation
            float height = GetHeightOfPoint(new Vector2(point.x, point.z), usedEffectors);
            
            point.y = height;
            verticies[i] = point;
        }

        // Needed to reset effect of height effectors
        if (usedEffectors.Count == 0)
        {
            _changedHeightLastUpdate = false;
        }
        
        _meshFilter.sharedMesh.vertices = verticies;
        RecalculateNormals();
        _meshFilter.sharedMesh.RecalculateBounds();
        _meshCollider.sharedMesh = _meshFilter.sharedMesh;
        
    }

    private List<Chunk> GetUsedEffectors()
    {
        ICollection allEffectors = _chunkManager.GetHeightChangingChunks();
        
        List<Chunk> usedEffectors = new List<Chunk>();
        foreach (Chunk effector in allEffectors)
        {
            if (Vector3.Distance(effector.transform.position, transform.position) < effector.heightRange + _chunkSize * 1.5f)
            {
                usedEffectors.Add(effector);
            }
        }

        return usedEffectors;
    }

    public void ChangeWrapping(int newWrapping)
    {
        if (newWrapping % 2 != 1)
        {
            newWrapping += 1;
        }
        _wrappingOfPoints = newWrapping;
        CalculateMesh();
    }

    private float GetHeightOfPoint(Vector2 pointXZ, List<Chunk> usedEffectors)
    {
        Vector3 chunkPosition = transform.position;
        
        float perlinX = pointXZ.x + chunkPosition.x + _offsetFromSeed;
        float perlinZ = pointXZ.y + chunkPosition.z + _offsetFromSeed;
        float height = 0;

        float tempFrequency = 1;
        float tempAmplitude = _amplitude;
        
        // Recursive detail
        for (int i = 0; i < _numberOfOctaves; i++)
        {
            Vector3 editOfOctave = new Vector3(0, 0, DEFAULT_SCALE);
            if (i < _editOfOctaves.Length)
            {
                editOfOctave = _editOfOctaves[i];
                if (editOfOctave.z == 0) editOfOctave.z = DEFAULT_SCALE;
            }
            
            float tempX = perlinX / editOfOctave.z * tempFrequency;
            float tempZ = perlinZ / editOfOctave.z * tempFrequency;

            height += MakeQuadratic(Mathf.PerlinNoise(tempX + editOfOctave.x, tempZ + editOfOctave.y)) * tempAmplitude;
            
            tempAmplitude *= _persistence;
            tempFrequency *= _lacunarity;
        }
        
        Vector2 chunkPositionXZ = new Vector2(chunkPosition.x, chunkPosition.z);
        
        // Height offset from effectors
        float heightFromEffectors = 0f;
        foreach (Chunk effector in usedEffectors)
        {
            Vector3 effectorPosition = effector.transform.position;
            Vector2 epicenter = new Vector2(effectorPosition.x, effectorPosition.z);
                
            // Too far from epicenter
            float distanceFromEpicenter = Mathf.Abs((epicenter - (pointXZ + chunkPositionXZ)).magnitude);
            if (distanceFromEpicenter > effector.heightRange) continue;

            // Range is zero
            if (effector.heightRange == 0) continue;

            float interpolationCoefficient = 1 - distanceFromEpicenter / effector.heightRange;
                
            heightFromEffectors += MakeQuadratic(interpolationCoefficient) * effector.heightOffset; // TODO try x pow x
        }

        return height + heightFromEffectors;
    }

    private float MakeQuadratic(float x)
    {
        return -2 * (x*x*x) + 3 * (x*x);
    }

    public float GetChunkSize()
    {
        return _chunkSize;
    }
    
    // TODO connect border points
}
