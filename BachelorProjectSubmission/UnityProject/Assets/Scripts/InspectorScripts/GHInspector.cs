using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenerateHydraulicErosion))]
public class GHInspector : Editor
{
    private SerializedProperty _isAddingControlPoints;
    private SerializedProperty _controlPoints;
    private SerializedProperty _travelDistance;
    private SerializedProperty _maxAngle;
    private SerializedProperty _maxStepsUntilFail;
    private SerializedProperty _inertia;

    private void OnEnable()
    {
        _isAddingControlPoints = serializedObject.FindProperty("isAddingControlPoints");
        _controlPoints = serializedObject.FindProperty("controlPointsList");
        _travelDistance = serializedObject.FindProperty("travelDistance");
        _maxAngle = serializedObject.FindProperty("maxAngle");
        _maxStepsUntilFail = serializedObject.FindProperty("maxStepsUntilFail");
        _inertia = serializedObject.FindProperty("inertia");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GenerateHydraulicErosion generateHydraulicErosion = (GenerateHydraulicErosion) target;

        // Set text of button
        string buttonText;
        if (_isAddingControlPoints.boolValue)
        {
            buttonText = "Stop Manually Adding Points";
        }
        else
        {
            buttonText = "Start Manually Adding Points";
        }
        
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button(buttonText))
        {
            _isAddingControlPoints.boolValue = !_isAddingControlPoints.boolValue;
        }
        EditorGUILayout.EndHorizontal();
        
        
        EditorGUILayout.PropertyField(_controlPoints, new GUIContent("Droplet paths:", "Way of droplets"));
        
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Reset points"))
        {
            generateHydraulicErosion.ResetControlPointsButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.Space(20);

        EditorGUILayout.PropertyField(_travelDistance, new GUIContent("Travel Distance:", "How far will the droplet travel in one step."));
        EditorGUILayout.PropertyField(_maxAngle, new GUIContent("Maximum Angle:", "When the droplets next direction differs by this angle the simulation of this drop is stopped."));
        EditorGUILayout.PropertyField(_maxStepsUntilFail, new GUIContent("No. steps until failing:", "Maximum number of control droplets."));
        EditorGUILayout.PropertyField(_inertia, new GUIContent("Inertia of droplets:", "How much momentum will they keep from last position."));

        EditorGUILayout.Space(10);
        
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        /*if (GUILayout.Button("Send Agent To Generate Control Droplets"))
        {
            generateHydraulicErosion.SendLocalAgentButton();    // TODO refresh view
        }
        GUILayout.FlexibleSpace();/*/
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.Space(20);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Generate River"))
        {
            generateHydraulicErosion.GeneratePathButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        if(!_isAddingControlPoints.boolValue) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray rayToCast = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            
            if (Physics.Raycast(rayToCast, out hitInfo))
            {
                GenerateHydraulicErosion generateHydraulicErosion = (GenerateHydraulicErosion) target;
                generateHydraulicErosion.AddHydroPoint(hitInfo.point);
                
            }
            Event.current.Use();
        }

    }
}
