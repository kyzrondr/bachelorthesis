using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GeneratePath))]
public class GPInspector : Editor
{
    private SerializedProperty _useLocalPathfinding;
    private SerializedProperty _isAddingControlPoints;
    private SerializedProperty _controlPoints;
    private SerializedProperty _travelDistance;
    private SerializedProperty _tolerantDistance;
    private SerializedProperty _maxWalkableSteepness;
    private SerializedProperty _maxStepsUntilFail;
    private SerializedProperty _maxWalkableSlope;
    private SerializedProperty _steepnessMult;
    private SerializedProperty _slopeMult;
    private SerializedProperty _directionGoalMult;
    private SerializedProperty _sameDirectionMult;
    private SerializedProperty _useDistanceInDirectionGoal;

    private void OnEnable()
    {
        _useLocalPathfinding = serializedObject.FindProperty("useLocalPathfinding");
        _isAddingControlPoints = serializedObject.FindProperty("isAddingControlPoints");
        _controlPoints = serializedObject.FindProperty("controlPointsList");
        _travelDistance = serializedObject.FindProperty("travelDistance");
        _tolerantDistance = serializedObject.FindProperty("tolerantDistance");
        _maxWalkableSteepness = serializedObject.FindProperty("maxWalkableSteepness");
        _maxWalkableSlope = serializedObject.FindProperty("maxWalkableSlope");
        _maxStepsUntilFail = serializedObject.FindProperty("maxStepsUntilFail");
        _sameDirectionMult = serializedObject.FindProperty("sameDirectionMult");
        _directionGoalMult = serializedObject.FindProperty("directionGoalMult");
        _slopeMult = serializedObject.FindProperty("slopeMult");
        _steepnessMult = serializedObject.FindProperty("steepnessMult");
        _useDistanceInDirectionGoal = serializedObject.FindProperty("useDistanceInDirectionGoal");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GeneratePath generatePath = (GeneratePath) target;

        // Set text of button
        string buttonText;
        if (_isAddingControlPoints.boolValue)
        {
            buttonText = "Stop Manually Adding Points";
        }
        else
        {
            buttonText = "Start Manually Adding Points";
        }
        
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button(buttonText))
        {
            _isAddingControlPoints.boolValue = !_isAddingControlPoints.boolValue;
        }
        EditorGUILayout.EndHorizontal();
        
        
        EditorGUILayout.PropertyField(_controlPoints, new GUIContent("Control Points:", "Control points of the path"));
        
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Reset points"))
        {
            generatePath.ResetControlPointsButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Reset except 1st and last"))
        {
            generatePath.ResetControlPointsWithoutFirstLastButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.Space(20);

        EditorGUILayout.PropertyField(_useLocalPathfinding,
            new GUIContent("Use Local Pathfinding:",
                "If enabled it uses my own implementation of pathfinding. It's not as stable but can create nice winding paths. If disabled it uses Unity's navmesh and pathfinding."));
        
        EditorGUILayout.PropertyField(_travelDistance, new GUIContent("Travel Distance:", "How far will the agent travel in one step."));
        EditorGUILayout.PropertyField(_tolerantDistance, new GUIContent("Tolerant Distance:", "Size of area around control point which counts as reaching it. (Should be a bit higher than Travel Distance)"));
       

        if (_useLocalPathfinding.boolValue)
        {
            EditorGUILayout.PropertyField(_maxStepsUntilFail, new GUIContent("No. steps until failing:", "Maximum number of steps between control points, that the agent can take."));
            EditorGUILayout.PropertyField(_maxWalkableSteepness, new GUIContent("Max walkable steepness:", "Maximum angle Y between two steps."));
            EditorGUILayout.PropertyField(_maxWalkableSlope, new GUIContent("Max walkable slope:", "Maximum steepness of terrain in control point."));

            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("Local path finding parameters:" , EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_steepnessMult, new GUIContent("Steepness mult.:", "Multiplier of steepness in reward function calculation in pathfinding."));
            EditorGUILayout.PropertyField(_slopeMult, new GUIContent("Slope mult.:", "Multiplier of slope in reward function calculation in pathfinding."));
            EditorGUILayout.PropertyField(_sameDirectionMult, new GUIContent("Same Direction mult.:", "Multiplier of walking in similar direction in reward function calculation in pathfinding."));
            EditorGUILayout.PropertyField(_directionGoalMult, new GUIContent("Direction mult.:", "Multiplier of direction to the goal in reward function calculation in pathfinding. Basically how much towards the goal the agent goes."));
            EditorGUILayout.PropertyField(_useDistanceInDirectionGoal, new GUIContent("Divide dir. by dist.:", "If enabled it divides the direction to the goal by distance. Thus making more winding and non-direct paths. However paths are less convergent."));

        
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Send Local Agent To Generate Control Points"))
            {
                generatePath.SendLocalAgentButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.HelpBox("Due to using Unity's experimental package for global NavMesh pathfinding please use the component below to change the parameters of the NavMeshSurface.", MessageType.Warning);
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Send Global Agent To Generate Control Points"))
            {
                generatePath.SendGlobalAgentButton();
                SceneView.RepaintAll();
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
        }
        
        EditorGUILayout.Space(20);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Generate Path"))
        {
            generatePath.GeneratePathButton();
            SceneView.RepaintAll();
        }
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        
        //TODO warning if maxSteps is lower than travelDist * numOfSteps
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        if(!_isAddingControlPoints.boolValue) return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray rayToCast = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            
            if (Physics.Raycast(rayToCast, out hitInfo))
            {
                GeneratePath generatePath = (GeneratePath) target;
                generatePath.AddPathPoint(hitInfo.point);
                
            }
            Event.current.Use();
        }

    }
}
