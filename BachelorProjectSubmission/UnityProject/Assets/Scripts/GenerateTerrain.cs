using System.ComponentModel;
using UnityEditor;
using UnityEngine;

public class GenerateTerrain : MonoBehaviour
{
    [SerializeField] public int seed;
    [SerializeField] public Transform parent;
    [SerializeField] public float size = 30;
    [SerializeField] public float perlinScale = 90f;
    [SerializeField] public float amplitude = 30f;
    [SerializeField] [Range(1, 15)] public int numberOfOctaves = 6;
    [SerializeField] public int wrapping = 23;
    [SerializeField] public Vector2Int numberOfChunks = new Vector2Int(5,5);
    [SerializeField] public Material material;
    [SerializeField][ReadOnlyInspectorAttribute] private ChunkManager chunkManager;
    
    // Temporarly readonly
    private float lacunarity = 1.8f;
    private float persistence = 0.4f;

    
    public void SetNewWrappingButton(int newWrapping)
    {
        if (newWrapping < 3) newWrapping = 3;
        wrapping = newWrapping;
    }
    
    public void GenerateButton()
    {
        Random.InitState(seed);
        float randomOffset = (Random.value * 200000) - 100000;    // Unity's perlin noise works well only in range [-100 000, 100 000]

        // Creates chunk manager with set parameters
        Transform chunks = new GameObject().transform;
        chunks.parent = parent;
        chunks.name = "ChunksManager";
        chunkManager = chunks.gameObject.AddComponent<ChunkManager>();
        chunkManager.InitializeParameters(randomOffset, size, perlinScale, numberOfOctaves, amplitude, lacunarity, persistence, wrapping, material);

        // Creates path generator
        Transform generatePath = new GameObject().transform;
        generatePath.parent = parent;
        generatePath.name = "GeneratePath";
        GeneratePath pathGenerator = generatePath.gameObject.AddComponent<GeneratePath>();
        pathGenerator.Initialize(chunkManager);
        
        Transform generateHydraulicErosion = new GameObject().transform;
        generateHydraulicErosion.parent = parent;
        generateHydraulicErosion.name = "GenerateHydraulicErosion";
        generateHydraulicErosion.gameObject.AddComponent<GenerateHydraulicErosion>();
        
        for (int x = 0; x < numberOfChunks.x; x++)
        {
            for (int y = 0; y < numberOfChunks.y; y++)
            {
                chunkManager.CreateChunk(new Vector2Int(x,y));
            }
        }
        Debug.Log("Generated Chunk Manager and Chunks.");
    }
}
