Shader "Custom/TerrainTextureShader"
{
    Properties
    {
        _GrassColor ("GrassColor", Color) = (1,1,1,1)
        _ThresholdGD ("ThresholdGD", Range(0,1)) = 0.5
        _InterpolateGD ("InterpolateGD", Range(0.0,1.0)) = 0
        _DirtColor ("DirtColor", Color) = (1,1,1,1)
        _ThresholdDS ("ThresholdDS", Range(0,1)) = 0.5
        _InterpolateDS ("InterpolateDS", Range(0.0,1.0)) = 0
        _StoneColor ("StoneColor", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _GrassColor;
        half _ThresholdGD;
        half _InterpolateGD;
        fixed4 _DirtColor;
        half _ThresholdDS;
        half _InterpolateDS;
        fixed4 _StoneColor;
        

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float getAngle(float3 a, float3 b)
        {
            return acos(dot(a, b)/(length(a) * length(b)));
        }
        
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
           // fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;

            float angle = 1 - getAngle(IN.worldNormal,float3(0.f,1.f,0.f));

            float grassThresholdLower = _ThresholdGD + _InterpolateGD;
            float dirtThresholdUpper = _ThresholdGD - _InterpolateGD;
            float dirtThresholdLower = _ThresholdDS + _InterpolateDS;
            float stoneThresholdUpper = _ThresholdDS - _InterpolateDS;
            
            if (angle > grassThresholdLower)
            {
                o.Albedo = _GrassColor;                
            }
            else if (angle > dirtThresholdUpper)
            {
                float divisor = grassThresholdLower - dirtThresholdUpper;
                float offset = dirtThresholdUpper/divisor;
                angle = lerp(0.0f, 1.f, angle/divisor - offset);
                o.Albedo = lerp(_DirtColor, _GrassColor, angle);              
            }
            else if (angle > dirtThresholdLower)
            {
                o.Albedo = _DirtColor;   
            }
            else if (angle > stoneThresholdUpper)
            {
                float divisor = dirtThresholdLower - stoneThresholdUpper;
                float offset = stoneThresholdUpper/divisor;
                angle = lerp(0.0f, 1.f, angle/divisor - offset);
                o.Albedo = lerp(_StoneColor, _DirtColor, angle); 
            }
            else
            {
                o.Albedo = _StoneColor;
            }
            
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            //o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
